# AfSIS Streaming Prototype

## Preliminaries

### Required software

   * Vagrant 1.7.2+
   * VirtualBox 4.3.x
   * Ansible 1.9.x
   * Python packages: `pip install -r requirements.txt`

### Kafka VM

```
git clone https://github.com/emaadmanzoor/vagrantKafkaBox.git
cd vagrantKafkaBox
vagrant up
```

Verify the Kafka VM: `python test_kafka_vm.py` (`^C` to stop).

## Quickstart

   * Run the simulation: `./run_simulation.sh`
