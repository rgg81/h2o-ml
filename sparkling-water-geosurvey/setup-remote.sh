#!/usr/bin/env bash

usage ()
{
  echo 'Usage : setup-remote [-h] [-u ssh-user] [-r ssh-host-remote] [-i ssh-pem-file] [-f csv-file]'
  echo '             -h                    display help'
  echo "             -u ssh-user           ssh user to log in remote machine"
  echo "             -r ssh-host-remote    remote host to run"
  echo "             -i ssh-pem-file       pem file credentials to ssh remote machine"
  echo "             -f csv-file           file that will be copied to remote machine"
  exit
}



while [ "$1" != "" ]; do
case $1 in
        -u )           shift
                       USER=$1
                       ;;
        -r )           shift
                       REMOTE_HOST=$1
                       ;;
        -i )           shift
                       PEM_FILE=$1
                       ;;
        -f )           shift
                       FILE=$1
                       ;;
        -h )           shift
                       usage
                       ;;
        * )            QUERY=$1
    esac
    shift
done


if [ "$USER" == "" ]
then
    echo "SSH user must be set. Type -h for help."
    exit
fi

if [ "$REMOTE_HOST" == "" ]
then
    echo "Remote host must be set. Type -h for help."
    exit
fi

if [ "$PEM_FILE" == "" ]
then
    echo "PEM file must be set. Type -h for help."
    exit
fi

if [ "$FILE" == "" ]
then
    echo "File must be set. Type -h for help."
    exit
fi

./gradlew shadowJar

ssh -o "StrictHostKeyChecking no" -i "$PEM_FILE" "$USER@$REMOTE_HOST" 'mkdir -p ~/sparkling-water-geosurvey/build/libs'
scp -o StrictHostKeyChecking=no -i "$PEM_FILE" build/libs/sparkling-water-geosurvey-app.jar "$USER@$REMOTE_HOST":~/sparkling-water-geosurvey/build/libs/
scp -o StrictHostKeyChecking=no -i "$PEM_FILE" "$FILE" "$USER@$REMOTE_HOST":~/sparkling-water-geosurvey/
scp -o StrictHostKeyChecking=no -i "$PEM_FILE" run.sh "$USER@$REMOTE_HOST":~/sparkling-water-geosurvey/

ssh -o "StrictHostKeyChecking no" -i "$PEM_FILE" "$USER@$REMOTE_HOST" <<-'ENDSSH'
    # installing java 8
    sudo add-apt-repository -y ppa:webupd8team/java
    sudo apt-get update
    echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections
    sudo apt-get install -y oracle-java8-installer

    # Downloading spark
    cd $HOME
    wget http://d3kbcqa49mib13.cloudfront.net/spark-1.5.2-bin-hadoop2.6.tgz
    tar -zxf spark-1.5.2-bin-hadoop2.6.tgz
    cd spark-1.5.2-bin-hadoop2.6
    export SPARK_HOME=$HOME/spark-1.5.2-bin-hadoop2.6
    echo "export SPARK_HOME=$HOME/spark-1.5.2-bin-hadoop2.6" | sudo tee -a /etc/profile.d/sparkling.sh
ENDSSH




