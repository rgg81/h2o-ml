# Sparkling Water Geo Survey

This is a project uses H2O sparkling water and mllib sparking library. The goal is to solve a problem using machine learning algorithms.     
The project has 4 applications: H2OClassifierGeoSurvey, H2OClassifierModelBuilder, H2OClassifierPredictor and MllibClassifierGeoSurvey.  
The input data must be a csv and for now is required to have a header.  


## H2OClassifierGeoSurvey
First, it splits the input data in training(70%), valid(10%) and test(20%). 
Then, it builds a machine learning model using one of the algorithm: Gradient Boost Machine, Deep Learning and Distributed Random Forest.  
Finally the test data is used for predictions and the result is saved as a csv.  
Arguments(index - description):
0 - the csv file location used as input to build the model
1 - the algorithm to be used (DRF, DL and GBM)
Parameters:
Balance Class: when set to true, it will oversample labels. Works on all algorithms.  
Total trees: number of trees to be produced. Works on tree algorithms DRF and GBM.
Max Depth: max depth used in tree algorithms DRF and GBM.  
MTRIES: DRF Param: mtries specifies the number of features to be selected from the whole set. When set to -1, the number of features is the square root of the total number of features, rounded to the nearest integer.


## H2OClassifierModelBuilder

First, it splits the input data in training(80%) and valid(20%) since this job does not make predictions. 
Then, it builds a machine learning model using one of the algorithm: Gradient Boost Machine, Deep Learning and Distributed Random Forest.  
The model is serialized and saved in a destination defined in a parameter. The destination could be a file system, hdfs or s3.  
Arguments(index - description):
0 - the csv file location used as input to build the model
1 - the algorithm to be used (DRF, DL and GBM)
2 - destination of the model after being serialized
Parameters:
Balance Class: when set to true, it will oversample labels. Works on all algorithms.  
Total trees: number of trees to be produced. Works on tree algorithms DRF and GBM.
Max Depth: max depth used in tree algorithms DRF and GBM.  
MTRIES: DRF Param: mtries specifies the number of features to be selected from the whole set. When set to -1, the number of features is the square root of the total number of features, rounded to the nearest integer.
Problem type: regression or classification, defaults to classification.


## H2OClassifierPredictor

The model is loaded and deserialized from a source location defined in an application parameter. The source location can be defined as a file system, hdfs or s3.  
The predictions result is uploaded to the client machine in directory build/predictions.csv
Arguments(index - description):
0 - the csv file location used as input to predict
1 - the algorithm to be used (DRF, DL and GBM)
2 - location of the model that will be loaded

## Creating and Running Spark Application

Set environment variable SPARK_HOME pointing to your home dir:  
```
export SPARK_HOME=/home/spark-1.5.2-bin-hadoop2.6/
```  

Create application assembly which can be directly submitted to Spark cluster:
```
./gradlew shadowJar
```  
The command creates jar file `build/libs/sparkling-water-geosurvey-app.jar` containing all necessary classes to run application on top of Spark cluster.

Submit H2OClassifierGeoSurvey to Spark cluster (in this case, local cluster is used). Notice that the csv file path is passed as an application parameter:  
```
$SPARK_HOME/bin/spark-submit --conf "spark.ext.h2o.client.log.level=INFO" --conf "spark.driver.extraJavaOptions=-Dntrees=$TREES -DmaxDepth=$DEPTH -DcolumnLabel=$RESPONSE_COLUMN -DcolumnExclude=$EXCLUDE_COLUMN -DresponseType=$RESPONSE_TYPE -Dmtries=$MTRIES -DbalanceClass=$BALANCE_CLASS" --class $MAIN_CLASS --master $MASTER $JAR_FILE $LOCATION $ALGORITHM $MODEL_LOCATION
```

Submit MllibClassifierGeoSurvey to Spark cluster (in this case, local cluster is used). Notice that the csv file path is passed as an application parameter::  
```
$SPARK_HOME/bin/spark-submit --conf "spark.driver.extraJavaOptions=-Dntrees=10 -DmaxDepth=12" --driver-memory 4G --executor-memory 4G --class qed.ai.MllibClassifierGeoSurvey --master local[8] build/libs/sparkling-water-geosurvey-app.jar /home/documents/crp_clean_csv
```

## Running the Applications Locally
Inside folder scripts, there are three scripts. One for each algorithm. The algorithms supported are Distributed Random   
Forest, Gradient Boost Machine and Gradient Linear Model.
 They allow you to run the applications locally in an easy way.
You can run ./run_glm.sh -h to see usage options. The following options are worth pointing out:
-l location specifies the location of the csv file.  
-r response_column indicates which csv's column is the response column.
-rt response_type specifies if the problem is regression or classification.
-a application-type current possible values are h2o, mllib, h2o-model-builder and h2o-predictor.  
Here are the mapping between application type parameter and the application that will be submitted:  
h2o - H2OClassifierGeoSurvey  
h2o-model-builder - H2OClassifierModelBuilder
h2o-predictor - H2OClassifierPredictor
mllib - MllibClassifierGeoSurvey  

A example:  
```
 $./run_gbm.sh -l ~/Documents/data_set.csv -d 15 -t 100 -r response -c cat_column,cat_Column2 -bc -e colId --learning_rate 0.3  
```

The script above will launch a local spark application using the gradient boost machine algorithm.  It is set up with   
with 100 trees, 15 max depth and learning rate 0.3. It will use cat_column and cat_column2 as a categorical column.   
Exclude csv column colId.

 

## Running the Applications in Amazon EC2
The ec2s scripts, located in scripts/ec2-integration-scripts directory, allows you to launch, manage and shut down Spark clusters on Amazon EC2.  
It automatically sets up Spark and HDFS on the cluster for you. 
To run the scripts, you need to install the [AWS Command Line Interface](http://docs.aws.amazon.com/cli/latest/userguide/installing.html)  
and [JQ](https://stedolan.github.io/jq/).
Create a file copy of conf/conf.sh.template and rename to conf.sh at the same directory.  
Required properties are:  
AWS_SECRET_ACCESS_KEY, AWS_ACCESS_KEY_ID, PEM_FILE,REGION,ZONE,TYPE_EC2_MACHINE,KEY_PAIR,VPC_ID,SUBNET_ID,S3_BUCKET_NAME,USER,FILE,CLUSTER_NAME, TOTAL_SLAVES, MASTER_INSTANCE_TYPE.  
It is required a minimum of 2 machines. One master and one cluster. TOTAL_SLAVES define the number of slave's machines.  
The S3_BUCKET_NAME defines the place where the model will be saved and restored. The aws account must have privileges in managing s3 objects.  
The FILE property specifies the csv data source. It is used during model building (for training) and in the prediction phase (for predicting).

There are three folders named flowui, glm, gbm and random_forest.  Inside each folder, there are scripts for launching   
applications in ec2 amazon: ec2-model-builder.sh, ec2-predictor.sh and ec2-flow-ui.sh. All of them can launches new clusters and destroy.
All have three options: launch, deploy and destroy. 
For instance: 
```
./gbm/ec2-model-builder.sh -a launch
```
It will launch a cluster first and then builds the model.

```
./gbm/ec2-model-builder.sh -a deploy
```
It will deploy a new application version and then builds the model.


### The ec2-model-builder.sh
- Launches a cluster or deploy a new application version (depending the -a option);  
- Create the s3 bucket defined in property S3_BUCKET_NAME;
- Upload the csv file and put it in a hdfs mounted during cluster lauch;
- Submit the job;
- Saves the model in the s3 bucket;
- Finish.


### The ec2-predictor.sh
- Launch a cluster or deploy a new application version (depending the -a option);  
- Upload the csv file for prediction and put it in a hdfs mounted during cluster lauch;
- Submit the job;
- Load the model from s3 bucked specified in property S3_BUCKET_NAME;
- Make predictions;
- Download the predictions result in folder build/predictions.csv;
- Finish.

Is is important to note that the results, under the folder predictions.csv, are in separate files part-0000, part-00001, part-0000x.  
 These files are the prediction results for each spark partition.

### The ec2-flow-ui.sh
- Launch a cluster (this script do not have the deploy property since it is a third party application);  
- Upload the csv file and put it in a hdfs mounted during cluster lauch (you can use this file in UI);
- Submit the H2O Flow Web Interface job; 
- Not finish.

The ec2-flow-ui job is not terminated. To terminate, you should access the spark ui in http://<ec2-master-url>:8080 and 
and manually kill the application.


## Dependencies
  - Spark 1.5.2
  - H2O 3.0 Shannon

For more details see [build.gradle](build.gradle).

## Project structure
 
```
├─ gradle/        - Gradle definition files  
├─ src/           - Source code  
│  ├─ main/       - Main implementation code   
│  │  ├─ scala/  
│  ├─ test/       - Test code  
│  │  ├─ scala/  
├─ build.gradle   - Build file for this project  
├─ gradlew        - Gradle wrapper  
```



## Running tests

To run tests, please, run:
```
./gradlew test
```

# Checking code style

To check codestyle:
```
./gradlew scalaStyle
```

## Starting with Idea

There are two ways to open this project in Idea

  * Using Gradle build file directly
    1. Open project's `build.gradle` in Idea via _File > Open_ 




