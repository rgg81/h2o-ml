#!/usr/bin/env bash


currentPath() {
    echo "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
}



while [ "$1" != "" ]; do
case $1 in
        -a )           shift
                       APPLICATION=$1
                       ;;
        -l )           shift
                       LOCATION=$1
                       ;;
        -t )           shift
                       TREES=$1
                       ;;
        -d )           shift
                       DEPTH=$1
                       ;;
        -r )           shift
                       RESPONSE_COLUMN=$1
                       ;;
        -rt )          shift
                       RESPONSE_TYPE=$1
                       ;;
        -e )           shift
                       EXCLUDE_COLUMN=$1
                       ;;
        -m )           shift
                       MASTER=$1
                       ;;
        -ml )          shift
                       MODEL_LOCATION=$1
                       ;;
        -mtries )      shift
                       MTRIES=$1
                       ;;
        -bdt )         BINARY_DOUBLE_TREES="true"
                       ;;
        -bc )          BALANCE_CLASS="true"
                       ;;
        -j )           shift
                       JAR_FILE=$1
                       ;;
        -c )           shift
                       CATEGORICAL_COLUMN=$1
                       ;;
        --alpha )      shift
                       GLM_ALPHA=$1
                       ;;
        --lambda )     shift
                       GLM_LAMBDA=$1
                       ;;
        --family )     shift
                       GLM_FAMILY=$1
                       ;;
        --solver )     shift
                       GLM_SOLVER=$1
                       ;;
--csv_test_dataset )   shift
                       TEST_DATA_SET=$1
                       ;;
 --learning_rate )     shift
                       LEARNING_RATE=$1
                       ;;
 --lambda_search )     GLM_LAMBDA_SEARCH="true"
                       ;;
 --not_keep_flow_ui )  NOT_KEEP_FLOW_UI="true"
                       ;;
        --nlambda )    shift
                       GLM_LAMBDA_TOTAL=$1
                       ;;
        -h )           shift
                       usage
                       ;;
        * )            QUERY=$1
    esac
    shift
done

# extra validation suggested by @technosaurus
if [ "$APPLICATION" = "h2o" ] || [ "$APPLICATION" = "" ]
then
    MAIN_CLASS=qed.ai.H2OClassifierGeoSurvey
    echo "USING CLASS $MAIN_CLASS"
elif [ "$APPLICATION" = "h2o-model-builder" ]
then
    MAIN_CLASS=qed.ai.H2OClassifierModelBuilder
    echo "USING CLASS $MAIN_CLASS"
elif [ "$APPLICATION" = "h2o-predictor" ]
then
    MAIN_CLASS=qed.ai.H2OClassifierPredictor
    echo "USING CLASS $MAIN_CLASS"
else
	MAIN_CLASS=qed.ai.MllibClassifierGeoSurvey
	echo "USING CLASS $MAIN_CLASS"
fi


if [ "$SPARK_HOME" == "" ]
then
    echo "ENVIRONMENT VARIABLE SPARK_HOME must be set."
    exit
fi


if [ "$LOCATION" == "" ]
then
    echo "CSV location must be set. Type -h for help."
    exit
fi

if [ "$MASTER" == "" ]
then
    echo "USING LOCAL MASTER local[8]"
    MASTER=local[8]
fi


if [ "$BALANCE_CLASS" == "" ]
then
    BALANCE_CLASS=false
fi

if [ "$JAR_FILE" == "" ]
then
    JAR_FILE="$( currentPath )/../build/libs/sparkling-water-geosurvey-app.jar"
fi

# generating jar
$( currentPath )/../gradlew shadowJar -p "$(currentPath)"/../

export SPARK_HOME=~/spark-2.0.1-bin-hadoop2.7