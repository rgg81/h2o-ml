#!/usr/bin/env bash

usage ()
{
  echo -e \
"Usage : run_random_forest  [-h] [-a application-type] [-l csv-location] [-t total_trees] [-d max_depth]\n\
             [-r response_column] [-e exclude_column] [-c categorical_column] [-rt response_type] [-m master_url]\n\
             [-ml model_location] [-mtries mtries] [-bc] [-j application_jar] [-bdt] [--not_keep_flow_ui] \n\
             [--csv_test_dataset test_dataset]"
  echo '             -h                    display help'
  echo "             -a application-type   current possible values are h2o, mllib, h2o-model-builder and h2o-predictor"
  echo "             -l csv-location       File csv directory"
  echo "             --csv_test_dataset csv-location       test dataset"
  echo "             -r response_column    The label/response column in csv"
  echo "             -rt response_type     The response type. Possible values are regression and classification"
  echo "             -e exclude_column     Columns in csv to exclude. Must be comma separated. Ex: column1,column2"
  echo "             -c categorical_column Categorical columns in csv. Must be comma separated. Ex: column1,column2"
  echo "             -t total_trees        Number of trees for GBM and GBT algorithms."
  echo "             -d max_depth          Maximum trees depth for GBM, GBT and DRF algorithms."
  echo "             -bdt                  (Binary classification only) Build twice as many trees (one per class). Enabling this option can lead to higher accuracy, while disabling can result in faster model building. This option is disabled by default."
  echo "             -mtries mtries        DRF Param: mtries specifies the number of features to be selected from the whole set. When set to -1, the number of features is the square root of the total number of features, rounded to the nearest integer."
  echo "             -m master_url         Master url in which the job will be submitted. Default local[8]"
  echo "             -ml model_location    The location of the model that will be loaded/exported for predictions. It is used in job h2o-predictor and h2o-model-builder"
  echo "             -bc                   When set, oversample the minority classes to balance the class distribution"
  echo "             -j application_jar    Application jar file that will be submitted to spark"
  echo "             --not_keep_flow_ui     Not Keep flow ui running. Applicable only to h2o application type."
  exit
}

CURRENT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$CURRENT_PATH"/common.sh

# generating jar
$( currentPath )/../gradlew shadowJar -p "$(currentPath)"/../
# submitting application to spark
$SPARK_HOME/bin/spark-submit --executor-memory 6G --driver-memory 2G --conf "spark.ext.h2o.client.log.level=INFO" --conf "spark.driver.extraJavaOptions=-Xmx6G -Xms6G -Dntrees=$TREES -DmaxDepth=$DEPTH -DcolumnLabel=$RESPONSE_COLUMN -DcolumnExclude=$EXCLUDE_COLUMN -DcolumnCategorical=$CATEGORICAL_COLUMN -DresponseType=$RESPONSE_TYPE -Dmtries=$MTRIES -DbalanceClass=$BALANCE_CLASS -DbinomialDoubleTrees=$BINARY_DOUBLE_TREES -DnotKeepFlowUi=$NOT_KEEP_FLOW_UI  -DcsvValidTestLocation=$TEST_DATA_SET" --class "$MAIN_CLASS" --master "$MASTER" "$JAR_FILE" "$LOCATION" DRF "$MODEL_LOCATION"
