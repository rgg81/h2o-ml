#!/usr/bin/env bash

usage ()
{
  echo -e \
"Usage : run  [-h] [-a application-type] [-l csv-location] [-r response_column] [-e exclude_column]\n\
              [-c categorical_column] [-rt response_type] [-m master_url][-ml model_location]\n\
              [-bc] [-j application_jar] [--alpha glm_alpha] [--lambda glm_lambda] [--family glm_family]\n\
              [--solver glm_solver] [--lambda_search] [--nlambda total_lambdas] [--not_keep_flow_ui]"
  echo '             -h                    display help'
  echo "             -a application-type   current possible values are h2o, mllib, h2o-model-builder and h2o-predictor"
  echo "             -l csv-location       File csv directory"
  echo "             -r response_column    The label/response column in csv"
  echo "             -rt response_type     The response type. Possible values are regression and classification"
  echo "             -e exclude_column     Columns in csv to exclude. Must be comma separated. Ex: column1,column2"
  echo "             -c categorical_column Categorical columns in csv. Must be comma separated. Ex: column1,column2"
  echo "             -m master_url         Master url in which the job will be submitted. Default local[8]"
  echo "             -ml model_location    The location of the model that will be loaded/exported for predictions. It is used in job h2o-predictor and h2o-model-builder"
  echo "             -bc                   When set, oversample the minority classes to balance the class distribution"
  echo "             -j application_jar    Application jar file that will be submitted to spark"
  echo "             --alpha glm_alpha     Alpha param for GLM algorithm. See h2o docs for a more detail explanation. Accept an array of double values. ex: 0.12,0.11"
  echo "             --lambda glm_lambda   Lambda param for GLM algorithm. See h2o docs for a more detail explanation. Accept an array of double values. ex: 0.021,0.2212"
  echo "             --family glm_family   Select the model type: gaussian, binomial, multinomial, poisson, gamma and tweedie. Default is binomial."
  echo "             --solver glm_solver   Select the solver to use: AUTO, IRLSM, L_BFGS, COORDINATE_DESCENT_NAIVE, or COORDINATE_DESCENT. Default is AUTO."
  echo "             --lambda_search       Enable lambda search, starting with lambda max. The given lambda is then interpreted as lambda min."
  echo "             --nlambda total_lambdas  Applicable only if lambda_search is enabled. Specify the number of lambdas to use in the search. The default is 100."
  echo "             --not_keep_flow_ui     Not Keep flow ui running. Applicable only to h2o application type."
  exit
}


CURRENT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$CURRENT_PATH"/common.sh

# submitting application to spark
$SPARK_HOME/bin/spark-submit --executor-memory 6G --driver-memory 2G --conf "spark.ext.h2o.client.log.level=INFO" --conf "spark.driver.extraJavaOptions=-Xmx6G -Xms6G -Dntrees=$TREES -DmaxDepth=$DEPTH -DcolumnLabel=$RESPONSE_COLUMN -DcolumnExclude=$EXCLUDE_COLUMN -DcolumnCategorical=$CATEGORICAL_COLUMN -DresponseType=$RESPONSE_TYPE -Dmtries=$MTRIES -DbalanceClass=$BALANCE_CLASS -DglmAlpha=$GLM_ALPHA -DglmLambda=$GLM_LAMBDA -DglmFamily=$GLM_FAMILY -DglmSolver=$GLM_SOLVER -DglmLambdaSearch=$GLM_LAMBDA_SEARCH -DglmNLambdas=$GLM_LAMBDA_TOTAL -DnotKeepFlowUi=$NOT_KEEP_FLOW_UI" --class "$MAIN_CLASS" --master "$MASTER" "$JAR_FILE" "$LOCATION" GLM "$MODEL_LOCATION"
