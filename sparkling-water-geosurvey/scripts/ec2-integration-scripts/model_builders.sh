#!/usr/bin/env bash

#!/usr/bin/env bash

usage () {
  echo 'Usage : ./ec2-predictions.sh [-h] [-a action]'
  echo '             -h                    display help'
  echo "             -a action             launch, destroy and deploy"
  exit
}

while [ "$1" != "" ]; do
case $1 in
        -a )           shift
                       ACTION=$1
                       ;;
        -h )           shift
                       usage
                       ;;
        * )            QUERY=$1
    esac
    shift
done


# Requirements: JAVA 7+, AWSCLI, JQ
CURRENT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$CURRENT_PATH"/conf/funcs.sh
initVariables

aws --region "$REGION" s3 rb "s3://$S3_BUCKET_NAME" --force
aws --region "$REGION" s3 mb "s3://$S3_BUCKET_NAME"

if [ "$ACTION" = "launch" ] || [ "$ACTION" = "" ]
then
    launchCluster
    # create inbound for h2o flow web interface
    openAwsFlowUIPort
    masterHostName
    uploadAndSubmit
elif [ "$ACTION" = "destroy" ]
then
	# destroy cluster
	destroyCluster
elif [ "$ACTION" = "deploy" ]
then
    masterHostName
    scp -o StrictHostKeyChecking=no -i "$PEM_FILE" "$CURRENT_PATH"/../../../build/libs/sparkling-water-geosurvey-app.jar "$USER@$REMOTE_HOST":/libs/
	uploadAndSubmit
else
   echo "action value not identified. Permitted values are: launch, destroy and deploy."
fi




