#!/usr/bin/env bash

uploadAndSubmit() {
    # copy csv data set file to remote and then put in the hadoop file system which is mounted by the launch ec2 script
    uploadFileToCluster
    cat "$CURRENT_PATH"/../../common.sh | ssh -o "StrictHostKeyChecking no" -i "$PEM_FILE" "$USER@$REMOTE_HOST" \
     "cat > /tmp/common.sh ; chmod 755 /tmp/common.sh"

    cat "$CURRENT_PATH"/../../run_gbm.sh | ssh -o "StrictHostKeyChecking no" -i "$PEM_FILE" "$USER@$REMOTE_HOST" \
     "cat > /tmp/run.sh ; chmod 755 /tmp/run.sh ;export SPARK_HOME=~/spark && export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
      && export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY && /tmp/run.sh \
       $(testParam $RESPONSE_COLUMN r) $(testParam $EXCLUDE_COLUMNS e) $(testParam $RESPONSE_TYPE rt) \
       $(testParam $LEARNING_RATE -learning_rate) \
       $(testParam $N_TREES t) $(testParam $MAX_DEPTH d) \
       $(testParamBoolean $BALANCE_CLASS bc) $(testParam $CATEGORICAL_COLUMN c) -a h2o-model-builder -m spark://$REMOTE_HOST:7077 -j /libs/sparkling-water-geosurvey-app.jar -l hdfs:///$FILE_NAME  -ml s3n://$S3_BUCKET_NAME/"
}

CURRENT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$CURRENT_PATH"/../model_builders.sh




