#!/usr/bin/env bash

usage ()
{
  echo 'Usage : ./ec2-predictions.sh [-h] [-a action]'
  echo '             -h                    display help'
  echo "             -a action             launch or destroy"
  exit
}

while [ "$1" != "" ]; do
case $1 in
        -a )           shift
                       ACTION=$1
                       ;;
        -h )           shift
                       usage
                       ;;
        * )            QUERY=$1
    esac
    shift
done

# Requirements: JAVA 7+, AWSCLI, JQ
CURRENT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$CURRENT_PATH"/funcs.sh
initVariables

if [ "$ACTION" = "launch" ] || [ "$ACTION" = "" ]
then
    launchCluster
    # create inbound for h2o flow web interface
    openAwsFlowUIPort
    masterHostName
    uploadFileToCluster
    # Start Flow Ui
    ssh -o "StrictHostKeyChecking no" -i "$PEM_FILE" "$USER@$REMOTE_HOST" "export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID && export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY && spark/bin/spark-submit --class water.SparklingWaterDriver --total-executor-cores 2 --master spark://$REMOTE_HOST:7077 /libs/sparkling-water-geosurvey-app.jar"
else
	destroyCluster
fi

