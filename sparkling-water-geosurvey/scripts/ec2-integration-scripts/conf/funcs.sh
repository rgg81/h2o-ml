#!/usr/bin/env bash

# variables are set in conf.sh
launchCluster() {
    $SPARK_HOME/ec2/spark-ec2 --deploy-root-dir="$CURRENT_PATH"/../../../build/libs --key-pair="$KEY_PAIR" --identity-file="$PEM_FILE" --region="$REGION" --instance-type="$TYPE_EC2_MACHINE" -m "$MASTER_INSTANCE_TYPE" --vpc-id="$VPC_ID" --subnet-id="$SUBNET_ID" --zone="$ZONE" -s "$TOTAL_SLAVES" --copy-aws-credentials launch "$CLUSTER_NAME"
    rc=$?; if [[ $rc != 0 ]]; then exit $rc; fi
}

destroyCluster() {
    yes | $SPARK_HOME/ec2/spark-ec2 --key-pair="$KEY_PAIR" --identity-file="$PEM_FILE" --delete-groups --region="$REGION" --vpc-id="$VPC_ID" --subnet-id="$SUBNET_ID" --zone="$ZONE" destroy "$CLUSTER_NAME"
}
testParam() {
    if [ ! -z "$2" ]
    then
        echo "-$2 $1";
    fi
}

testParamBoolean() {
    if [ "$1" == "true" ]
    then
        echo "-$2";
    fi
}


currentPath() {
    echo "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
}
initVariables() {
    hash jq 2>/dev/null || { echo >&2 "Script requires application jq but it's not installed.  Aborting."; exit 1; }
    hash aws 2>/dev/null || { echo >&2 "Script requires aws but it's not installed.  Aborting."; exit 1; }

    # All the environment variables are set in conf.sh
    CURRENT_PATH="$( currentPath )"
    source "$CURRENT_PATH"/conf.sh

    for var in AWS_SECRET_ACCESS_KEY AWS_ACCESS_KEY_ID PEM_FILE REGION ZONE TYPE_EC2_MACHINE KEY_PAIR VPC_ID SUBNET_ID S3_BUCKET_NAME USER FILE CLUSTER_NAME TOTAL_SLAVES MASTER_INSTANCE_TYPE ; do
        if [ ! -n "${!var}" ] ; then
            echo "$var is not set. Check if you create a conf.sh from template or if it is missing $var variable."
            exit 1
        fi
    done

    # Downloading spark
    cd $HOME
    if [ ! -d "$HOME/spark-1.5.1-bin-hadoop1" ]; then
      wget http://d3kbcqa49mib13.cloudfront.net/spark-1.5.1-bin-hadoop1.tgz
      tar -zxf spark-1.5.1-bin-hadoop1.tgz
    fi
    cd spark-1.5.1-bin-hadoop1
    SPARK_HOME=$HOME/spark-1.5.1-bin-hadoop1

    cd "$CURRENT_PATH"

    "$CURRENT_PATH"/../../../gradlew shadowJar -p "$CURRENT_PATH"/../../../
    export SPARK_HOME
    export AWS_ACCESS_KEY_ID
    export AWS_SECRET_ACCESS_KEY

    FILE_NAME=`basename "$FILE"`
}

openAwsFlowUIPort() {
    GROUP_ID=`aws ec2 describe-security-groups --filters Name=group-name,Values="$CLUSTER_NAME"-master | jq -r '.SecurityGroups[0].GroupId'`
    aws --region "$REGION" ec2 authorize-security-group-ingress --group-id "$GROUP_ID" --protocol tcp --port 54321 --cidr 0.0.0.0/0
}

masterHostName() {
    # get master host name
    RETURN_MESSAGE=`"$SPARK_HOME"/ec2/spark-ec2 --key-pair="$KEY_PAIR" --identity-file="$PEM_FILE" --region="$REGION" get-master "$CLUSTER_NAME"`
    # get third line of the message
    REMOTE_HOST=`echo "$RETURN_MESSAGE" | awk 'NR==3'`
}
uploadFileToCluster() {
    # copy csv data set file to remote and then put in the hadoop file system which is mounted by the launch ec2 script
    scp -o StrictHostKeyChecking=no -i "$PEM_FILE" "$FILE" "$USER@$REMOTE_HOST":~/
    # remove old csv file
    ssh -o "StrictHostKeyChecking no" -i "$PEM_FILE" "$USER@$REMOTE_HOST" "ephemeral-hdfs/bin/hadoop fs -rm /$FILE_NAME"
    # remove old predictions in hdfs
    ssh -o "StrictHostKeyChecking no" -i "$PEM_FILE" "$USER@$REMOTE_HOST" "ephemeral-hdfs/bin/hadoop fs -rmr /predictions.csv"
    # remove old predictions in disk
    ssh -o "StrictHostKeyChecking no" -i "$PEM_FILE" "$USER@$REMOTE_HOST" "rm -rf /root/predictions.csv"
    # copy current csv file into hdfs
    ssh -o "StrictHostKeyChecking no" -i "$PEM_FILE" "$USER@$REMOTE_HOST" "ephemeral-hdfs/bin/hadoop fs -copyFromLocal  /root/$FILE_NAME /"
}