#!/usr/bin/env bash


uploadAndSubmit() {

    # copy csv data set file to remote and then put in the hadoop file system which is mounted by the launch ec2 script
    uploadFileToCluster
    # submit job to generate the model and save to s3 bucket
    cat "$CURRENT_PATH"/../../common.sh | ssh -o "StrictHostKeyChecking no" -i "$PEM_FILE" "$USER@$REMOTE_HOST" \
     "cat > /tmp/common.sh ; chmod 755 /tmp/common.sh"

    cat "$CURRENT_PATH"/../../run_glm.sh | ssh -o "StrictHostKeyChecking no" -i "$PEM_FILE" "$USER@$REMOTE_HOST" \
     "cat > /tmp/run.sh ; chmod 755 /tmp/run.sh ;export SPARK_HOME=~/spark && export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
      && export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY && /tmp/run.sh $(testParam $GLM_ALPHA -alpha) $(testParam $GLM_LAMBDA -lambda) \
       $(testParam $GLM_FAMILY -family) $(testParam $GLM_SOLVER -solver) $(testParamBoolean $GLM_LAMBDA_SEARCH -lambda_search) \
        $(testParam $GLM_SOLVER -solver) $(testParam $GLM_LAMBDA_TOTAL -nlambda) $(testParam $RESPONSE_COLUMN r) $(testParam $EXCLUDE_COLUMNS e) $(testParam $RESPONSE_TYPE rt) \
         $(testParamBoolean $BALANCE_CLASS bc) $(testParam $CATEGORICAL_COLUMN c) --not_keep_flow_ui -a h2o -m spark://$REMOTE_HOST:7077 -j /libs/sparkling-water-geosurvey-app.jar -l hdfs:///$FILE_NAME -al GLM -ml s3n://$S3_BUCKET_NAME/"

   # merge all prediction file parts into one file
    ssh -o "StrictHostKeyChecking no" -i "$PEM_FILE" "$USER@$REMOTE_HOST" "ephemeral-hdfs/bin/hadoop fs -getmerge hdfs:///predictions.csv /root/predictions.csv"
    # copy the remote prediction file to local directory
    scp -o StrictHostKeyChecking=no -i "$PEM_FILE" "$USER@$REMOTE_HOST":/root/predictions.csv "$CURRENT_PATH"/../../../build/libs/

}

CURRENT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$CURRENT_PATH"/../model_builders.sh