/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// scalastyle:off println
package qed.ai

import com.typesafe.config.ConfigFactory
import org.apache.spark.sql.SQLContext
import org.apache.spark.{SparkConf, SparkContext}

import scala.util.Try

// $example on$
import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.classification.{GBTClassifier, RandomForestClassifier}
import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator
import org.apache.spark.ml.feature.{IndexToString, StringIndexer, VectorAssembler, VectorIndexer}
// $example off$

object MllibClassifierGeoSurvey {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("MllibClassifierGeoSurvey")
    val sc = new SparkContext(conf)
    val sqlContext = new SQLContext(sc)

    // Config object for using application.conf file
    val configProps = ConfigFactory.load()

    // $example on$
    // Load and parse the data file, converting it to a DataFrame.
    val data = sqlContext.read.format("com.databricks.spark.csv").option("header","true").option("inferSchema","true")
      .load(args(0))

    val algorithm = Try(Algorithm.withName(args(1))).getOrElse(Algorithm.GBT)

    val assembler = new VectorAssembler()
      .setInputCols(Array("GWCRP_loc","AFPOPs_loc","AFPOPs_se","AFPOPs_e","GWCRP_e",
        "AFPOPs_sw","AFPOPs_s","GWCRP_w","GWCRP_s","AFPOPs_nw","AFPOPs_n","AFPOPs_w","LSTNs_e",
        "GWCRP_n","AFPOPs_ne","LSTNs_se","LSTNs_loc","GWCRP_se","GWCRP_nw","GWCRP_ne","LSTNs_s",
        "LSTNs_ne","REF7s_loc","GWCRP_sw","LSTNs_nw","LSTNs_n","LSTNs_w","BSASs_loc","LSTNs_sw",
        "BSANs_loc","REF1s_loc"))
      .setOutputCol("features")

    val transformed = assembler.transform(data)

    // Index labels, adding metadata to the label column.
    // Fit on whole dataset to include all labels in index.
    val labelIndexerTraining = new StringIndexer()
      .setInputCol("Cropland_present")
      .setOutputCol("indexedLabel")
      .fit(transformed)


    // Automatically identify categorical features, and index them.
    // Set maxCategories so features with > 4 distinct values are treated as continuous.
    val featureIndexerTraining = new VectorIndexer()
      .setInputCol("features")
      .setOutputCol("indexedFeatures")
      .setMaxCategories(5)
      .fit(transformed)

    val Array(trainingData, testData) = transformed.randomSplit(Array(0.85, 0.15))

    // Convert indexed labels back to original labels.
    val labelConverter = new IndexToString()
      .setInputCol("prediction")
      .setOutputCol("predictedLabel")
      .setLabels(labelIndexerTraining.labels)

    // Select (prediction, true label) and compute test error
    val evaluator = new MulticlassClassificationEvaluator()
      .setLabelCol("indexedLabel")
      .setPredictionCol("prediction")
      .setMetricName("precision")

    if(algorithm == Algorithm.GBT || algorithm == Algorithm.ALL) {
      //     Train a GBT model.
      val gbt = new GBTClassifier()
        .setLabelCol("indexedLabel")
        .setFeaturesCol("indexedFeatures")
        .setMaxDepth(configProps.getInt("algorithm.gbt.maxDepth"))
        .setMaxIter(configProps.getInt("algorithm.gbt.ntrees"))
      // Chain indexers and forest in a Pipeline
      val pipelineGbt = new Pipeline()
        .setStages(Array(labelIndexerTraining, featureIndexerTraining, gbt, labelConverter))
      // Train model.  This also runs the indexers.
      val modelGbt = pipelineGbt.fit(trainingData)
      // Make predictions.
      val predictionsGbt = modelGbt.transform(testData)
      val accuracyGbt = evaluator.evaluate(predictionsGbt)
      println("Accuracy GBT = " + accuracyGbt)
    }

    if(algorithm == Algorithm.RF || algorithm == Algorithm.ALL) {
      // Train a RandomForest model.
      val rf = new RandomForestClassifier()
        .setLabelCol("indexedLabel")
        .setFeaturesCol("indexedFeatures")
        .setNumTrees(configProps.getInt("algorithm.gbt.ntrees"))
        .setMaxDepth(configProps.getInt("algorithm.gbt.maxDepth"))
      // Chain indexers and forest in a Pipeline
      val pipelineRf = new Pipeline()
        .setStages(Array(labelIndexerTraining, featureIndexerTraining, rf, labelConverter))
      val modelRf = pipelineRf.fit(trainingData)
      val predictionsRf = modelRf.transform(testData)
      val accuracyRf = evaluator.evaluate(predictionsRf)
      println("Accuracy RF = " + accuracyRf)
    }


    sc.stop()
  }
  object Algorithm extends Enumeration {
    type Algorithm = Value
    val GBT = Value("GBT")
    val RF = Value("RF")
    val ALL = Value("ALL")
  }
}
// scalastyle:on println
