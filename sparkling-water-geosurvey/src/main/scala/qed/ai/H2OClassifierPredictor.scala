package qed.ai

import java.net.URI

import com.typesafe.config.ConfigFactory
import hex.Model
import hex.deeplearning.DeepLearningModel
import hex.glm.GLMModel
import hex.tree.drf.DRFModel
import hex.tree.gbm.GBMModel
import org.apache.log4j.Logger
import org.apache.spark.h2o._
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{DataFrame, Row, SQLContext, SaveMode}
import org.apache.spark.{SparkContext, h2o}
import qed.ai.H2OClassifierGeoSurvey._
import water.fvec.H2OFrame
import water.support.{ModelMetricsSupport, ModelSerializationSupport, SparkContextSupport, SparkSessionSupport}

import scala.util.{Failure, Try}


object H2OClassifierPredictor extends SparkContextSupport with SparkSessionSupport with ModelMetricsSupport with ModelSerializationSupport {
  private val logger = Logger.getLogger(this.getClass)

  def main(args: Array[String]): Unit = {
    val conf = configure("H2OClassifierPredictor")
    val sc = new SparkContext(conf)

    implicit val sql = sqlContext
    // Create H2O Context
    implicit val h2oContext = H2OContext.getOrCreate(sc)
    // Config object for using application.conf file
    implicit val configProps = ConfigFactory.load()

    try {
      // Load data and parse it via h2o parser
      val h20Table = H2OClassifierModelBuilder.prepareData(args(0))

      val algorithm = Try(Algorithm.withName(args(1))).getOrElse(Algorithm.GBM)
      val urlModelStore = Try(new URI(args(2))).getOrElse(H2OClassifierModelBuilder.defaultURI)

      if(algorithm == Algorithm.GBM || algorithm == Algorithm.ALL) {
        // -- Run GBM
        val model = loadH2OModel[GBMModel](urlModelStore)
        predictAndWrite(model,h20Table)

      }
      if(algorithm == Algorithm.DL || algorithm == Algorithm.ALL) {
        // -- Run DeepLearning
        val model = loadH2OModel[DeepLearningModel](urlModelStore)
        predictAndWrite(model,h20Table)
      }

      if(algorithm == Algorithm.DRF || algorithm == Algorithm.ALL) {
        // -- Run Distributed Random Forest
        val model = loadH2OModel[DRFModel](urlModelStore)
        predictAndWrite(model,h20Table)
      }
      if(algorithm == Algorithm.GLM || algorithm == Algorithm.ALL) {
        // -- Run GLM
        val model = loadH2OModel[GLMModel](urlModelStore)
        predictAndWrite(model,h20Table)
      }
    } catch {
      case e:Exception =>
        logger.error("error in running job", e)
    } finally {
      // Shutdown Spark cluster and H2O
      h2oContext.stop(stopSparkContext = true)
    }
  }

  def predictAndWrite(model:Model[_,_,_],h20Table:H2OFrame)(implicit h2oContext: H2OContext, sqlContext: SQLContext) = {
    import h2oContext._

    val predictTableGbm = model.score(h20Table)
    val h2oFrame = new h2o.H2OFrame(predictTableGbm)

    val dfPredicted = asDataFrame(h2oFrame)
    val tested = asDataFrame(h20Table)

    val zipped = dfPredicted.rdd.zip(tested.rdd).map {
      case (row1,row2) =>
        Row.fromSeq(row1.toSeq ++ row2.toSeq)
    }

  // Merge schemas
  val schema = StructType(dfPredicted.schema.fields ++ tested.schema.fields)

  // Create new data frame
  val joined: DataFrame = sqlContext.createDataFrame(zipped, schema)


    val saveHdfs = Try(dfPredicted.write
      .format("com.databricks.spark.csv")
      .option("header", "true")
      .mode(SaveMode.Overwrite)
      .save(s"hdfs:///predictions.csv"))
    saveHdfs match {
      // saving in local file system
      case Failure(_) =>
        joined.write
        .format("com.databricks.spark.csv")
        .option("header", "true")
        .mode(SaveMode.Overwrite)
        .save(s"file:///${System.getProperty("user.dir")}/build/predictions.csv")
      case _ =>
    }
  }


}
