package qed.ai

import com.typesafe.config.Config
import hex.deeplearning.DeepLearningModel
import hex.deeplearning.DeepLearningModel.DeepLearningParameters
import hex.deeplearning.DeepLearningModel.DeepLearningParameters.Activation
import hex.genmodel.utils.DistributionFamily
import hex.glm.GLMModel.GLMParameters
import hex.glm.GLMModel.GLMParameters.{Family, Link, Solver}
import hex.glm.{GLM, GLMModel}
import hex.tree.drf.DRFModel
import hex.tree.gbm.GBMModel
import org.apache.spark.h2o.H2OContext
import water.fvec.H2OFrame

import scala.util.Try

/**
  * Created by roberto on 4/3/16.
  */
object Models {

  def GBMParamsProperty(implicit configProps:Config) = {
    val learningRate = Try(configProps.getString("algorithm.gbm.learningRate").toFloat).getOrElse(0.1f)
    learningRate
  }

  def GBMModel(train: H2OFrame, valid: Option[H2OFrame], response: String,
               nTrees:Int = 10, depth:Int = 20, family: DistributionFamily = DistributionFamily.AUTO,
               balanceClasses:Boolean = false, learningRate:Float = 0.1f)
              (implicit h2oContext: H2OContext, configProps:Config): GBMModel = {
    import hex.tree.gbm.GBM
    import hex.tree.gbm.GBMModel.GBMParameters

    val columnNameExclude = Try(configProps.getString("csv.column.exclude"))
    val gbmParams = new GBMParameters()
    gbmParams._train = train.key
    for {
      validData <- valid
    } yield gbmParams._valid = validData.key
    gbmParams._response_column = response
    gbmParams._ntrees = nTrees
    gbmParams._learn_rate = learningRate
    gbmParams._max_depth = depth
    gbmParams._distribution = family
    gbmParams._balance_classes = balanceClasses
    columnNameExclude.map {
      case names =>
        gbmParams._ignored_columns = names.split(",")
    }


    val gbm = new GBM(gbmParams)
    val model = gbm.trainModel.get
    model
  }

  def DRFParamsProperty(implicit configProps:Config) = {
    val drfBinomialDoubleTrees = Try(configProps.getString("algorithm.drf.binomialDoubleTrees").toBoolean).getOrElse(false)
    val mtries = Try(configProps.getInt("algorithm.drf.mtries")).getOrElse(-1)
    (drfBinomialDoubleTrees,mtries)
  }

  def DRFModel(train: H2OFrame, valid: Option[H2OFrame], response: String,
               nTrees:Int = 10, depth:Int = 6, family: DistributionFamily = DistributionFamily.AUTO,
               binomialDoubleTrees: Boolean = false,
               mtries:Int = -1, balanceClasses:Boolean = false)
              (implicit h2oContext: H2OContext, configProps:Config) : DRFModel = {
    import hex.tree.drf.DRF
    import hex.tree.drf.DRFModel.DRFParameters

    val columnNameExclude = Try(configProps.getString("csv.column.exclude"))

    val drfParams = new DRFParameters()
    drfParams._train = train.key
    for {
      validData <- valid
    } yield drfParams._valid = validData.key
    drfParams._response_column = response
    drfParams._ntrees = nTrees
    drfParams._binomial_double_trees = binomialDoubleTrees
    drfParams._max_depth = depth
    drfParams._distribution = family
    columnNameExclude.map {
      case names =>
        drfParams._ignored_columns = names.split(",")
    }

    drfParams._mtries = mtries
    drfParams._balance_classes = balanceClasses


    val drf = new DRF(drfParams)
    val model = drf.trainModel.get
    model
  }

  def DLModel(train: H2OFrame, valid: Option[H2OFrame], response: String,
              epochs: Int = 10, l1: Double = 0.0001, l2: Double = 0.0001,
              activation: Activation = Activation.RectifierWithDropout,
              hidden:Array[Int] = Array(100,100), family: DistributionFamily = DistributionFamily.AUTO,
              balanceClasses:Boolean = false)
             (implicit h2oContext: H2OContext, configProps:Config) : DeepLearningModel = {
    import hex.deeplearning.DeepLearning

    val columnNameExclude = Try(configProps.getString("csv.column.exclude"))

    val dlParams = new DeepLearningParameters()
    dlParams._train = train.key
    for {
      validData <- valid
    } yield dlParams._valid = validData.key

    dlParams._response_column = response
    dlParams._epochs = epochs
    dlParams._l1 = l1
    dlParams._l2 = l2
    dlParams._activation = activation
    dlParams._hidden = hidden
    dlParams._distribution = family
    dlParams._reproducible = true
    dlParams._force_load_balance = false
    dlParams._balance_classes = balanceClasses
    columnNameExclude.map {
      case names =>
        dlParams._ignored_columns = names.split(",")
    }

    // Create a job
    val dl = new DeepLearning(dlParams)
    val model = dl.trainModel.get
    model
  }

  def GLMParamsProperty(implicit configProps:Config) = {
    val alphaParam = Try(configProps.getString("algorithm.glm.alpha").split(",").map(_.toDouble)).getOrElse(null)
    val lambdaParam = Try(configProps.getString("algorithm.glm.lambda").split(",").map(_.toDouble)).getOrElse(null)
    val glmFamily = Try(GLMParameters.Family.valueOf(configProps.getString("algorithm.glm.family"))).getOrElse(GLMParameters.Family.binomial)
    val glmSolver = Try(Solver.valueOf(configProps.getString("algorithm.glm.solver"))).getOrElse(Solver.AUTO)
    val glmLambdaSearch = Try(configProps.getString("algorithm.glm.lambdaSearch").toBoolean).getOrElse(false)
    val glmNLambdas = Try(configProps.getString("algorithm.glm.nLambdas").toInt).getOrElse(100)
    (alphaParam,lambdaParam,glmFamily,glmSolver,glmLambdaSearch,glmNLambdas)
  }

  def GLMModel(train: H2OFrame, valid: Option[H2OFrame], response: String,
               alpha: Array[Double] = null,
               lambda:Array[Double] = null,
               family: GLMParameters.Family = GLMParameters.Family.binomial,
               solver: GLMParameters.Solver = GLMParameters.Solver.IRLSM,
               tweedieVariancePower: Double = 0,
               tweedieLinkPower: Double = 1,
               lambdaSearch:Boolean = false,
               maxIterations:Int = -1,
               link:Link = Link.family_default,
//               Applicable only if lambda_search is enabled
               nLambdas:Int = 100,
               standardize:Boolean = true,
               nonNegative:Boolean = false,
               balanceClasses:Boolean = false)
              (implicit h2oContext: H2OContext, configProps:Config) : GLMModel = {
    val columnNameExclude = Try(configProps.getString("csv.column.exclude"))

    val glmParams = new GLMParameters()
    glmParams._train = train.key
    for {
      validData <- valid
    } yield glmParams._valid = validData.key

    glmParams._response_column = response
    glmParams._alpha = alpha
    glmParams._lambda = lambda
    glmParams._lambda_search = lambdaSearch
    glmParams._nlambdas = nLambdas

    glmParams._max_iterations = maxIterations
    glmParams._link = link

    glmParams._family = family
    glmParams._solver = solver

    glmParams._standardize = standardize
    glmParams._non_negative = nonNegative

    glmParams._balance_classes = balanceClasses
    columnNameExclude.map {
      case names =>
        glmParams._ignored_columns = names.split(",")
    }

    // Create a job
    val glm = new GLM(glmParams)
    val model = glm.trainModel.get
    model
  }
}
