package qed.ai

import com.typesafe.config.ConfigFactory
import hex.splitframe.ShuffleSplitFrame
import org.apache.log4j.Logger
import org.apache.spark.SparkContext
import org.apache.spark.h2o._
import qed.ai.Models._
import water.Key
import water.fvec.H2OFrame
import water.support.{ModelMetricsSupport, SparkContextSupport, SparkSessionSupport}

import scala.util.{Success, Try}


object H2OClassifierGeoSurvey extends SparkContextSupport with SparkSessionSupport with ModelMetricsSupport {
  private val logger = Logger.getLogger(this.getClass)

  def main(args: Array[String]): Unit = {

    val conf = configure("H2OClassifierGeoSurvey")
    val sc = new SparkContext(conf)
    implicit val sql = sqlContext

    // Config object for using application.conf file
    implicit val configProps = ConfigFactory.load()

    // Create H2O Context
    implicit @transient val h2oContext = H2OContext.getOrCreate(sc)
    try {

      // Load data and parse it via h2o parser
      // Load and parse the data file, converting it to a DataFrame.
      val h20Table = H2OClassifierModelBuilder.prepareData(args(0))

      val algorithm = Try(Algorithm.withName(args(1))).getOrElse(Algorithm.GBM)
      val balanceClass_? = Try(configProps.getString("algorithm.balanceClass").toBoolean).getOrElse(false)
      val csvTestValidDataSetLocation = Try(configProps.getString("csv.validTestLocation"))

      val (train, valid, test) = csvTestValidDataSetLocation match {
        case Success(value) if !value.isEmpty =>
          val h20TableTestDataSet = H2OClassifierModelBuilder.prepareData(value)
          val keys = Array[String]("valid.hex", "test.hex")
          val ratios = Array[Double](0.9, 0.1)
          val frs = splitFrame(h20TableTestDataSet, keys, ratios)
          (h20Table, new H2OFrame(frs(0)), new H2OFrame(frs(1)))
        case _ =>
          val keys = Array[String]("train.hex", "valid.hex", "test.hex")
          val ratios = Array[Double](0.7, 0.1, 0.2)
          val frs = splitFrame(h20Table, keys, ratios)
          (H2OFrame(frs(0)), H2OFrame(frs(1)), H2OFrame(frs(2)))
      }


      if(algorithm == Algorithm.GBM || algorithm == Algorithm.ALL) {
        // -- Run GBM
        val learningRateGBM = GBMParamsProperty
        val gbmModel = GBMModel(train, Some(valid), "cat_output",configProps.getInt("algorithm.gbm.ntrees"),
          configProps.getInt("algorithm.gbm.maxDepth"), balanceClasses = balanceClass_?, learningRate = learningRateGBM)
        H2OClassifierPredictor.predictAndWrite(gbmModel,test)
      }


      if(algorithm == Algorithm.DL || algorithm == Algorithm.ALL) {
        // -- Run DeepLearning
        val dlModel = DLModel(train, Some(valid),"cat_output", balanceClasses = balanceClass_?)
        H2OClassifierPredictor.predictAndWrite(dlModel,test)
      }

      if(algorithm == Algorithm.GLM || algorithm == Algorithm.ALL) {
        // -- Run GLM
        val (alphaParam,lambdaParam,glmFamily,glmSolver,glmLambdaSearch,glmNLambdas) = GLMParamsProperty

        val glmModel = GLMModel(train, Some(valid),"cat_output",
          balanceClasses = balanceClass_?,
          family = glmFamily,
          alpha = alphaParam,
          solver = glmSolver,
          lambda = lambdaParam,
          lambdaSearch = glmLambdaSearch,
          nLambdas = glmNLambdas)
        H2OClassifierPredictor.predictAndWrite(glmModel,test)
      }


      if(algorithm == Algorithm.DRF || algorithm == Algorithm.ALL) {
        // -- Run Distributed Random Forest
        val (drfBinomialDoubleTrees,drfMtries) = DRFParamsProperty


        val drfModel = DRFModel(train, Some(valid),"cat_output",configProps.getInt("algorithm.drf.ntrees"),
          configProps.getInt("algorithm.drf.maxDepth"), mtries = drfMtries,
          balanceClasses = balanceClass_?, binomialDoubleTrees = drfBinomialDoubleTrees)
        H2OClassifierPredictor.predictAndWrite(drfModel,test)
      }

      val notKeepFlowUi = Try(configProps.getString("notKeepFlowUi").toBoolean).getOrElse(false)
      if(!notKeepFlowUi) {
        // Infinite wait
        this.synchronized(while (true) {
          this.wait
        })
      }
    } catch {
      case e:Exception =>
        logger.error("error in running job", e)
    }
    finally {
      // Shutdown Spark cluster and H2O
      h2oContext.stop(stopSparkContext = true)
    }

  }

  def splitFrame(df: H2OFrame, keys: Seq[String], ratios: Seq[Double]): Array[Frame] = {
    val ks = keys.map(Key.make[Frame](_)).toArray
    val frs = ShuffleSplitFrame.shuffleSplitFrame(df, ks, ratios.toArray, System.nanoTime())
    frs
  }



  object Algorithm extends Enumeration {
    type Algorithm = Value
    val GBM = Value("GBM")
    val DL = Value("DL")
    val GLM = Value("GLM")
    val DRF = Value("DRF")
    val ALL = Value("ALL")
  }
}
