package qed.ai

import java.net.URI

import com.typesafe.config.{Config, ConfigFactory}
import org.apache.log4j.Logger
import org.apache.spark.SparkContext
import org.apache.spark.h2o._
import org.apache.spark.sql.SQLContext
import qed.ai.H2OClassifierGeoSurvey._
import qed.ai.Models._
import water.fvec.H2OFrame
import water.support.{ModelMetricsSupport, ModelSerializationSupport, SparkContextSupport, SparkSessionSupport}

import scala.util.{Success, Try}


object H2OClassifierModelBuilder extends SparkContextSupport with SparkSessionSupport with ModelMetricsSupport with ModelSerializationSupport {
  private val logger = Logger.getLogger(this.getClass)
  val defaultURI = new URI(s"file:///${System.getProperty("user.dir")}/build/model/")
  val defaultColumnLabelIndex = 0

  def main(args: Array[String]): Unit = {
    val conf = configure("H2OClassifierModelBuilder")
    val sc = new SparkContext(conf)

    implicit val sql = sqlContext
    // Config object for using application.conf file
    implicit val configProps = ConfigFactory.load()

    // Create H2O Context
    implicit val h2oContext = H2OContext.getOrCreate(sc)

    try {
      val h20Table = prepareData(args(0))
      val algorithm = Try(Algorithm.withName(args(1))).getOrElse(Algorithm.GBM)
      val urlModelStore = Try(new URI(args(2))).getOrElse(defaultURI)
      val balanceClass_? = Try(configProps.getString("algorithm.balanceClass").toBoolean).getOrElse(false)


      if(algorithm == Algorithm.GBM || algorithm == Algorithm.ALL) {
        // -- Run GBM
        val gbmModel = GBMModel(h20Table, None, "cat_output",configProps.getInt("algorithm.gbm.ntrees"),
          configProps.getInt("algorithm.gbm.maxDepth"),balanceClasses = balanceClass_?)
        exportH2OModel(gbmModel,urlModelStore)

      }
      if(algorithm == Algorithm.DL || algorithm == Algorithm.ALL) {
        // -- Run DeepLearning
        val dlModel = DLModel(h20Table, None,"cat_output", balanceClasses = balanceClass_?)
        exportH2OModel(dlModel,urlModelStore)
      }


      if(algorithm == Algorithm.DRF || algorithm == Algorithm.ALL) {
        // -- Run Distributed Random Forest
        val (drfBinomialDoubleTrees,drfMtries) = DRFParamsProperty

        val drfModel = DRFModel(h20Table, None,"cat_output",configProps.getInt("algorithm.gbm.ntrees"),
          configProps.getInt("algorithm.gbm.maxDepth"), mtries = drfMtries,
          balanceClasses = balanceClass_?,binomialDoubleTrees = drfBinomialDoubleTrees)
        exportH2OModel(drfModel,urlModelStore)
      }

      if(algorithm == Algorithm.GLM || algorithm == Algorithm.ALL) {
        // -- Run GLM
        val (alphaParam,lambdaParam,glmFamily,glmSolver,glmLambdaSearch,glmNLambdas) = GLMParamsProperty

        val glmModel = GLMModel(h20Table, None,"cat_output",
          balanceClasses = balanceClass_?,
          family = glmFamily,
          alpha = alphaParam,
          solver = glmSolver,
          lambda = lambdaParam,
          lambdaSearch = glmLambdaSearch,
          nLambdas = glmNLambdas)
        exportH2OModel(glmModel,urlModelStore)
      }

    } catch {
      case e:Exception =>
        logger.error("error in running job", e)
    } finally {
      // Shutdown Spark cluster and H2O
      h2oContext.stop(stopSparkContext = true)
    }

  }


  def prepareData(dataSetLocation:String)(implicit sqlContext: SQLContext, h2oContext: H2OContext, configProps:Config): H2OFrame = {
    import h2oContext._
    // Load data and parse it via h2o parser
    // Load and parse the data file, converting it to a DataFrame.
    val data = sqlContext.read.format("com.databricks.spark.csv").option("header","true").option("inferSchema","true")
      .load(dataSetLocation)
    val h20Table = asH2OFrame(data)

    val columnNameLabel = Try(configProps.getString("csv.column.label"))

    val classificationOrRegression = Try(configProps.getString("algorithm.responseType"))
    val columnCategorical = Try(configProps.getString("csv.column.categorical"))

    (columnNameLabel,classificationOrRegression) match {
      case (Success(value),Success("classification")) if ! value.isEmpty =>
        h20Table.add("cat_output",h20Table.vec(value).toCategoricalVec)
        h20Table.remove(value)
      case (Success(value),Success("regression")) if ! value.isEmpty =>
        h20Table.add("cat_output",h20Table.vec(value))
        h20Table.remove(value)
      case (Success(value),_) if ! value.isEmpty =>
        h20Table.add("cat_output",h20Table.vec(value).toCategoricalVec)
        h20Table.remove(value)
      case (_,Success("classification")) =>
        h20Table.add("cat_output",h20Table.vec(defaultColumnLabelIndex).toCategoricalVec)
        h20Table.remove(defaultColumnLabelIndex)
      case (_,Success("regression")) =>
        h20Table.add("cat_output",h20Table.vec(defaultColumnLabelIndex))
        h20Table.remove(defaultColumnLabelIndex)
      case _ =>
        h20Table.add("cat_output",h20Table.vec(defaultColumnLabelIndex).toCategoricalVec)
        h20Table.remove(defaultColumnLabelIndex)
    }

    columnCategorical match {
      case Success(value) if ! value.isEmpty =>
        // columns are comma separated
        value.split(",").foreach{
          case columnLabel=>
            Try {
              h20Table.add(s"cat_${columnLabel}", h20Table.vec(columnLabel).toCategoricalVec)
              h20Table.remove(columnLabel)
            }.getOrElse {
              throw new RuntimeException(s"categorical ${columnLabel} column not found.")
            }
        }
      case _ => // nothing to exclude
    }
    h20Table
  }
}
