#!/usr/bin/env bash

# setup directories
mkdir log
mkdir data
mkdir model

# start streaming topology

# start batch layer scripts
# python pull_and_persist_training_data.py
# python train_and_persist_model.py

# start serving layer scripts
# python predict.py

# start data simulation scripts
# python generate_and_push_training_data.py 10 10

# start webserver
