BATCH_DURATION = 1 # seconds per batch
KAFKA_TOPIC = 'afsis-train'
KAFKA_URL = 'localhost:9092'
KAFKA_GROUP = 'afsis'
LOG_FILE = 'conf/logging.conf'
AUTOCOMMIT_INTERVAL_MS = 1000
DATA_DIR = 'data/'
MODEL_DIR=  'model/'
