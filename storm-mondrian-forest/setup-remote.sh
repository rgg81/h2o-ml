#!/usr/bin/env bash

#installing redis
wget http://download.redis.io/redis-stable.tar.gz
tar xvzf redis-stable.tar.gz
cd redis-stable
sudo apt-get install -y make
sudo apt-get install -y gcc
sudo apt-get install -y build-essential
sudo apt-get update
make distclean
make
sudo cp src/redis-server /usr/local/bin/
sudo cp sudo cp src/redis-cli /usr/local/bin/

# installing java 7
sudo add-apt-repository ppa:webupd8team/java -y
sudo apt-get update
echo debconf shared/accepted-oracle-license-v1-1 select true | sudo debconf-set-selections
echo debconf shared/accepted-oracle-license-v1-1 seen true | sudo debconf-set-selections
sudo apt-get install -y oracle-java7-installer

# installing maven
sudo apt-get install -y maven

# installing git
sudo apt-get install -y git



