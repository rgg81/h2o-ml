'use strict';

// Declare app level module which depends on views, and components
angular.module('forest', [
    'forest.controllers',
    'forest.directives',
    'forest.services',
    'ngMaterial'
]).config(['$routeProvider', function($routeProvider) {
    $routeProvider.otherwise({redirectTo: '/home'});
}]);