'use strict';

angular.module('forest.directives', [])

    .directive('drawTree', function() {
        //explicitly creating a directive definition variable
        //this may look verbose but is good for clarification purposes
        //in real life you'd want to simply return the object {...}
        var directiveDefinitionObject = {
            restrict: "E",
            scope: {
                data: "="
            },
            link: function (scope, element, attrs) {
                // ************** Generate the tree diagram	 *****************
                var margin = {top: 40, right: 50, bottom: 20, left: 50},
                    width = 960 - margin.right - margin.left,
                    height = 400 - margin.top - margin.bottom;

                var tree = d3.layout.tree()
                    .size([height, width]);

                // specify the diagonal path link draw function
                var diagonal = d3.svg.diagonal().projection(function (d) {
                    return [d.x, d.y];
                });
                //in D3, any selection[0] contains the group
                //selection[0][0] is the DOM node
                //but we won't need that this time
                var svg = d3.select(element[0]).append("svg:svg").attr("width", width + margin.right + margin.left)
                            .append("svg:g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                // Compute the new tree layout.
                var nodes = tree.nodes(scope.data).reverse(),
                    links = tree.links(nodes);

                // Normalize for fixed-depth.
                nodes.forEach(function(d) { d.y = d.depth * 100; });

                // Declare the nodes…
                var node = svg.selectAll("g.node")
                    .data(nodes, function(d) { return d.id || (d.id = ++id); });

                // Enter the nodes.
                var nodeEnter = node.enter().append("svg:g")
                    .attr("class", "node")
                    .attr("transform", function(d) {
                        return "translate(" + d.x + "," + d.y + ")"; });

                nodeEnter.append("svg:circle")
                    .attr("r", 10)
                    .style("fill", "#fff");

                nodeEnter.append("svg:text")
                    .attr("y", function(d) {
                        return d.children || d._children ? -18 : 18; })
                    .attr("dy", ".35em")
                    .attr("text-anchor", "middle")
                    .text(function(d) { return d.name; })
                    .style("fill-opacity", 1);

                // Declare the links…
                var link = svg.selectAll("path.link")
                    .data(links, function(d) { return d.target.id; });

                // Enter the links.
                link.enter().insert("path", "g")
                    .attr("class", "link")
                    .attr("d", diagonal);

                // adjust dynamic height and width
                var heightSvg = svg[0][0].getBBox().height;
                var widthSvg = svg[0][0].getBBox().width;
                d3.select(svg[0][0].parentNode).attr("height", heightSvg + 20);
                d3.select(svg[0][0].parentNode).attr("width", widthSvg + 200);

            }
        };
        return directiveDefinitionObject;
    });