
'use strict';

angular.module('forest.controllers', ['ngRoute'])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/home', {
            templateUrl: 'forest/templates/home.html',
            controller: 'ViewCtrl'
        }).when('/detail/:treeId', {
            templateUrl: 'forest/templates/detail.html',
            controller: 'TreeDetailCtrl'
        });
    }])
    .controller('ViewCtrl', ['$scope','treeService',function($scope,treeService) {

        treeService.all().then(function(data) {
            //this will execute when the
            //AJAX call completes.
            $scope.treeData = data;
        });

}]).controller('TreeDetailCtrl', ['$scope','$routeParams','treeService','$mdDialog',function($scope,$routeParams,treeService,$mdDialog) {

    treeService.detailTree($routeParams.treeId).then(function(data) {
        //this will execute when the
        //AJAX call completes.
        $scope.treeData = data;
    });

    $scope.showAlert = function(ev,dataTree) {
        // Appending dialog to document.body to cover sidenav in docs app
        // Modal dialogs should fully cover application
        // to prevent interaction outside of dialog
        $mdDialog.show(
            $mdDialog.alert()
                .clickOutsideToClose(true)
                .title('Tree Content Data')
                .textContent(dataTree)
                .ariaLabel('Tree Content Data')
                .ok('Close!')
                .targetEvent(ev)
        );
    };

}]);