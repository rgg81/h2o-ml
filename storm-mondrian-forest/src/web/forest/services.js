'use strict';

// Service definition
angular.module('forest.services', []).service('treeService',function($http){
    this.all = function(){
        return $http({
            method: 'GET',
            url: '/all'
        }).then(function successCallback(response) {
            return response.data;
        }, function errorCallback(response) {
            console.error(response);
            return [];
        });
    };
    this.detailTree = function(treeId){
        return $http({
            method: 'GET',
            url: '/all/' + treeId
        }).then(function successCallback(response) {
            return response.data;
        }, function errorCallback(response) {
            console.error(response);
            return [];
        });
    };
});