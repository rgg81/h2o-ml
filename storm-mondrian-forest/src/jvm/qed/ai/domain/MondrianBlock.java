package qed.ai.domain;

import com.google.common.base.Optional;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import org.apache.log4j.Logger;
import qed.ai.util.DistributedRandomNumberGenerator;
import qed.ai.util.RandomFactory;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A single partition of the input space corresponding to a node in the Mondrian tree.
 */
public class MondrianBlock {

    private static final Logger LOG = Logger.getLogger(MondrianBlock.class);
    private static final double EPSILON = Math.pow(10,-10);
    //     instance variables (data-based)
    private XData _lowerBounds;      // bounds depending on this node's data
    private XData _upperBounds;      // bounds depending on this node's data
    private Double _linearDimension = 0.0; // sum of bound ranges
    private Double _tau = 0.0;                // creation "time" (in the Mondrian context)
    private List<XData> _data;              // a slice of the original data, for this node
    private Split _split = new Split(0,0);          // (split dimension, split location)
    private Double _lifetime;    // lifetime of the tree
    private List<Integer> _labels = new ArrayList<>(); // labels for the data in this node
    private int _nClass = -1; // number of classes (the same for all nodes)
    private List<Integer> _classFreq; // frequency of each class at this node
    private List<Integer> _tab; // number of "tables" for each class (IKN)
    private List<Double> _classProb; // predictive posterior distribution

    //     instance variables (tree-based)
    private MondrianBlock _parent = null;
    private MondrianBlock _left = null;
    private List<XData> _leftData = new ArrayList<>();
    private List<Integer> _leftLabels = new ArrayList<>();
    private MondrianBlock _right = null;
    private List<XData> _rightData = new ArrayList<>();
    private List<Integer> _rightLabels = new ArrayList<>();


    /**
     * Only exists because this object needs to be serialized.
     */
    public MondrianBlock() {

    }

    public MondrianBlock(List<XData> data, List<Integer> labels, int nClass, Optional<MondrianBlock> parent, Double lifetime, Optional<Double> tau, Optional<Split> split, Boolean recurse) throws Exception {
        // initialise prediction variables
        _labels = labels;
        _nClass = nClass;
        _classProb = zeros(nClass);

        // (NIPS14, Algorithm 5), if j' is a leaf
        _classFreq = binCount(labels,nClass);
        _tab = minimum(_classFreq, 1);

        _data=data;
        _parent=parent.orNull();
        _lifetime=lifetime;
        computeDataBounds();
        if(tau.isPresent() && split.isPresent()) {
            _tau=tau.get();
            _split=split.get();
        }
        else {
            Double e;
            if (_linearDimension < EPSILON) { // no data in this block
                e = Double.POSITIVE_INFINITY;
            }
            else {
                e = Math.log(1-RandomFactory.getRandom().generate().nextDouble())/(-_linearDimension); // uses beta = 1/lambda
            }
            Double parentTau = getParentTau();
            if (e + parentTau >= _lifetime){ // this is a leaf
                _tau = _lifetime;
                return;
            }

            _tau = e + parentTau;
            _split = sampleSplit();
        }
        if (!recurse) {
            return;
        }
        splitData();

        _left = new MondrianBlock(_leftData,_leftLabels,_nClass,Optional.of(this), _lifetime, Optional.<Double>absent(), Optional.<Split>absent(),true);
        _right = new MondrianBlock(_rightData,_rightLabels,_nClass,Optional.of(this), _lifetime, Optional.<Double>absent(), Optional.<Split>absent(),true);

//         (NIPS14, Algorithm 5), if j' is not a leaf
//         (in the paper, c_j' = tab_left + tab_right, does not include self._tab)
//         (we deviate because otherwise, higher class freqs may end up having lower
//          class probs at internal nodes)
        List<Integer> sumLeftRightTab = add(_left._tab,_right._tab);
        for (int i = 0; i < sumLeftRightTab.size(); i++) {
            _classFreq.set(i, sumLeftRightTab.get(i));
        }
        _tab = minimum(_classFreq, 1);

    }

    List<Integer> add(List<Integer> list1, List<Integer> list2) {
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < list1.size(); i++) {
            result.add(list1.get(i) + list2.get(i));
        }
        return result;
    }

    private void splitData() {
        int index = 0;
        for (XData xData : _data) {
            if(xData.getDimensionByIndex(_split._splitDimension) <= _split._splitLocation) {
                _leftData.add(xData);
                _leftLabels.add(_labels.get(index));
            } else {
                _rightData.add(xData);
                _rightLabels.add(_labels.get(index));
            }
            index++;
        }
    }

    private Double getParentTau() {
        if (_parent == null)
            return 0.0; // root
        return _parent._tau;
    }



    /**
     *  Trying to reproduce the result of python numpy API np.zeros(nclass)
     * @param nClass class of label data
     * @return an array of zero values elements
     */
    public static <U extends Number> List<U> zeros(int nClass) {
        List<U> result = new ArrayList<>();
        for (int i = 0; i < nClass; i++) {
            result.add((U)new Double(0));
        }
        return result;
    }

    /**
     * Port the python numpy function np.bincount.
     * It counts the frequency based in class.
     * @param labels the labels to count the frequency
     * @param nClass range of numbers in label
     * @return the frequency of each index
     */
    private List<Integer> binCount(List<Integer> labels, int nClass) {
        Multiset<Integer> freq = HashMultiset.create(labels);
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < nClass; i++) {
            result.add(freq.count(i));
        }
        return  result;
    }

    /**
     * Compare each element in the array and return the least
     * @param classFreq array to compare
     * @param compare value to compare with each element
     * @return array containing the minimum elements
     */
    private List<Integer> minimum(List<Integer> classFreq, int compare) {
        List<Integer> result = new ArrayList<>();
        for (Integer item : classFreq) {
            if(item < compare) result.add(item);
            else result.add(compare);
        }
        return result;
    }

    private Split sampleSplit() {
        // sample a dimension w/ prob. proportional to the range of each dimension
        double[] dimensionProbabilities = _upperBounds.subtract(_lowerBounds).divide(_linearDimension).getXValues();
        Integer dimension = getDimension(dimensionProbabilities);
        //sample locations uniformly from lower_bounds - upper_bounds
        Double location = RandomFactory.getRandom().nextDouble(_lowerBounds.getDimensionByIndex(dimension), _upperBounds.getDimensionByIndex(dimension));

        return new Split(dimension, location);
    }

    private Integer getDimension(double[] dimensionProbabilities) {

        DistributedRandomNumberGenerator distributedRandomNumberGenerator = new DistributedRandomNumberGenerator();
        for (int i = 0; i < dimensionProbabilities.length; i++) {
            distributedRandomNumberGenerator.addNumber(i,dimensionProbabilities[i]);
        }

        return distributedRandomNumberGenerator.getDistributedRandomNumber();
    }

    private void computeDataBounds() throws NoSuchMethodException {
        _lowerBounds = XData.lowerBounds(_data);
        _upperBounds = XData.upperBounds(_data);
        _linearDimension = _upperBounds.subtract(_lowerBounds).sumDims();
    }

    public static class Split {
        Integer _splitDimension;
        double _splitLocation;
        Split(Integer splitDimension, double splitLocation) {
            _splitDimension = splitDimension;
            _splitLocation = splitLocation;
        }

        /**
         * Just exists to serialize/deserialize data
         */
        public Split() {}

        @Override
        public String toString() {
            return "(d:" + _splitDimension + " l:" + String.format("%.2f", _splitLocation) + ")";
        }

        public Integer getSplitDimension() {
            return _splitDimension;
        }

        public double getSplitLocation() {
            return _splitLocation;
        }
    }

    public MondrianBlock extend(XData row, int label) throws Exception {
        //(NIPS14 Algorithm 4)
        Double e;
        XData zero = XData.buildWithZeros(_lowerBounds.getXValues().length);
        XData extentLower = XData.maximum(zero, _lowerBounds.subtract(row));
        XData extentUpper = XData.maximum(zero, row.subtract(_upperBounds));
        XData sumLowerUpper = extentLower.add(extentUpper);
        double extentLinearDimension = sumLowerUpper.sumDims();

        if (extentLinearDimension < EPSILON) // new row is within this node
            e = Double.POSITIVE_INFINITY;
        else e = Math.log(1-RandomFactory.getRandom().generate().nextDouble())/(- extentLinearDimension);

        double parentTau = getParentTau();
        if (e + parentTau < _tau) {
            //create a new parent for the current node, containing the
            //data of the current node + the new row.
            double[] dimensionProbabilities = extentUpper.add(extentLower).divide(extentLinearDimension).getXValues();

            Integer dimension = getDimension(dimensionProbabilities);

            double locationRangeMin,locationRangeMax;
            if (row.getDimensionByIndex(dimension) > _upperBounds.getDimensionByIndex(dimension)) {
                locationRangeMin = _upperBounds.getDimensionByIndex(dimension);
                locationRangeMax = row.getDimensionByIndex(dimension);
            }
            else {
                locationRangeMin = row.getDimensionByIndex(dimension);
                locationRangeMax = _lowerBounds.getDimensionByIndex(dimension);
            }

            Double location = RandomFactory.getRandom().nextDouble(locationRangeMin, locationRangeMax);
            List<XData> newDataList = new ArrayList<>();
            List<Integer> newLabelList = new ArrayList<>();
            newDataList.addAll(_data);
            newDataList.add(row);
            newLabelList.addAll(_labels);
            newLabelList.add(label);
            MondrianBlock newParentNode = new MondrianBlock(newDataList,newLabelList,_nClass, Optional.fromNullable(_parent), _lifetime, Optional.of(e + getParentTau()), Optional.of(new Split(dimension,location)), false);

            if (_parent != null) {
                if (_parent._left != null && _parent._left.equals(this)) _parent._left = newParentNode;
                else  _parent._right = newParentNode;
            }
            _parent = newParentNode;

            // create a new sibling for the current node
            MondrianBlock newSibling = new MondrianBlock(new ArrayList<>(Arrays.asList(row)),new ArrayList<>(Arrays.asList(label)),_nClass,Optional.of(newParentNode),_lifetime, Optional.<Double>absent(),Optional.<Split>absent(), true);

            // set the left and right children of the new parent
            if (row.getDimensionByIndex(dimension) <= location) {
                newParentNode._left =newSibling;
                newParentNode._right = this;
            } else {
                newParentNode._left = this;
                newParentNode._right = newSibling;
            }

            // (NIPS14, Algorithm 6)
            List<Integer> sumLeftRightTab = add(newParentNode._left._tab,newParentNode._right._tab);
            for (int i = 0; i < sumLeftRightTab.size(); i++) {
                newParentNode._classFreq.set(i, sumLeftRightTab.get(i));
            }
            newParentNode._tab = minimum(newParentNode._classFreq, 1);
            newParentNode.updatePosteriorCounts(label);

            return newParentNode;

        } else {
            _data = new ArrayList<>(_data);
            _data.add(row);
            _labels.add(label);
            computeDataBounds();
            if(_left != null && _right != null) {
                Integer dimension = _split._splitDimension;
                double location = _split._splitLocation;
                MondrianBlock child;
                if (row.getDimensionByIndex(dimension) <= location) child = _left;
                else child = _right;
                child.extend(row,label);
            } else {
                _classFreq.set(label, _classFreq.get(label) + 1);
                this.updatePosteriorCounts(label);
            }
            return this;
        }

    }

    public List<Double> predict(XData row, double discountParam, List<Double> classProb, double pNotSeparatedYet) {
    // (NIPS14 Algorithm 8)
        double delta = _tau - getParentTau();
        XData zero = XData.buildWithZeros(_lowerBounds.getXValues().length);
        XData extentLower = XData.maximum(zero, _lowerBounds.subtract(row));
        XData extentUpper = XData.maximum(zero, row.subtract(_upperBounds));
        XData sumExtentLowerUpper = extentUpper.add(extentLower);
        double eta = sumExtentLowerUpper.sumDims();
        double pSplit = 1 - Math.exp(-delta * eta);
        if(pSplit > 0) { //x lies outside the bounding box, create a new parent
            //         Derivation of d (not in the paper):
            //
            //          d is the expected value of exp(-discount_param * x), where x is
            //          drawn from exponential(eta) truncated to (0, delta]
            //
            //          The exponential(a) truncated to (0,b] has the PDF:
            //
            //            f(x) = a * exp(-ax) / (1 - exp(-ab))
            //
            //          d = integral of exp(-discount_param * x) from 0 to delta has been
            //          hand-verified to work out to the the value below:
            //
            //            d =         eta             1 - exp(-delta(eta + discount_param))
            //                -------------------- X --------------------------------------
            //                eta + discount_param          1 - exp(-delta * eta)
            double d = (eta / (eta + discountParam)) * (-Math.expm1(-delta * (eta + discountParam))) / (-Math.expm1(-delta * eta));
            List<Integer> nuparentClassFreq = minimum(_classFreq, 1);
            //TODO is this right? should'nt be _tab? Going with _tab
            // from python nuparent_tab = np.minimum(self._class_freq, 1)
            List<Integer> nuparentTab = minimum(_tab, 1);
//            List<Integer> nuparentTab = minimum(_classFreq, 1);
            List<Double> parentClassProb = getParentClassProb();
            // from python nuparent_class_prob = (nuparent_class_freq - d * nuparent_tab + d * np.sum(nuparent_tab) * parent_class_prob) / np.sum(nuparent_class_freq);
            List<Double> nuparentClassProb = new ArrayList<>();
            double sumClassFreq = sumElementsList(nuparentClassFreq);
            double sumTab = sumElementsList(nuparentTab);
            for (int i = 0; i < _classProb.size() ; i++) {
                double prob = (nuparentClassFreq.get(i) - d * nuparentTab.get(i) + d * sumTab * parentClassProb.get(i)) / sumClassFreq;
                nuparentClassProb.add(prob);
            }
            // from python class_prob = class_prob + p_notseparatedyet * p_split * nuparent_class_prob;
            for (int i = 0; i < classProb.size(); i++) {
                classProb.set(i, classProb.get(i) + pNotSeparatedYet * pSplit * nuparentClassProb.get(i));
            }
        }
        if (_left == null && _right == null) {// leaf
            //class_prob = class_prob + p_notseparatedyet * (1 - p_split) * self._class_prob
            for (int i = 0; i < classProb.size(); i++) {
                Double prob = classProb.get(i) + pNotSeparatedYet * (1-pSplit) * _classProb.get(i);
                classProb.set(i, prob);
            }
            return classProb;

        } else {
            pNotSeparatedYet = pNotSeparatedYet * (1 - pSplit);
            Split split = getSplit();
            if(row.getDimensionByIndex(split.getSplitDimension()) <= split.getSplitLocation())
                return _left.predict(row, discountParam, classProb, pNotSeparatedYet);
            else return _right.predict(row, discountParam, classProb, pNotSeparatedYet);
        }
    }

    private void updatePosteriorCounts(int label) {
        if (_tab.get(label) == 1) {
            return;
        }
        if (_left != null && _right != null) { //not a leaf
            List<Integer> sumLeftRightTab = add(_left._tab,_right._tab);
            for (int i = 0; i < sumLeftRightTab.size(); i++) {
                _classFreq.set(i, sumLeftRightTab.get(i));
            }
        }
        _tab = minimum(_classFreq, 1);

        if (_parent == null)
            return;

        _parent.updatePosteriorCounts(label);
    }

    public void computePredictivePosteriorDistribution(double discountParam) {
        //(NIPS14, Algorithm 7)
        List<Double> parentClassProb = getParentClassProb();

        double d = Math.exp(-discountParam * (_tau - getParentTau()));
        double sumClassFreq = sumElementsList(_classFreq);
        double sumTab = sumElementsList(_tab);

        for (int i = 0; i < _classProb.size() ; i++) {
            double prob = (_classFreq.get(i) - d * _tab.get(i) + d * sumTab * parentClassProb.get(i)) / sumClassFreq;
            _classProb.set(i, prob);
        }

        if (_left != null)
            _left.computePredictivePosteriorDistribution(discountParam);

        if (_right != null)
            _right.computePredictivePosteriorDistribution(discountParam);
    }

    private List<Double> getParentClassProb() {
        List<Double> parentClassProb;
        if (_parent == null) { //root
            //uniform prior at the root
            parentClassProb = new ArrayList<>();
            for (int i = 0; i < _nClass ; i++) {
                parentClassProb.add(1 / new Double(_nClass));
            }
        }
        else
            parentClassProb = _parent._classProb;
        return parentClassProb;
    }

    private <U extends Number> double sumElementsList(List<U> list) {
        double sum = 0;
        for (U element : list) {
            sum += element.doubleValue();
        }
        return sum;
    }



    public List<XData> getData() {
        return _data;
    }

    @Override
    public String toString() {
        String value = "[" + _split + "]";
        return value;
    }

    public String longName() {
        String leftData="[]",rightData = "[]";
        if(_left  != null) leftData = _left._data.toString();
        if(_right != null) rightData = _right._data.toString();

        return "Data:" + _data + " label:" + _labels + " tau:" + _tau + " lifetime:" + _lifetime + " lowerbounds:" + _lowerBounds
                + " upperBounds:" + _upperBounds + " linear dimension:" + _linearDimension + " split: " + _split
                + " left:" + leftData + " right:" + rightData + " classFreq:" + _classFreq + " classProb:" + _classProb
                + " tab:" + _tab;
    }

    public synchronized static void appendTreeToFile(MondrianTree tree) {
        try {
            String contents = printAll(tree.getRoot(),"");
            FileWriter fileStream = new FileWriter("trees.txt", true);
            BufferedWriter out = new BufferedWriter(fileStream);
            out.newLine();
            out.write("--------------------------------------------------------------");
            out.newLine();
            out.write("tree_id:" + tree.getId());
            out.newLine();
            out.write(contents);
            out.close();
        } catch (Exception e) {
            LOG.error("error in saving trees to file", e);
        }
    }
    public synchronized static String printAll(MondrianBlock block, String tabs) {

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(tabs + "DATA: " + block.getData() + "\n");
        stringBuilder.append(tabs + "LABELS: " + block.getLabels() + "\n");
        stringBuilder.append(tabs + "CLASSFREQ/TAB" + "\n");
        stringBuilder.append(tabs + block.getClassFreq() + "/" + block.getTab()+ "\n");
        stringBuilder.append(tabs + "CLASSPROB: " + block.getClassProb()+ "\n");
        MondrianBlock left = block.getLeft();
        MondrianBlock right = block.getRight();
        if(left != null && right != null) {
            stringBuilder.append(tabs + "SPLIT (dim, loc, tau): " + block.getSplit().getSplitDimension() + "," + block.getSplit().getSplitLocation() + "," + block.getTau()+ "\n");
            stringBuilder.append(tabs + "LEFT"+ "\n");
            stringBuilder.append(printAll(block.getLeft(),tabs + "\t"));
            stringBuilder.append(tabs + "RIGHT"+ "\n");
            stringBuilder.append(printAll(block.getRight(),tabs + "\t"));
        } else {
            stringBuilder.append(tabs + "LEAF"+ "\n");
        }
        return stringBuilder.toString();

    }

    public MondrianBlock getLeft() {
        return _left;
    }
    public MondrianBlock getRight() {
        return _right;
    }
    public MondrianBlock getParent() {
        return _parent;
    }
    public XData getUpperBounds() {
        return _upperBounds;
    }
    public XData getLowerBounds() {
        return _lowerBounds;
    }
    public double getLinearDimension() {
        return _linearDimension;
    }
    public Split getSplit(){ return _split;}
    public List<Integer> getClassFreq() {
        return _classFreq;
    }
    public List<Integer> getTab() {return _tab;}
    public List<Double> getClassProb(){return _classProb;}
    public List<Integer> getLabels() { return _labels;}
    public double getTau() {return _tau;}

}
