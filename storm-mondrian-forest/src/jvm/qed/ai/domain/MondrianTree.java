package qed.ai.domain;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.google.common.base.Optional;
import org.apache.storm.shade.org.apache.commons.codec.binary.Base64;
import org.apache.storm.shade.org.apache.http.impl.client.SystemDefaultCredentialsProvider;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.UUID;

/**
 * Created by roberto on 10/12/15.
 */
public class MondrianTree {

    public static final double DEFAULT_DISCOUNT_PARAM = 10d;

    private MondrianBlock _root;
    private Double _lifeTime;
    private String _id;
    private Double _discountParam;
    private Integer _nClass;

    /**
     * Only exists because of deserialization/serialization
     */
    public MondrianTree() {

    }

    public String getId() {
        return _id;
    }

    public MondrianTree(Double lifeTime, List<XData> data, List<Integer> labels, double discountParam, int nClass) throws Exception {
        _lifeTime = lifeTime;
        _discountParam = discountParam;
        _nClass = nClass;
        _root = new MondrianBlock(data,labels,_nClass,Optional.<MondrianBlock>absent(),_lifeTime,Optional.<Double>absent(),Optional.<MondrianBlock.Split>absent(),true);

        _id = UUID.randomUUID().toString();
        _root.computePredictivePosteriorDistribution(_discountParam);
    }

    public void extend(XData data, int label) throws Exception {
        _root = _root.extend(data,label);
        _root.computePredictivePosteriorDistribution(_discountParam);
    }

    public List<Double> predict(XData x) {
        assert _root != null;
        double pNotSeparatedYet = 1.0;
        List<Double> probs = MondrianBlock.zeros(_nClass);
        return _root.predict(x,_discountParam,probs,pNotSeparatedYet);
    }

    @Override
    public String toString() {
        return "rootNode: " + _root;
    }

    public MondrianBlock getRoot() {
        return _root;
    }

    /**
     * Serialize the MondrianTree object.
     * @param kryo serialize and deserialize
     * @return object serialized
     */
    public String serialize(Kryo kryo) {
        //Serialize object using kryo
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        Output output = new Output(outputStream);
        kryo.writeObject(output, this);
        output.flush();
        output.close();
        return Base64.encodeBase64String(outputStream.toByteArray());
    }

    /**
     * Deserialize MondrianTree objects.
     * @param binaryData binary object data
     * @param kryo serialize and deserialize
     * @return Mondrian object
     * @throws IOException
     */
    static public MondrianTree deserialize(String binaryData, Kryo kryo) throws IOException {
        InputStream stream = new ByteArrayInputStream(Base64.decodeBase64(binaryData));
        Input input = new Input(stream);
        MondrianTree tree = kryo.readObject(input, MondrianTree.class);
        stream.close();
        return tree;
    }


}
