package qed.ai.domain;

import java.io.Serializable;
import java.util.List;

/**
 * Created by roberto on 08/12/15.
 */
public class XData implements Serializable {

    private double[] xValues;


    public XData() {

    }

    public XData(double... xValues) {
        this.xValues = xValues;
    }

    @Override
    public String toString() {
        StringBuilder appender = new StringBuilder("(" + xValues[0]);
        for (int i = 1; i < xValues.length; i++) {
            appender.append("," + xValues[i]);
        }
        appender.append(")");
        return appender.toString();
    }

    public Double getDimensionByIndex(int index) {
        return xValues[index];
    }

    public XData add(XData other) {
        if (this.xValues.length != other.xValues.length)
            throw new IllegalArgumentException("both must have the size");

        double[] result = new double[xValues.length];
        for (int i = 0; i < xValues.length; i++) {
            result[i] = xValues[i] + other.xValues[i];
        }
        return new XData(result);
    }
    public XData subtract(XData other) {
        if (this.xValues.length != other.xValues.length)
            throw new IllegalArgumentException("both must have the size");

        double[] result = new double[xValues.length];
        for (int i = 0; i < xValues.length; i++) {
            result[i] = xValues[i] - other.xValues[i];
        }
        return new XData(result);
    }

    public double sumDims() {
        double sum = 0.0;
        for (double i : xValues)
            sum += i;
        return sum;
    }

    public XData divide(double toDivide) {
        double[] result = new double[xValues.length];
        for (int i = 0; i < xValues.length; i++) {
            result[i] = xValues[i]/toDivide;
        }
        return new XData(result);
    }

    public double[] getXValues() {
        return xValues;
    }

    public static XData maximum(XData data1, XData data2) {

        if (data1.xValues.length != data2.xValues.length)
            throw new IllegalArgumentException("both params must have the size " + data1.xValues.length + " != " + data2.xValues.length);

        double[] result = new double[data1.xValues.length];
        for (int i = 0; i < data1.xValues.length; i++) {
            double maxDim;
            if(data1.xValues[i] >= data2.xValues[i]) maxDim = data1.xValues[i];
            else maxDim = data2.xValues[i];
            result[i] = maxDim;
        }

        return new XData(result);

    }

    public static XData lowerBounds(List<XData> list) {
        // every element in list has the same size, picking the first
        int length = list.get(0).xValues.length;
        double[] result = new double[length];

        for (int i = 0; i < length ; i++) {
            Double min = null;
            for (XData element : list) {
                if (min == null)
                    min = element.xValues[i];
                if(element.xValues[i] < min)
                    min = element.xValues[i];
            }
            result[i] = min;
        }
        return new XData(result);
    }

    public static XData upperBounds(List<XData> list) {
        // every element in list has the same size, picking the first
        int length = list.get(0).xValues.length;
        double[] result = new double[length];

        for (int i = 0; i < length ; i++) {
            Double max = null;
            for (XData element : list) {
                if (max == null)
                    max = element.xValues[i];
                if(element.xValues[i] > max)
                    max = element.xValues[i];
            }
            result[i] = max;
        }
        return new XData(result);
    }

    public static XData buildWithZeros(int size) {
        double[] zeros = new double[size];
        for (int i = 0; i < zeros.length; i++) {
            zeros[i] = 0.0;
        }
        return new XData(zeros);
    }

}
