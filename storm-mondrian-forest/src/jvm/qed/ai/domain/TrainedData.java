package qed.ai.domain;

import qed.ai.spout.DataProducer;

/**
 * Created by roberto on 18/01/16.
 */
public class TrainedData {

    public static final int N_CLASS = DataProducer.getTypeProducer(System.getProperty("type.data")).classY();

    private XData x;
    private int label;

    public TrainedData(XData x, int label) {
        this.x = x;
        this.label = label;
    }

    public void setX(XData x) {
        this.x = x;
    }

    public void setLabel(int label) {
        this.label = label;
    }

    public XData getX() {
        return x;
    }

    public int getLabel() {
        return label;
    }
}
