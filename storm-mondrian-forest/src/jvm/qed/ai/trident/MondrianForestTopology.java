package qed.ai.trident;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.tuple.Fields;
import org.apache.log4j.Logger;
import org.apache.storm.redis.common.config.JedisPoolConfig;
import qed.ai.functions.*;
import qed.ai.spout.*;
import qed.ai.util.Flush;
import storm.trident.Stream;
import storm.trident.TridentState;
import storm.trident.TridentTopology;
import storm.trident.operation.impl.FilterExecutor;
import storm.trident.state.StateSpec;

/**
 * Generates Mondrian trees and persist the state in memory.
 * It has two streams. One stream has a spout that generates
 * a list of data with size 5. These data are random selected in
 * a Array defined in Spout. These data are aggregated to build the tree
 * and then persisted in Redis.
 *
 * The second stream has a spout that generates one data at a time. This data is random
 * selected in a list in the spout. This data is broadcast to all partitions. In each partition,
 * for each data coming, all trees in this partition is selected and extended with this data.
 * Finally, the extended trees are persisted in Redis again.
 *
 * The trees persisted in Redis have a TTL of 30 minutes.
 */
public class MondrianForestTopology {

    private static final int PARALLELISM_UPDATE_STREAM = 1;
    private static final int TOTAL_TREE_PARALLEL_PRODUCER = 10;
    public static final int TOTAL_KEY_DISTRIBUTION = 16;
    private static final long TTL_TREE = 1000 * 60 * 30 * 10 * 10; // 6 hours
    private static final Logger LOG = Logger.getLogger(MondrianForestTopology.class);


    public static void main(String args[]) throws Exception {
        int totalTrees,totalInitialPointsPerTree;
        if(args[0] == null) totalTrees = 4000;
        else totalTrees = Integer.parseInt(args[0]);

        if(args[1] == null) totalInitialPointsPerTree = 5;
        else totalInitialPointsPerTree = Integer.parseInt(args[1]);

        LOG.debug("Total trees to produce:" + totalTrees + " Initial points per tree:" + totalInitialPointsPerTree + " Reload CSV:" + System.getProperty("load.csv"));

        TridentTopology topology = new TridentTopology();
        JedisPoolConfig poolConfig = new JedisPoolConfig.Builder()
                .setHost("localhost").setPort(6379)
                .build();

        TreesDB.Factory factory = new TreesDB.Factory(poolConfig,TTL_TREE);

        // flushRedis/clear redis database
        Flush.flushRedis();
        Flush.flushResultFiles();

        topology.newStream("spout1", new BatchDataSpout(totalInitialPointsPerTree, totalTrees, TOTAL_TREE_PARALLEL_PRODUCER)).parallelismHint(TOTAL_TREE_PARALLEL_PRODUCER).
                partitionAggregate(new Fields("number","label"), new DataAggregator(), new Fields("tree"))
                .each(new Fields("tree"), new TreePrinter(), new Fields("treePrinted"))
                .partitionPersist(factory, new Fields("tree"), new PersistForest());

        StateSpec spec = new StateSpec(factory);
        spec.requiredNumPartitions = TOTAL_KEY_DISTRIBUTION;
        TridentState staticState = topology.newStaticState(spec);

        topology.newStream("spout2", new ExtendDataSpout()).parallelismHint(PARALLELISM_UPDATE_STREAM)
                .broadcast()
                .stateQuery(staticState, new Fields("number","label"), new ExtendAllTrees(), new Fields("extendedTrees", "originalTrees")).parallelismHint(TOTAL_KEY_DISTRIBUTION).
                partitionPersist(factory, new Fields("extendedTrees", "originalTrees"), new UpdateForest());


        boolean debugPredictions = Boolean.valueOf(System.getProperty("debug.predictions"));
        Stream baseStream = topology.newStream("predict", new PredictDataSpout()).broadcast()
                .stateQuery(staticState, new Fields("unLabel", "expected"), new PredictAllTrees(), new Fields("predictions","trees"));
        if(debugPredictions) baseStream.each(new Fields("predictions", "trees", "unLabel", "expected"), new PredictStats(), new Fields("stats"));
        baseStream.aggregate(new Fields("predictions","expected", "unLabel"), new ConsolidatePredictions(), new Fields("majorityPrediction"));


        Config conf = new Config();
        conf.setDebug(true);

        LocalCluster cluster = new LocalCluster();
        cluster.submitTopology("mondrian-trees", conf, topology.build());

    }
}
