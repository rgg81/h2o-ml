package qed.ai.spout;

import backtype.storm.Config;
import backtype.storm.task.TopologyContext;
import backtype.storm.tuple.Fields;
import org.apache.log4j.Logger;
import qed.ai.domain.XData;
import storm.trident.operation.TridentCollector;
import storm.trident.spout.IBatchSpout;

import java.util.*;

/**
 * Created by roberto on 09/12/15.
 */
public class PredictDataSpout implements IBatchSpout {

    private static final Logger LOG = Logger.getLogger(PredictDataSpout.class);
    private int currentCycleRun = 0;
    private int currentIndex = 0;

    private static final DataProducer dataSource = DataProducer.getTypeProducer(System.getProperty("type.data"));


    @Override
    public void open(Map conf, TopologyContext context) {
        LOG.debug("opening, waiting 60 seconds to start");
        try{
            Thread.sleep(60000);
        } catch (Exception e) {
            LOG.error("error sleeping thread", e);
        }
    }

    @Override
    public void emitBatch(long batchId, TridentCollector collector) {

        LOG.debug("PredictDataSpout.emitBatch batchId: " + batchId);
        Map.Entry<XData,Integer> data = dataSource.getTreeDataByIndex(currentIndex, "predict");
        collector.emit(Arrays.asList((Object)data.getKey(),data.getValue()));
        currentIndex++;
        if(currentIndex == dataSource.getTotalItemsToPredict()) currentIndex = 0;

    }

    @Override
    public void ack(long batchId) {
        LOG.debug("PredictDataSpout.ack batchId: " + batchId);
        currentCycleRun++;
        // thread will sleep after CYCLES_RUN executions
//        if(currentCycleRun == dataSource.getTotalItemsToPredict()) {
        if(currentCycleRun == 2000) {
            try{
                LOG.debug("finishing execution PredictDataSpout");
                Thread.sleep(Long.MAX_VALUE);
            } catch (Exception e) {
                LOG.error("error sleeping thread", e);
            }
        }
    }

    @Override
    public void close() {
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        Config conf = new Config();
//        conf.setMaxTaskParallelism(32);
        return conf;
    }

    @Override
    public Fields getOutputFields() {
        return new Fields("unLabel", "expected");
    }

}
