package qed.ai.spout;

import backtype.storm.Config;
import backtype.storm.task.TopologyContext;
import backtype.storm.tuple.Fields;
import org.apache.log4j.Logger;
import qed.ai.domain.MondrianBlock;
import qed.ai.domain.TrainedData;
import qed.ai.domain.XData;
import storm.trident.operation.TridentCollector;
import storm.trident.spout.IBatchSpout;

import java.util.*;

/**
 * Created by roberto on 09/12/15.
 */
public class ExtendDataSpout implements IBatchSpout {

    private static final Logger LOG = Logger.getLogger(ExtendDataSpout.class);

    private int currentCycleRun = 0;
    private static final DataProducer dataSource = DataProducer.getTypeProducer();
    public List<Double> classCountExtendPoints = MondrianBlock.zeros(TrainedData.N_CLASS);
    private static final int CYCLES_RUN = 500;

    @Override
    public void open(Map conf, TopologyContext context) {
        LOG.debug("opening, waiting 10 seconds to start.");
        try{
            Thread.sleep(10000);
//            Thread.sleep(Long.MAX_VALUE);
        } catch (Exception e) {
            LOG.error("error sleeping thread", e);
        }
    }

    @Override
    public void emitBatch(long batchId, TridentCollector collector) {

        LOG.debug("ExtendDataSpout.emitBatch batchId: " + batchId);

        //pick a random element in array
        Random rand = new Random(System.nanoTime());
        int nj = Math.abs(rand.nextInt()) % dataSource.getTotalItemsToExtend();
        Map.Entry<XData,Integer> data = dataSource.getTreeDataByIndex(nj, "extend");

        double first = classCountExtendPoints.get(0);
        boolean checkEqual = true;
        for (Double classCountExtendPoint : classCountExtendPoints) {
            if(classCountExtendPoint == first) checkEqual = false;
        }

        // check if all elements are equals. If so, emit any point.
        if(checkEqual) {
            classCountExtendPoints.set(data.getValue(), classCountExtendPoints.get(data.getValue()) + 1);
            LOG.debug("ExtendDataSpout.emitBatch point label: " + data.getValue() + " features:" + data.getKey());
            collector.emit(Arrays.asList((Object)data.getKey(), data.getValue()));
        } else {
            // take the class that has minimum count
            int min = classCountExtendPoints.indexOf(Collections.min(classCountExtendPoints));
            while (data.getValue() != min) {
                nj = Math.abs(rand.nextInt()) % dataSource.getTotalItemsToExtend();
                data = dataSource.getTreeDataByIndex(nj, "extend");
            }
            classCountExtendPoints.set(data.getValue(), classCountExtendPoints.get(data.getValue()) + 1);
            LOG.debug("ExtendDataSpout.emitBatch point label: " + data.getValue() + " features:" + data.getKey());
            collector.emit(Arrays.asList((Object)data.getKey(), data.getValue()));
        }

    }

    @Override
    public void ack(long batchId) {
        LOG.debug("ExtendDataSpout.ack batchId: " + batchId);
        currentCycleRun++;
        // thread will sleep after CYCLES_RUN executions
        if(currentCycleRun == CYCLES_RUN) {
            try{
                LOG.debug("finishing execution ExtendDataSpout");
                Thread.sleep(Long.MAX_VALUE);
            } catch (Exception e) {
                LOG.error("error sleeping thread", e);
            }
        }
    }

    @Override
    public void close() {
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        Config conf = new Config();
//        conf.setMaxTaskParallelism(32);
        return conf;
    }

    @Override
    public Fields getOutputFields() {
        return new Fields("number","label");
    }

}
