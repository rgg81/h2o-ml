package qed.ai.spout;

import backtype.storm.Config;
import backtype.storm.task.TopologyContext;
import backtype.storm.tuple.Fields;
import org.apache.log4j.Logger;
import qed.ai.domain.XData;
import storm.trident.operation.TridentCollector;
import storm.trident.spout.IBatchSpout;

import java.util.*;

/**
 * Created by roberto on 09/12/15.
 */
public class UnLabelToyDataSpout implements IBatchSpout {

    private static final Logger LOG = Logger.getLogger(UnLabelToyDataSpout.class);
    private List<XData> _xData = new ArrayList<>(
            Arrays.asList(new XData(-0.25, -1.25), new XData(-2.5, -1.5), new XData(+1.0, +0.5),
                    new XData(-2.5, -1.0), new XData(-1.2, +1.2), new XData(-1.0, +2.0))
    );
    private static final int CYCLES_RUN = 4;
    private int currentCycleRun = 0;


    @Override
    public void open(Map conf, TopologyContext context) {
        LOG.debug("opening, waiting 15 seconds to start");
        try{
            Thread.sleep(15000);
        } catch (Exception e) {
            LOG.error("error sleeping thread", e);
        }
    }

    @Override
    public void emitBatch(long batchId, TridentCollector collector) {

        LOG.debug("UnLabelToyDataSpout.emitBatch batchId: " + batchId);
        //pick a random element in array
        Random rand = new Random(System.nanoTime());
        int nj = Math.abs(rand.nextInt())%_xData.size();
        collector.emit(Arrays.asList((Object)_xData.get(nj)));

    }

    @Override
    public void ack(long batchId) {
        LOG.debug("UnLabelToyDataSpout.ack batchId: " + batchId);
        currentCycleRun++;
        // thread will sleep after CYCLES_RUN executions
        if(currentCycleRun == CYCLES_RUN) {
            try{
                LOG.debug("finishing execution UnLabelToyDataSpout");
                Thread.sleep(Long.MAX_VALUE);
            } catch (Exception e) {
                LOG.error("error sleeping thread", e);
            }
        }
    }

    @Override
    public void close() {
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        Config conf = new Config();
//        conf.setMaxTaskParallelism(32);
        return conf;
    }

    @Override
    public Fields getOutputFields() {
        return new Fields("unLabel");
    }

}
