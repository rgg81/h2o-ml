package qed.ai.spout;

import com.google.common.collect.Maps;
import com.opencsv.CSVReader;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import qed.ai.domain.XData;
import qed.ai.spout.datasource.BreastCancerDataProducer;
import qed.ai.spout.datasource.GeoSurveyCsvDataProducer;
import qed.ai.spout.datasource.IrisCsvDataProducer;
import qed.ai.spout.datasource.TestDataProducer;
import qed.ai.util.RandomFactory;
import qed.ai.util.RedisConnector;
import redis.clients.jedis.Jedis;

import java.io.FileReader;
import java.util.Map;

/**
 * Created by roberto on 1/29/16.
 */
public abstract class DataProducer extends RedisConnector {

    private static final Logger LOG = Logger.getLogger(DataProducer.class);
    private static DataProducer ourInstance = null;


    private static final double percentPredict = 0.15;
    private static final double percentExtend = 0.40;


    private  int totalItemsToPredict;
    private  int totalItemsToExtend;
    protected  int totalItems = 0;


    public abstract int classY();
    public abstract String convertLabelToName(Integer label);
    public abstract Integer convertLineToLabel(String[] line);
    public abstract XData convertLineToXData(String[] line);

    synchronized public static DataProducer getTypeProducer() {
        return getTypeProducer(System.getProperty("type.data"));
    }

    synchronized public static DataProducer getTypeProducer(String type) {
        if(ourInstance == null) {

            switch (type) {
                case "breast-cancer":
                    ourInstance = BreastCancerDataProducer.getInstance();
                    break;
                case "iris":
                    ourInstance = IrisCsvDataProducer.getInstance();
                    break;
                case "geo-survey":
                    ourInstance = GeoSurveyCsvDataProducer.getInstance();
                    break;
                case "test":
                    ourInstance = new TestDataProducer();
                    break;
            }
        }
        return ourInstance;
    }

    protected DataProducer() {
        //only used for unit test
        super(0);
    }

    protected DataProducer(boolean excludeHeader) {
        super(6380);
        LOG.debug("creating instance");
        Jedis jedis = null;
        try {

            jedis = getJedis();

            final boolean reset = Boolean.valueOf(System.getProperty("load.csv"));
            if(! reset) {
                totalItems = jedis.dbSize().intValue();
                totalItemsToPredict = new Double(totalItems*percentPredict).intValue();
                totalItemsToExtend = new Double(totalItems*percentExtend).intValue();
            } else {
                // cleaning
                jedis.flushDB();
                CSVReader reader = new CSVReader(new FileReader(System.getProperty("csv.location")));
                String [] nextLine;
                boolean shouldInclude = true;
                // exclude header
                if(excludeHeader) {
                    reader.readNext();
                }
                while ((nextLine = reader.readNext()) != null) {
                    // nextLine[] is an array of values from the line
                    totalItems++;
                    for (String s : nextLine) {
                        if(s.equals("NA")) {
                            totalItems--;
                            shouldInclude = false;
                            break;
                        }
                    }
                    if(shouldInclude) {
                        jedis.set("index-grow-" + totalItems, StringUtils.join(nextLine, ","));
                    } else shouldInclude = true;
                }
                totalItemsToPredict = new Double(totalItems*percentPredict).intValue();
                totalItemsToExtend = new Double(totalItems*percentExtend).intValue();

                for (int i = 0; i < totalItemsToPredict; i++) {
                    int indexToRemove = RandomFactory.getRandom().generate().nextInt(totalItems);
                    boolean search = true;
                    String line = null;
                    while (search) {
                        indexToRemove = RandomFactory.getRandom().generate().nextInt(totalItems);
                        line = jedis.get("index-grow-" + indexToRemove);
//                        if((line != null && convertLineToLabel(line.split(",")) == 0)) {
//                            search = false;
//                        }
                        if(line != null) {
                            search = false;
                        }

                    }
                    jedis.set("index-predict-" + i, line);
                    jedis.del("index-grow-" + indexToRemove);
                }

                for (int i = 0; i < totalItemsToExtend; i++) {
                    int indexToRemove = RandomFactory.getRandom().generate().nextInt(totalItems);
                    boolean search = true;
                    String line = null;
                    while (search) {
                        indexToRemove = RandomFactory.getRandom().generate().nextInt(totalItems);
                        line = jedis.get("index-grow-" + indexToRemove);
                        if(line != null) {
                            search = false;
                        }
                    }

                    jedis.set("index-extend-" + i, line);
                    jedis.del("index-grow-" + indexToRemove);
                }

            }
        } catch(Exception e) {
            LOG.error("could not import the csv file", e);
        } finally {
            if (jedis != null) {
                returnJedis(jedis);
            }
        }
    }

    public int getTotalItemsToPredict() {
        return totalItemsToPredict;
    }

    public int getTotalItemsToExtend() {
        return totalItemsToExtend;
    }

    public int getTotalItems() {
        return totalItems;
    }

    public Map.Entry<XData,Integer> getTreeDataByIndex(int index, String type) {
        Jedis jedis = null;
        try {
            jedis = getJedis();
            String value = jedis.get("index-" + type + "-" + index);
            if(value == null) return null;
            String[] valuesSplit = value.split(",");
            return Maps.immutableEntry(convertLineToXData(valuesSplit), convertLineToLabel(valuesSplit));

        } finally {
            if (jedis != null) {
                returnJedis(jedis);
            }
        }
    }


}
