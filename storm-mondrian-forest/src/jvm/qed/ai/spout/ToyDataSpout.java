package qed.ai.spout;

import backtype.storm.Config;
import backtype.storm.task.TopologyContext;
import backtype.storm.tuple.Fields;
import org.apache.log4j.Logger;
import qed.ai.domain.XData;
import storm.trident.operation.TridentCollector;
import storm.trident.spout.IBatchSpout;

import java.util.*;

/**
 * Generates batches of random selected items. It generates duplicates.
 */
public class ToyDataSpout implements IBatchSpout {

    private static final Logger LOG = Logger.getLogger(ToyDataSpout.class);
    private static final int CYCLES_RUN = 20;
    private int currentCycleRun = 0;

    private List<XData> _xData = new ArrayList<>(
            Arrays.asList(new XData(-0.5, -1.0), new XData(-2.0, -2.0), new XData(+1.0, +0.5),
                    new XData(-3.0, -1.5), new XData(-1.0, +1.0), new XData(-1.5, +1.5))
    );

    private List<Integer> _label = new ArrayList<>(
            Arrays.asList(0,0,1,1,2,2)
    );

    private int _totalDataPerModel;


    public ToyDataSpout(int totalDataPerModel) {
        _totalDataPerModel = totalDataPerModel;
    }

    @Override
    public void open(Map conf, TopologyContext context) {
        LOG.debug("opening");
    }

    @Override
    public void emitBatch(long batchId, TridentCollector collector) {
        //bootstrap aggregating of training data for a random forest
        LOG.debug("ToyDataSpout.emitBatch batchId: " + batchId);
        Random rand = new Random(System.nanoTime());
        List<XData> baggedXData = new ArrayList<>();
        List<Integer> baggedYData = new ArrayList<>();

        for (int j = 0; j < _totalDataPerModel; j++){
            //add a random data point to the training data for this tree
            int nj = Math.abs(rand.nextInt())%_xData.size();
            baggedXData.add(_xData.get(nj)) ;
            baggedYData.add(_label.get(nj));
        }

        for (int i = 0; i < baggedXData.size(); i++) {
            collector.emit(Arrays.asList(baggedXData.get(i),(Object)_label.get(i)));
        }

    }

    @Override
    public void ack(long batchId) {
        LOG.debug("ToyDataSpout.ack batchId: " + batchId);
        currentCycleRun++;
        // thread will sleep after CYCLES_RUN executions
        if(currentCycleRun == CYCLES_RUN) {
            try{
                LOG.debug("finishing execution ToyDataSpout");
                Thread.sleep(Long.MAX_VALUE);
            } catch (Exception e) {
                LOG.error("error sleeping thread", e);
            }
        }

    }

    @Override
    public void close() {
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        Config conf = new Config();
//        conf.setMaxTaskParallelism(32);
        return conf;
    }

    @Override
    public Fields getOutputFields() {
        return new Fields("number","label");
    }

}
