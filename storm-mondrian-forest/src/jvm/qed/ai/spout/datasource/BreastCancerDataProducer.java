package qed.ai.spout.datasource;

import qed.ai.domain.XData;
import qed.ai.spout.DataProducer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by roberto on 1/29/16.
 */
public class BreastCancerDataProducer extends DataProducer {


    private static class LazyHolder {
        private static final BreastCancerDataProducer INSTANCE = new BreastCancerDataProducer(false);
    }
    public static BreastCancerDataProducer getInstance() {
        return LazyHolder.INSTANCE;
    }

    private BreastCancerDataProducer(boolean excludeHeader) {
        super(excludeHeader);

    }

    public int classY() {
        return 2;
    }


    public XData convertLineToXData(String[] line) {
        List<Double> xData = new ArrayList<>();
        for (int i = 2; i < line.length; i++) {
            double aValue = Double.valueOf(line[i]);
            xData.add(aValue);
        }
        double[] result = new double[xData.size()];

        for (int i = 0; i < xData.size(); i++) {
            result[i] = xData.get(i);
        }

        return new XData(result);
    }
    public Integer convertLineToLabel(String[] line) {
        String labelStringValue = line[1];
        Integer returnValue = null;
        switch (labelStringValue) {
            case "M":
                returnValue = 0;
                break;
            case "B":
                returnValue = 1;
                break;

        }
        return returnValue;
    }

    public String convertLabelToName(Integer label) {
        switch (label) {
            case 0:
                return "M";
            case 1:
                return "B";

        }
        return null;
    }
}
