package qed.ai.spout.datasource;

import qed.ai.domain.XData;
import qed.ai.spout.DataProducer;

/**
 * Created by roberto on 22/01/16.
 */
public class IrisCsvDataProducer extends DataProducer {

    private static class LazyHolder {
        private static final IrisCsvDataProducer INSTANCE = new IrisCsvDataProducer(false);
    }

    public static IrisCsvDataProducer getInstance() {
        return LazyHolder.INSTANCE;
    }

    private IrisCsvDataProducer(boolean excludeHeader) {
        super(excludeHeader);
    }

    public int classY() {
        return 3;
    }

    public XData convertLineToXData(String[] line) {
        return new XData(Double.valueOf(line[0]),Double.valueOf(line[1]),Double.valueOf(line[2]),Double.valueOf(line[3]));
    }
    public Integer convertLineToLabel(String[] line) {
        String labelStringValue = line[4];
        Integer returnValue = null;
        switch (labelStringValue) {
            case "Iris-setosa":
                returnValue = 0;
                break;
            case "Iris-versicolor":
                returnValue = 1;
                break;
            case "Iris-virginica":
                returnValue = 2;
                break;
        }
        return returnValue;
    }
    public String convertLabelToName(Integer label) {
        switch (label) {
            case 0:
                return "Iris-setosa";
            case 1:
                return "Iris-versicolor";
            case 2:
                return "Iris-virginica";
        }
        return null;
    }


}
