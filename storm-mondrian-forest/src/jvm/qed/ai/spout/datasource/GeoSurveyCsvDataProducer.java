package qed.ai.spout.datasource;

import qed.ai.domain.XData;
import qed.ai.spout.DataProducer;
import qed.ai.util.RandomFactory;
import redis.clients.jedis.Jedis;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by roberto on 22/01/16.
 */
public class GeoSurveyCsvDataProducer extends DataProducer implements Serializable {
    private static class LazyHolder {
        private static final GeoSurveyCsvDataProducer INSTANCE = new GeoSurveyCsvDataProducer(true);
    }

    // for column CRP
    public int classY() { return 2;}


    public static GeoSurveyCsvDataProducer getInstance() {
        return LazyHolder.INSTANCE;
    }

    private GeoSurveyCsvDataProducer(boolean hasHeader) {
        super(hasHeader);
//        final boolean reset = Boolean.valueOf(System.getProperty("load.csv"));
//        final int totalToOverSample = 360000;
//
//        if(reset) {
//            Jedis jedis = null;
//            try {
//                jedis = getJedis();
//                // doubling the frequency
//                Set<String> keysGrow = jedis.keys("index-grow-*");
//                int size = keysGrow.size();
//                Object[] array = keysGrow.toArray();
//                for (int i = 0; i < totalToOverSample; i++) {
//                    int randomIndex = RandomFactory.getRandom().nextInt(0, size);
//                    String value = jedis.get((String)array[randomIndex]);
//                    if(value.split(",")[5].equals("Y")) {
//                        totalItems++;
//                        jedis.set("index-grow-" + totalItems, value);
//                    } else i--;
//                }
//            } finally {
//                returnJedis(jedis);
//            }
//        }
    }

    public XData convertLineToXData(String[] line) {
        List<Double> xData = new ArrayList<>();
        for (int i = 1; i < line.length; i++) {
            double aValue = Double.valueOf(line[i]);
            xData.add(aValue);
        }
        double[] result = new double[xData.size()];

        for (int i = 0; i < xData.size(); i++) {
            result[i] = xData.get(i);
        }

        return new XData(result);
//        return new XData(Double.valueOf(line[8]),Double.valueOf(line[9]),Double.valueOf(line[10]),Double.valueOf(line[11]),
//                Double.valueOf(line[12]),Double.valueOf(line[13]),Double.valueOf(line[14]),Double.valueOf(line[15]),
//                Double.valueOf(line[16]),Double.valueOf(line[17]),Double.valueOf(line[18]),Double.valueOf(line[19]),
//                Double.valueOf(line[20]),Double.valueOf(line[21]),Double.valueOf(line[22]),Double.valueOf(line[23]),
//                Double.valueOf(line[24]),Double.valueOf(line[25]),Double.valueOf(line[26]),Double.valueOf(line[27]),Double.valueOf(line[28]));
    }
    public Integer convertLineToLabel(String[] line) {
        String labelStringValue = line[0];
//        Integer returnValue = null;
//        switch (labelStringValue) {
//            case "Y":
//                returnValue = 1;
//                break;
//            case "N":
//                returnValue = 0;
//                break;
//        }
        return Integer.valueOf(labelStringValue);
    }
    public String convertLabelToName(Integer label) {
//        switch (label) {
//            case 0:
//                return "N";
//            case 1:
//                return "Y";
//
//        }
        return label.toString();
    }

}
