package qed.ai.spout;

import backtype.storm.Config;
import backtype.storm.task.TopologyContext;
import backtype.storm.tuple.Fields;
import org.apache.log4j.Logger;
import qed.ai.domain.XData;
import storm.trident.operation.TridentCollector;
import storm.trident.spout.IBatchSpout;

import java.util.*;

/**
 * Generates batches of random selected items. It generates duplicates.
 */
public class BatchDataSpout implements IBatchSpout {

    private static final Logger LOG = Logger.getLogger(BatchDataSpout.class);
    private int cyclesToRun;
    private int currentCycleRun = 0;
    private int _totalDataPerModel;
    private static final DataProducer dataSource = DataProducer.getTypeProducer(System.getProperty("type.data"));


    public BatchDataSpout(int totalDataPerModel, int totalTrees, int parallel) {
        _totalDataPerModel = totalDataPerModel;
        this.cyclesToRun = totalTrees/parallel;
        LOG.debug("cycles to run:" + cyclesToRun);
    }

    @Override
    public void open(Map conf, TopologyContext context) {
        LOG.debug("opening");

    }

    @Override
    public void emitBatch(long batchId, TridentCollector collector) {
        //bootstrap aggregating of training data for a random forest
        Random rand = new Random(System.nanoTime());
        List<XData> baggedXData = new ArrayList<>();
        List<Integer> baggedYData = new ArrayList<>();

        for (int j = 0; j < _totalDataPerModel; j++){
            //add a random data point to the training data for this tree
            Map.Entry<XData,Integer> data;
            int nj = Math.abs(rand.nextInt()) % dataSource.getTotalItems();
            while((data = dataSource.getTreeDataByIndex(nj, "grow")) == null) {
                nj = Math.abs(rand.nextInt()) % dataSource.getTotalItems();
            }
            baggedXData.add(data.getKey()) ;
            baggedYData.add(data.getValue());
        }

        for (int i = 0; i < baggedXData.size(); i++) {
            collector.emit(Arrays.asList(baggedXData.get(i),(Object)baggedYData.get(i)));
        }

    }

    @Override
    public void ack(long batchId) {
        currentCycleRun++;
        // thread will sleep after CYCLES_RUN executions
        if(currentCycleRun == cyclesToRun) {
            try{
                LOG.debug("finishing execution BatchDataSpout");
                Thread.sleep(Long.MAX_VALUE);
            } catch (Exception e) {
                LOG.error("error sleeping thread", e);
            }
        }

    }

    @Override
    public void close() {
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        Config conf = new Config();
//        conf.setMaxTaskParallelism(32);
        return conf;
    }

    @Override
    public Fields getOutputFields() {
        return new Fields("number","label");
    }

}
