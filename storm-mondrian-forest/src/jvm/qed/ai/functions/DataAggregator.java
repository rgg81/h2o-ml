package qed.ai.functions;

import backtype.storm.tuple.Values;
import org.apache.log4j.Logger;
import qed.ai.domain.MondrianTree;
import qed.ai.domain.TrainedData;
import qed.ai.domain.XData;
import qed.ai.trident.MondrianForestTopology;
import storm.trident.operation.BaseAggregator;
import storm.trident.operation.TridentCollector;
import storm.trident.tuple.TridentTuple;

import java.util.ArrayList;
import java.util.List;

/**
 * Aggregate data, and on complete builds the mondrian tree.
 */
public class DataAggregator extends BaseAggregator<List<TrainedData>> {

    private static final Logger LOG = Logger.getLogger(DataAggregator.class);
    public List<TrainedData> init(Object batchId, TridentCollector collector) {
        return new ArrayList<>();
    }

    public void aggregate(List<TrainedData> state, TridentTuple tuple, TridentCollector collector) {
        state.add(new TrainedData((XData)tuple.getValue(0), tuple.getInteger(1)));
    }

    public void complete(List<TrainedData> state, TridentCollector collector) {
        try {
            List<XData> xData = new ArrayList<>();
            List<Integer> labels = new ArrayList<>();
            for (TrainedData trainedData : state) {
                xData.add(trainedData.getX());
                labels.add(trainedData.getLabel());
            }
            MondrianTree tree = new MondrianTree(Double.MAX_VALUE,xData,labels,MondrianTree.DEFAULT_DISCOUNT_PARAM,TrainedData.N_CLASS);
            collector.emit(new Values(tree));
        } catch (Exception e) {
            LOG.error("error in emitting trees", e);
        }
    }
}