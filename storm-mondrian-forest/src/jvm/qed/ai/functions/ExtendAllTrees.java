package qed.ai.functions;

import backtype.storm.tuple.Values;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.util.IdentityMap;
import org.apache.log4j.Logger;
import qed.ai.domain.MondrianTree;
import qed.ai.domain.XData;
import storm.trident.operation.TridentCollector;
import storm.trident.state.BaseQueryFunction;
import storm.trident.tuple.TridentTuple;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by roberto on 14/12/15.
 */
public class ExtendAllTrees extends BaseQueryFunction<TreesDB, Map<String,String>> {

    private static final Logger LOG = Logger.getLogger(ExtendAllTrees.class);
    public List<Map<String,String>> batchRetrieve(TreesDB state, List<TridentTuple> inputs) {
        List<Map<String,String>> result = new CopyOnWriteArrayList<>();
        for(TridentTuple input: inputs) {
            Map<String,String> treesFromState = state.getTrees(state.partition);
            result.add(treesFromState);
        }
        return result;
    }

    public void execute(TridentTuple tuple, Map<String,String> forest, TridentCollector collector) {
        XData newData = (XData)tuple.get(0);
        Integer newLabel = tuple.getInteger(1);
        List<MondrianTree> extendedForest = new ArrayList<>();
        Kryo kryo = new Kryo();
        kryo.register(MondrianTree.class);

        for (Map.Entry<String,String> treeSerialized : forest.entrySet()) {
            try {
                MondrianTree tree = MondrianTree.deserialize(treeSerialized.getValue(),kryo);
                //update state extending currents tree with new node
                tree.extend(newData,newLabel);
                extendedForest.add(tree);
            } catch (Exception e) {
                throw new RuntimeException("error in extending tree", e);
            }
        }
        collector.emit(new Values(extendedForest, forest));
    }
}
