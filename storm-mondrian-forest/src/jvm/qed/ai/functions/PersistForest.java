package qed.ai.functions;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Output;
import org.apache.storm.shade.org.apache.commons.codec.binary.Base64;
import qed.ai.domain.MondrianTree;
import storm.trident.operation.TridentCollector;
import storm.trident.state.BaseStateUpdater;
import storm.trident.tuple.TridentTuple;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.*;

/**
 * Receives tuples of Mondrian trees and persist in a map instance. Updates the state database.
 */
public class PersistForest extends BaseStateUpdater<TreesDB> {
    public void updateState(TreesDB state, List<TridentTuple> tuples, TridentCollector collector) {
        Map<String,String> valuesAndIds = new HashMap<>();
        Kryo kryo = new Kryo();
        kryo.register(MondrianTree.class);

        for(TridentTuple t: tuples) {
            MondrianTree tree = (MondrianTree)t.get(0);
            valuesAndIds.put(tree.getId(),tree.serialize(kryo));
        }
        state.setTreesBulk(valuesAndIds);
    }
}
