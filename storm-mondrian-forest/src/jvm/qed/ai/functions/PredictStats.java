package qed.ai.functions;

import backtype.storm.tuple.Values;
import org.apache.log4j.Logger;
import qed.ai.domain.MondrianBlock;
import qed.ai.domain.MondrianTree;
import qed.ai.domain.TrainedData;
import qed.ai.domain.XData;
import qed.ai.spout.DataProducer;
import storm.trident.operation.BaseFunction;
import storm.trident.operation.TridentCollector;
import storm.trident.tuple.TridentTuple;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Print Mondrian Trees in a pretty way.
 */
public class PredictStats extends BaseFunction {
    private static final Logger LOG = Logger.getLogger(PredictStats.class);
    private Map<String,Stats> statsPerTree = new HashMap<>();

    public void execute(TridentTuple tuple, TridentCollector collector) {
        List<List<Double>> predictions = (List<List<Double>>)tuple.get(0);
        List<MondrianTree> trees = (List<MondrianTree>)tuple.get(1);
        XData row = (XData) tuple.get(2);
        Integer expected = tuple.getInteger(3);
        StringBuilder contents = new StringBuilder();

        contents.append("Point to predict:" + row + "\n");
        contents.append("Predictions[TREE_ID/PROB CLASS PREDICTION/PREDICTION/TREE STATS]\n");
        for (int i = 0; i < trees.size(); i++) {
            int classPrediction = predictions.get(i).indexOf(Collections.max(predictions.get(i)));
            String treeId = trees.get(i).getId();
            Stats stats = (statsPerTree.get(treeId)!=null)?statsPerTree.get(treeId):new Stats();
            stats.updateStat(classPrediction, expected);
            contents.append(treeId + "/" + predictions.get(i) +
                    "/" + DataProducer.getTypeProducer().convertLabelToName(classPrediction) +
                    "/" + stats + "\n");
            statsPerTree.put(treeId,stats);
        }
        contents.append("Expected:" + DataProducer.getTypeProducer().convertLabelToName(expected));
        contents.append("\n\n\n\n");

        try {
            FileWriter fileStream = new FileWriter("predict-info.txt", true);
            BufferedWriter out = new BufferedWriter(fileStream);
            out.write(contents.toString());
            out.close();
        } catch (Exception e) {
            LOG.error("error in saving trees to file", e);
        }

        collector.emit(new Values(contents.toString()));

    }

    public static class Stats implements Serializable {
        public int totalPredictions = 0;
        public List<Double> classScorePrediction = MondrianBlock.zeros(TrainedData.N_CLASS);
        public int totalCorrect = 0;
        public List<Double> classScoreCorrectPrediction = MondrianBlock.zeros(TrainedData.N_CLASS);
        @Override
        public String toString() {
            return "totalPredictions:" + totalPredictions + " totalCorrect:" + totalCorrect + " classScoreSummary:"
                    + classScorePrediction() + " classCorrectPredictionScore:" + classScoreCorrectPrediction() +
                    " Accuracy:" + MessageFormat.format("{0,number,#.##%}", getAccuracy());
        }

        private String classScorePrediction() {
            double total = 0;
            for (Double aDouble : classScorePrediction) {
                total = total + aDouble;
            }
            StringBuilder result = new StringBuilder();
            result.append("[ ");
            for (Double aDouble : classScorePrediction) {
                result.append(aDouble + "(" + MessageFormat.format("{0,number,#.##%}", aDouble/total) + ") ");
            }
            result.append("]");
            return result.toString();
        }

        private String classScoreCorrectPrediction() {
            StringBuilder result = new StringBuilder();
            result.append("[ ");
            for (int i = 0; i < classScoreCorrectPrediction.size(); i++) {
                result.append(classScoreCorrectPrediction.get(i) + "(" + MessageFormat.format("{0,number,#.##%}",
                        classScoreCorrectPrediction.get(i)/classScorePrediction.get(i)) + ") ");
            }
            result.append("]");
            return result.toString();
        }

        /**
         * Updates state when a new prediction comes.
         */
        public void updateStat(int classPrediction, int expected) {
            totalPredictions++;
            classScorePrediction.set(classPrediction,classScorePrediction.get(classPrediction)+1);
            if(classPrediction == expected) {
                totalCorrect++;
                classScoreCorrectPrediction.set(classPrediction,classScoreCorrectPrediction.get(classPrediction)+1);
            }
        }
        private double getAccuracy() {
            return Double.valueOf(totalCorrect)/totalPredictions;
        }
    }
}