package qed.ai.functions;

import backtype.storm.tuple.Values;
import org.apache.log4j.Logger;
import qed.ai.domain.MondrianBlock;
import qed.ai.domain.TrainedData;
import qed.ai.domain.XData;
import qed.ai.spout.DataProducer;
import storm.trident.operation.BaseAggregator;
import storm.trident.operation.TridentCollector;
import storm.trident.tuple.TridentTuple;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by roberto on 1/22/16.
 */
public class ConsolidatePredictions extends BaseAggregator<List<List<Double>>> {

    private static final Logger LOG = Logger.getLogger(ConsolidatePredictions.class);
    String fileLocation;
    private Integer expected;
    private XData unLabel;
    private PredictStats.Stats stats = new PredictStats.Stats();

    public ConsolidatePredictions() {
        File dir = new File(".");
        try {
            fileLocation = dir.getCanonicalPath() + File.separator + "result.txt";
        } catch (IOException e) {
            LOG.error("could not find file location");
        }
    }

    public List<List<Double>> init(Object batchId, TridentCollector collector) {
        return new ArrayList<>();
    }


    public void aggregate(List<List<Double>> state, TridentTuple tuple, TridentCollector collector) {
        List<List<Double>> tupleTyped = (List<List<Double>>)tuple.get(0);
        expected = tuple.getInteger(1);
        unLabel = (XData) tuple.get(2);
        state.addAll(tupleTyped);
    }


    public void complete(List<List<Double>> state, TridentCollector collector) {

        final int totalTrees = state.size();

        List<Double> score = MondrianBlock.zeros(TrainedData.N_CLASS);
        for (List<Double> predictionsAllTree : state) {
            double maxValue = predictionsAllTree.get(0);
            int maxIndex = 0;
            for (int i = 1; i < predictionsAllTree.size(); i++) {
                if ( predictionsAllTree.get(i) > maxValue ) {
                    maxValue = predictionsAllTree.get(i);
                    maxIndex = i;
                }
            }
            // adding one vote to the class
            score.set(maxIndex, score.get(maxIndex) + 1 );
        }
        // print score board
        int maxProbIndex = 0;
        double maxProb = score.get(0);
        for (int i = 0; i < score.size(); i++) {
            if(score.get(i) > maxProb) {
                maxProb = score.get(i);
                maxProbIndex = i;
            }
        }
        stats.updateStat(maxProbIndex, expected);
        collector.emit(new Values(score));


        try {
            FileWriter fileStream = new FileWriter(fileLocation, true);
            BufferedWriter out = new BufferedWriter(fileStream);
            out.newLine();
            out.write("total trees:" + totalTrees);
            out.newLine();
            out.write("predict data point:" + unLabel);
            out.newLine();
            out.write("score:" + score + " expected:" + DataProducer.getTypeProducer().convertLabelToName(expected));
            out.newLine();
            if(expected == maxProbIndex) {
                out.write("prediction is correct! :)");
            }
            else out.write("prediction is wrong. :(");
            out.newLine();
            out.write(stats.toString());
            out.newLine();
            //close buffer writer
            out.close();
        } catch (Exception e) {
            LOG.error("error in generating report", e);
        }

    }
}
