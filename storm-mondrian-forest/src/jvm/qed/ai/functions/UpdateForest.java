package qed.ai.functions;

import backtype.storm.tuple.Values;
import com.esotericsoftware.kryo.Kryo;
import qed.ai.domain.MondrianTree;
import storm.trident.operation.TridentCollector;
import storm.trident.state.BaseStateUpdater;
import storm.trident.tuple.TridentTuple;

import java.util.*;

/**
 * Receives extended trees and the original trees. First it removes the old trees and next add the extended trees.
 * Basically, it updates a tree.
 */
public class UpdateForest extends BaseStateUpdater<TreesDB> {
    public void updateState(TreesDB state, List<TridentTuple> tuples, TridentCollector collector) {
        Kryo kryo = new Kryo();
        kryo.register(MondrianTree.class);

        for(TridentTuple t: tuples) {
            List<MondrianTree> extendedForest = (List<MondrianTree>)t.get(0);
            Map<String,String> oldMembers = (Map<String,String>)t.get(1);
            state.removeMembersTree(oldMembers.keySet(),kryo);
            state.addMembersTree(extendedForest,kryo);
            collector.emit(new Values(extendedForest));
        }
    }
}
