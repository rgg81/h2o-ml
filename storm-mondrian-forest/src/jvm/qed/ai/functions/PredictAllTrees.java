package qed.ai.functions;

import backtype.storm.tuple.Values;
import com.esotericsoftware.kryo.Kryo;
import org.apache.log4j.Logger;
import qed.ai.domain.MondrianTree;
import qed.ai.domain.XData;
import storm.trident.operation.TridentCollector;
import storm.trident.state.BaseQueryFunction;
import storm.trident.tuple.TridentTuple;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by roberto on 14/12/15.
 */
public class PredictAllTrees extends BaseQueryFunction<TreesDB, Collection<String>> {

    private static final Logger LOG = Logger.getLogger(PredictAllTrees.class);
    public List<Collection<String>> batchRetrieve(TreesDB state, List<TridentTuple> inputs) {
        List<Collection<String>> result = new CopyOnWriteArrayList<>();
        for(TridentTuple input: inputs) {
            Map<String,String> treesFromState = state.getTrees(state.partition);
            result.add(treesFromState.values());
        }
        return result;
    }

    public void execute(TridentTuple tuple, Collection<String> forest, TridentCollector collector) {
        XData newData = (XData)tuple.get(0);
        List<List<Double>> predictionsPerTree = new ArrayList<>();
        List<MondrianTree> trees = new ArrayList<>();
        Kryo kryo = new Kryo();
        kryo.register(MondrianTree.class);

        for (String treeSerialized : forest) {
            try {
                MondrianTree tree = MondrianTree.deserialize(treeSerialized,kryo);
                //prediction
                List<Double> predictions = tree.predict(newData);
                predictionsPerTree.add(predictions);
                trees.add(tree);
            } catch (Exception e) {
                throw new RuntimeException("error in predict tree", e);
            }
        }
        collector.emit(new Values(predictionsPerTree,trees));
    }
}
