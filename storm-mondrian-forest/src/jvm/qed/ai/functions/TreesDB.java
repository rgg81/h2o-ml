package qed.ai.functions;

import backtype.storm.task.IMetricsContext;
import com.esotericsoftware.kryo.Kryo;
import org.apache.log4j.Logger;
import org.apache.storm.redis.common.config.JedisPoolConfig;
import qed.ai.domain.MondrianTree;
import qed.ai.trident.MondrianForestTopology;
import qed.ai.util.RandomFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import storm.trident.state.State;
import storm.trident.state.StateFactory;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;


/**
 * Created by roberto on 13/12/15.
 */
public class TreesDB implements State {

    private static final Logger LOG = Logger.getLogger(TreesDB.class);
    private final long _expirationTime; // expire key in milliseconds

    private JedisPool _jedisPool;
    public final int partition;


    public TreesDB(JedisPool jedisPool, Long expirationTime, int partition) {
        _jedisPool = jedisPool;
        _expirationTime = expirationTime;
        this.partition = partition;
    }

    public void beginCommit(Long txId) {

    }

    public void commit(Long txId) {

    }


    public void setTreesBulk(Map<String,String> valuesAndIds) {
        Jedis jedis = null;
        try {
            jedis = getJedis();

            for (Map.Entry<String, String> idAndValue : valuesAndIds.entrySet()) {
                int partition = randomPartitionKey();
                jedis.set(partition + ":" + idAndValue.getKey(),idAndValue.getValue());
                jedis.pexpire(partition + ":" + idAndValue.getKey(), _expirationTime);
            }

        } finally {
            if (jedis != null) {
                returnJedis(jedis);
            }
        }
    }

    public void removeMembersTree(Set<String> oldMembersKeys, Kryo kryo) {
        Jedis jedis = null;
        try {
            jedis = getJedis();
            for (String oldTreeString : oldMembersKeys) {
                jedis.del(oldTreeString);
            }

        } catch (Exception e) {
            throw new RuntimeException("error in deserialize tree", e);
        }
        finally {
            if (jedis != null) {
                returnJedis(jedis);
            }
        }
    }

    public void addMembersTree(List<MondrianTree> newMembers, Kryo kryo) {
        Jedis jedis = null;
        try {
            jedis = getJedis();
            for (MondrianTree tree : newMembers) {
                String treeString = tree.serialize(kryo);

                int partition = randomPartitionKey();
                jedis.set(partition + ":" + tree.getId(),treeString);
                jedis.pexpire(partition + ":" + tree.getId(), _expirationTime);
            }

        } finally {
            if (jedis != null) {
                returnJedis(jedis);
            }
        }
    }

    private int randomPartitionKey() {
        return RandomFactory.getRandom().nextInt(0, MondrianForestTopology.TOTAL_KEY_DISTRIBUTION);
    }

    public Map<String,String> getTrees(int partition) {
        Jedis jedis = null;
        try {
            jedis = getJedis();
            Set<String> keys = jedis.keys(partition + ":*");
            Map<String,String> result = new ConcurrentHashMap<>();
            for (String oneKey : keys) {
                String aValue = jedis.get(oneKey);
                if(aValue != null) result.put(oneKey,aValue);
            }
            return result;

        } finally {
            if (jedis != null) {
                returnJedis(jedis);
            }
        }
    }

    /**
     * RedisState.Factory implements StateFactory for single Redis environment.
     *
     * @see StateFactory
     */
    public static class Factory implements StateFactory {
        public static final redis.clients.jedis.JedisPoolConfig DEFAULT_POOL_CONFIG = new redis.clients.jedis.JedisPoolConfig();

        private JedisPoolConfig jedisPoolConfig;
        private long _expirationTime;

        /**
         * Constructor
         *
         * @param config configuration of JedisPool
         */
        public Factory(JedisPoolConfig config, Long expirationTime) {
            this.jedisPoolConfig = config;
            this._expirationTime = expirationTime;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public State makeState(@SuppressWarnings("rawtypes") Map conf, IMetricsContext metrics, int partitionIndex, int numPartitions) {
            JedisPool jedisPool = new JedisPool(DEFAULT_POOL_CONFIG,
                    jedisPoolConfig.getHost(),
                    jedisPoolConfig.getPort(),
                    jedisPoolConfig.getTimeout(),
                    jedisPoolConfig.getPassword(),
                    jedisPoolConfig.getDatabase());

            return new TreesDB(jedisPool,_expirationTime, partitionIndex);
        }
    }

    /**
     * Borrows Jedis instance from pool.
     * <p></p>
     * Note that you should return borrowed instance to pool when you finish using instance.
     *
     * @return Jedis instance
     */
    public Jedis getJedis() {
        return _jedisPool.getResource();
    }

    /**
     * Returns Jedis instance to pool.
     *
     * @param jedis Jedis instance to return to pool
     */
    public void returnJedis(Jedis jedis) {
        jedis.close();
    }


}