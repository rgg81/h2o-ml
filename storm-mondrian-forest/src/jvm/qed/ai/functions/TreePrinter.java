package qed.ai.functions;

import backtype.storm.tuple.Values;
import com.google.common.base.Optional;
import qed.ai.domain.MondrianBlock;
import qed.ai.domain.MondrianTree;
import storm.trident.operation.BaseFunction;
import storm.trident.operation.TridentCollector;
import storm.trident.tuple.TridentTuple;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * Print Mondrian Trees in a pretty way.
 */
public class TreePrinter extends BaseFunction {

    public void execute(TridentTuple tuple, TridentCollector collector) {
        Object treeOrTrees = tuple.get(0);
        if(treeOrTrees instanceof MondrianTree) {
            MondrianTree tree = (MondrianTree)treeOrTrees;
            MondrianBlock.appendTreeToFile(tree);
            collector.emit(new Values(tree));
        }
    }
}