package qed.ai.web;

import java.util.List;

/**
 * Data structure representing a D3 node.
 */
public class D3Node {
    private String name;
    private List<D3Node> children;

    public D3Node() {

    }

    public D3Node(String name, List<D3Node> children) {
        this.name = name;
        this.children = children;
    }

    public String getName() {
        return name;
    }

    public List<D3Node> getChildren() {
        return children;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setChildren(List<D3Node> children) {
        this.children = children;
    }

}
