package qed.ai.web;

import com.esotericsoftware.kryo.Kryo;
import org.apache.storm.redis.common.config.JedisPoolConfig;
import org.restlet.Application;
import org.restlet.Component;
import org.restlet.Restlet;
import org.restlet.data.Protocol;
import org.restlet.engine.application.CorsFilter;
import org.restlet.resource.Directory;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;
import org.restlet.routing.Router;
import qed.ai.domain.MondrianBlock;
import qed.ai.domain.MondrianTree;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.ScanParams;
import redis.clients.jedis.ScanResult;

import java.util.*;

/**
 * Created by roberto on 21/12/15.
 */
public class ForestServerResource extends ServerResource {

    private JedisPoolConfig poolConfig = new JedisPoolConfig.Builder()
            .setHost("localhost").setPort(6379)
            .build();
    private final redis.clients.jedis.JedisPoolConfig DEFAULT_POOL_CONFIG = new redis.clients.jedis.JedisPoolConfig();

    private JedisPool _jedisPool = new JedisPool(DEFAULT_POOL_CONFIG,
            poolConfig.getHost(),
            poolConfig.getPort(),
            poolConfig.getTimeout(),
            poolConfig.getPassword(),
            poolConfig.getDatabase());


    public static void main(String[] args) throws Exception {

        // current project path
        final String currentDir = new java.io.File( "." ).getCanonicalPath();
        Component component = new Component();
        // Create an application
        Application application = new Application() {
            @Override
            public Restlet createInboundRoot() {
                // Create a Restlet router that defines routes
                final Router router = new Router(getContext());

                // Add a CORS filter to allow cross-domain requests
                CorsFilter corsFilter = new CorsFilter(getContext(), router);
                corsFilter.setAllowedOrigins(new HashSet<String>(Arrays.asList("*")));
                corsFilter.setAllowedCredentials(true);

                // Setup up resource routing
                router.attach("/all/{treeId}",ForestServerResource.class);
                router.attach("/all",ForestServerResource.class);
                router.attach("/view", new Directory(getContext(), "file://" + currentDir + "/src/web"));

                return corsFilter;  // Important!
            }
        };

        component.getServers().add(Protocol.HTTP, 8183);
        component.getClients().add(Protocol.FILE);

        component.getDefaultHost().attachDefault(application);
        // Create the HTTP server and listen on port 8183
        component.start();
    }

    @Get("json")
    public List<D3Tree> all() throws Exception {

        String treeId = (String) getRequest().getAttributes().get("treeId");
        String queryRedis;

        Kryo kryo = new Kryo();
        kryo.register(MondrianTree.class);
        List<D3Tree> forestD3 = new ArrayList<>();
        Jedis jedis = null;
        try {
            jedis = getJedis();
            if (treeId == null) {
                //return the most recent tree
                ScanParams params = new ScanParams();
                queryRedis = "*:current:*";
                params.match(queryRedis);
                params.count(100);
                ScanResult<String> scanResult = jedis.scan("0", params);
                List<String> keys = scanResult.getResult();
                for (String key : keys) {
                    // current key
                    String value = jedis.get(key);
                    MondrianTree tree = MondrianTree.deserialize(value, kryo);
                    forestD3.add(new D3Tree(tree.getId(),convertMondrianToD3(tree.getRoot()), tree.getRoot().getData()));
                }
            }
            else {
                // return 100 tree detail versions
                queryRedis = "*:history:" + treeId;
                Set<String> keys = jedis.keys(queryRedis);
                for (String key : keys) {
                    // check if the type key is a sorted set
                    if(jedis.type(key).equals("zset")) {
                        Set<String> valuesInSet = jedis.zrevrange(key,0,100);
                        for (String mondrianTreeSerialized : valuesInSet) {
                            MondrianTree tree = MondrianTree.deserialize(mondrianTreeSerialized, kryo);
                            forestD3.add(new D3Tree(tree.getId(),convertMondrianToD3(tree.getRoot()), tree.getRoot().getData()));
                        }
                    }
                }
            }
        } finally {
            if (jedis != null) {
                returnJedis(jedis);
            }
        }
        return forestD3;
    }



    private D3Node convertMondrianToD3(MondrianBlock root) {

        List<D3Node> children = new ArrayList<>();
        if(root.getLeft()!= null)
            children.add(convertMondrianToD3(root.getLeft()));
        if(root.getRight()!= null)
            children.add(convertMondrianToD3(root.getRight()));

        String label;
        if(root.getLeft() == null && root.getRight() == null) {
            label = root.getData().get(0).toString();
        } else {
            label = root.toString();
        }

        D3Node rootD3 = new D3Node(label, children);
        return rootD3;
    }

    /**
     * Borrows Jedis instance from pool.
     * <p></p>
     * Note that you should return borrowed instance to pool when you finish using instance.
     *
     * @return Jedis instance
     */
    private Jedis getJedis() {
        return _jedisPool.getResource();
    }

    /**
     * Returns Jedis instance to pool.
     *
     * @param jedis Jedis instance to return to pool
     */
    private void returnJedis(Jedis jedis) {
        jedis.close();
    }
}
