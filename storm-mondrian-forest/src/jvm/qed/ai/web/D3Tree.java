package qed.ai.web;

import qed.ai.domain.XData;

import java.util.List;

/**
 * Data structure representing a D3 tree.
 */
public class D3Tree {
    private String uuid;
    private D3Node root;
    private List<XData> dataTree;

    public D3Tree() {

    }

    public D3Tree(String uuid, D3Node root, List<XData> dataTree) {
        this.uuid = uuid;
        this.root = root;
        this.dataTree = dataTree;
    }

    public D3Node getRoot() {
        return root;
    }

    public void setRoot(D3Node root) {
        this.root = root;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public List<XData> getDataTree() {
        return dataTree;
    }

    public void setDataTree(List<XData> dataTree) {
        this.dataTree = dataTree;
    }
}
