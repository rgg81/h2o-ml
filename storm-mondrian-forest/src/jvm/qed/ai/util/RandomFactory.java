package qed.ai.util;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by roberto on 1/20/16.
 */
public class RandomFactory {

    public static abstract class RandomGenerator {
        public Random generate() {
            return ThreadLocalRandom.current();
        }

        /**
         * Returns a pseudorandom, uniformly distributed value between the
         * given least value (inclusive) and bound (exclusive).
         *
         * @param least the least value returned
         * @param bound the upper bound (exclusive)
         * @return the next value
         * @throws IllegalArgumentException if least greater than or equal
         * to bound
         */
        public double nextDouble(double least, double bound) {
            if (least >= bound)
                throw new IllegalArgumentException();
            return generate().nextDouble() * (bound - least) + least;
        }

        /**
         * Returns a pseudorandom, uniformly distributed value between the
         * given least value (inclusive) and bound (exclusive).
         *
         * @param least the least value returned
         * @param bound the upper bound (exclusive)
         * @throws IllegalArgumentException if least greater than or equal
         * to bound
         * @return the next value
         */
        public int nextInt(int least, int bound) {
            if (least >= bound)
                throw new IllegalArgumentException();
            return generate().nextInt(bound - least) + least;
        }
    }
    public static class RandomProd extends RandomGenerator {
        @Override
        public Random generate() {
            return ThreadLocalRandom.current();
        }
    }
    public static class RandomTest extends RandomGenerator {
        @Override
        public Random generate() {
            return new Random(-200999999);
        }
    }

    public static RandomGenerator getRandom() {
        String env = System.getProperty("env");
        if(env == null || env.equals("prod")) {
            return new RandomProd();
        } else if (env.equals("test")) {
            return new RandomTest();
        } else {
            throw new IllegalArgumentException("not found env value");
        }
    }
}
