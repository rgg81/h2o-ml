package qed.ai.util;

import org.apache.log4j.Logger;
import redis.clients.jedis.Jedis;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;

/**
 * Created by roberto on 2/2/16.
 */
public class Flush extends RedisConnector {

    private static Flush INSTANCE = new Flush();
    private static final Logger LOG = Logger.getLogger(Flush.class);

    private Flush() {
        super(6379);
    }

    public static void flushRedis() {
        Jedis jedis = null;
        try {
            jedis = INSTANCE.getJedis();
            jedis.flushDB();

        } finally {
            INSTANCE.returnJedis(jedis);
        }
    }

    public static void flushResultFiles() {
        try {
            Files.delete(new File("result.txt").toPath());
            Files.delete(new File("trees.txt").toPath());
            Files.delete(new File("predict-info.txt").toPath());
        } catch (NoSuchFileException x) {
            LOG.debug("no such file or directory - " + x.getMessage());
        } catch (DirectoryNotEmptyException x) {
            LOG.debug("not empty - " + x.getMessage());
        } catch (IOException x) {
            // File permission problems are caught here.
            LOG.debug("file permission error - " + x.getMessage());
        }
    }

}
