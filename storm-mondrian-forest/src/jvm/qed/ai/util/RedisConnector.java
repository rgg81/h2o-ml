package qed.ai.util;

import org.apache.storm.redis.common.config.JedisPoolConfig;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * Created by roberto on 2/2/16.
 */
public class RedisConnector {

    int _port;
    private final JedisPool _jedisPool;
    public RedisConnector(int port) {
        _port = port;
        _jedisPool = new JedisPool(DEFAULT_POOL_CONFIG,
                poolConfig.getHost(),
               _port,
                poolConfig.getTimeout(),
                poolConfig.getPassword(),
                poolConfig.getDatabase());

    }
    private final JedisPoolConfig poolConfig = new JedisPoolConfig.Builder()
            .setHost("localhost").setTimeout(10000)
            .build();
    private final redis.clients.jedis.JedisPoolConfig DEFAULT_POOL_CONFIG = new redis.clients.jedis.JedisPoolConfig();



    /**
     * Borrows Jedis instance from pool.
     * <p></p>
     * Note that you should return borrowed instance to pool when you finish using instance.
     *
     * @return Jedis instance
     */
    protected Jedis getJedis() {
        return _jedisPool.getResource();
    }

    /**
     * Returns Jedis instance to pool.
     *
     * @param jedis Jedis instance to return to pool
     */
    protected void returnJedis(Jedis jedis) {
        jedis.close();
    }
}
