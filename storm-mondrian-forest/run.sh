#!/usr/bin/env bash

usage ()
{
  echo 'Usage : run [-h] [-m maximum_trees] [-p points_initial] [-r reload_csv_memory] [-l location_csv] [-t type-data] [-d debug_predictions]'
  echo '             -h                    display help'
  echo '             -m maximum_trees      the maximum number of trees that will be grown, default is 4000'
  echo '             -p points_initial     total points converted to a tree data structure, default is 5'
  echo '             -r reload_csv_memory  read the csv specified in the location parameter and load to memory.'
  echo '                                   Possible values are true/false, default false.'
  echo "             -l location_csv       specify the csv file location, default is $HOME/small-dataset.csv"
  echo "             -t type_data          current possible values are iris, geo-survey and breast-cancer"
  echo "             -d debug_predictions  true/false. When true, save all the trees predictions in a file "

  exit
}



while [ "$1" != "" ]; do
case $1 in
        -m )           shift
                       MAX=$1
                       ;;
        -p )           shift
                       POINTS=$1
                       ;;
        -r )           shift
                       RELOAD=$1
                       ;;
        -l )           shift
                       LOCATION=$1
                       ;;
        -h )           shift
                       usage
                       ;;
        -t )           shift
                       TYPE_ALG=$1
                       ;;
        -d )           shift
                       DEBUG=$1
                       ;;
        * )            QUERY=$1
    esac
    shift
done

# extra validation suggested by @technosaurus
if [ "$MAX" = "" ]
then
    MAX=4000
    echo "MAX TREES NOT DEFINED USING DEFAULT $MAX"
fi
if [ "$POINTS" = "" ]
then
    POINTS=5
    echo "INITIAL POINTS PER TREE NOT DEFINED USING DEFAULT $POINTS"
fi
if [ "$RELOAD" = "" ]
then
    RELOAD=true
    echo "RELOAD CSV NOT DEFINED USING DEFAULT $RELOAD"
fi
if [ "$LOCATION" = "" ]
then
    LOCATION=$HOME/small-dataset.csv
    echo "CSV LOCATION NOT DEFINED USING DEFAULT $LOCATION"
fi
if [ "$TYPE_ALG" = "" ]
then
    TYPE_ALG=geo-survey
    echo "TYPE ALGORITHM NOT DEFINED USING DEFAULT $TYPE_ALG"
fi

if [ "$DEBUG" = "" ]
then
    DEBUG=false
    echo "DEBUG PREDICTIONS NOT DEFINED USING DEFAULT $DEBUG"
fi

RESPONSE=`redis-cli ping`

if [ "$RESPONSE" == "PONG" ]; then
	echo "redis in port 6379 is alive"
else
	echo "STARTING REDIS IN PORT 6379"
	screen -dmS redis-default redis-server
fi

RESPONSE=`redis-cli -p 6380 ping`

if [ "$RESPONSE" == "PONG" ]; then
	echo "redis in port 6380 is alive"
else
	echo "STARTING REDIS IN PORT 6380"
	screen -dmS redis-6380 redis-server --port 6380
fi

mvn -e compile exec:java -Dstorm.topology=qed.ai.trident.MondrianForestTopology -Dtotal.trees=$MAX -Dpoints.trees=$POINTS -Dload.csv=$RELOAD -Dcsv.location=$LOCATION -Dtype.data=$TYPE_ALG -Ddebug.predictions=$DEBUG
