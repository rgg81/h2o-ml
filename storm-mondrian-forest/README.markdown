# Mondrian Tree Algorithm with Apache Storm


# Getting started

## Prerequisites

First, you need `java` and `git` installed and in your user's `PATH`.


## Install Maven

Install [Maven](http://maven.apache.org/) (preferably version 3.x) by following
the [Maven installation instructions](http://maven.apache.org/download.cgi).

## Install Redis

Install [Redis](http://redis.io/) (latest version) by following
the [Redis Quick Start](http://redis.io/topics/quickstart).

Do not forget to execute these commands.

    $ sudo cp src/redis-server /usr/local/bin/
    $ sudo cp sudo cp src/redis-cli /usr/local/bin/

Start Redis

    $ redis-server

## Running topology with Maven

> Note: required that you are in current directory /storm-mondrian-forest.

to compile and run `MondrianTreeTopology` in local mode, use the command:

    $ mvn compile exec:java -Dstorm.topology=qed.ai.trident.MondrianForestTopology -Dtotal.trees=4000 -Dpoints.trees=5 -Dload.csv=true -Dcsv.location=$HOME
 

## Running using the bash shell script

type the command:

    $ ./run.sh -h
    
Usage : run [-h] [-m maximum_trees] [-p points_initial_per_tree] [-r reload_csv_memory] [-l location_csv]  
             -h                    display help  
             -m maximum_trees      the maximum number of trees that will be grown, default is 4000  
             -p points_initial     total points converted to a tree data structure, default is 5  
             -r reload_csv_memory  read the csv specified in the location parameter and load to memory.  
                                   Possible values are true/false, default false.  
             -l location_csv       specify the csv file location, default is $HOME/small-dataset.csv  
             -t type_data          current possible values are iris, geo-survey and breast-cancer  
             -d debug_predictions  true/false. When true, save all the trees predictions in a file  

## Understanding how the topology works

### Importing data

The csv file is imported during topology initialization. At this current time,  
the csv must follow a specified format depending of the choose type data (iris, geo-survey and breast-cancer).    
Depending of the csv size, it takes time to read. To avoid waiting every time, you can set the -r option in script to false.    

The data are split into test, batch training sets and real time training sets.  The proportions  
are 85% batch training sets, 10% tests and 5% real time training sets. Every time is made a reload  
the data are shuffled.
 
### Grow trees stream

It begins with a batch spout. It sends mini batches of data. These data are converted into a data tree structure.  
Initially, the topology is set up to build 4 trees in parallel, in each mini batch execution. For instance:  
if you set maximum trees to 4000, it will have to run 1000 times. Size of mini batch data is set in option -p.  
After reaching the maximum number of trees the spout is forcibly put in sleep state. All trees are stored in a file  
called trees.txt. It could be useful to debug how trees are being generated.

### Update trees stream

The extend data spout emits one point at a time. It waits 6 seconds before start.  
This is done just to wait some trees to be grown. The points are broadcast to all  
trees and then all trees are updated with new data point. It is set up to run 10 times  
using random data set elements.

### Predict stream

The predict spout emits on point at a time. It waits 30 seconds before start. The data points  
are sent in sequential order. It will sleep after predicting all points.
This done just to wait some trees to grow and updated. The points are broadcast to all  
trees and then all trees predict. Finally, all predictions are aggregated and a score is made.  
The final prediction will be the majority in score. All predictions are stored in a file  
called predict-info.txt when option -d is set to true. Be careful because depending the number of total trees the file could get  
to big.  It could be useful to debug how predictions are made by trees.      

### Seeing the prediction results

The predictions are stored in results.txt. At each prediction, the file is open and appended  
with the new prediction. It shows the percentage of success and how many trees exist.



## Visualizing the forest

> Note: required that you are in current directory /storm-mondrian-forest.

to compile and run `ForestServerResource`, use the command:

    $ mvn compile exec:java -Dstorm.topology=qed.ai.web.ForestServerResource
    
It will start a local http server in port 8183. It has a html and a rest service to get all trees. View in address
[localhost:8183/view/forest.html](http://localhost:8183/view/forest.html).

Be carefully because the topology generates a big number of trees. It is better to clean the database before run it,
using the command:

    $ redis-cli
    127.0.0.1:6379> flushdb



## Running unit tests

Use the following Maven command to run the unit tests.

    $ mvn test

