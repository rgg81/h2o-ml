package qed.ai;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import com.google.common.collect.ImmutableList;
import org.apache.storm.redis.common.config.JedisPoolConfig;
import org.testng.annotations.Test;
import qed.ai.domain.MondrianTree;
import qed.ai.domain.XData;
import qed.ai.functions.ConsolidatePredictions;
import qed.ai.functions.PredictAllTrees;
import qed.ai.functions.TreesDB;
import storm.trident.TridentState;
import storm.trident.TridentTopology;
import storm.trident.operation.Filter;
import storm.trident.operation.TridentOperationContext;
import storm.trident.state.StateSpec;
import storm.trident.testing.FeederBatchSpout;
import storm.trident.tuple.TridentTuple;

import java.util.List;
import java.util.Map;

import static org.fest.assertions.api.Assertions.assertThat;

/**
 * Tests for function ConsolidatePredictions
 */
public class ConsolidatePredictionsIntegrationTest extends TopologyTestCase {

    @Test()
    public void testConsolidatePredictions() throws Exception {

        // clean database
        MockRedisData.flushRedis();
        MondrianTree tree1 = new MondrianTree(Double.MAX_VALUE,dataAll,labelAll,MondrianTree.DEFAULT_DISCOUNT_PARAM, 3);
        MondrianTree tree2 = new MondrianTree(Double.MAX_VALUE,dataAll,labelAll,MondrianTree.DEFAULT_DISCOUNT_PARAM, 3);
        // insert tree
        MockRedisData.insertTree(tree1);
        MockRedisData.insertTree(tree2);

        TridentTopology topology = new TridentTopology();
        // This time we use a "FeederBatchSpout", a spout designed for testing.
        FeederBatchSpout testSpout = new FeederBatchSpout(ImmutableList.of("unLabel", "expected"));

        JedisPoolConfig poolConfig = new JedisPoolConfig.Builder()
                .setHost("localhost").setPort(6381)
                .build();
        TreesDB.Factory factory = new TreesDB.Factory(poolConfig,1000l);
        StateSpec spec = new StateSpec(factory);
        spec.requiredNumPartitions = 1;
        TridentState staticState = topology.newStaticState(spec);

        topology.newStream("spout2", testSpout).parallelismHint(1)
                .stateQuery(staticState, new Fields("unLabel", "expected"), new PredictAllTrees(), new Fields("predictions","trees"))
                .parallelismHint(1)
                .aggregate(new Fields("predictions","expected", "unLabel"), new ConsolidatePredictions(), new Fields("majorityPrediction"))
                .each(new Fields("majorityPrediction"), new FilterTest());

        LocalCluster cluster = new LocalCluster();

        Config conf = new Config();
        conf.setDebug(true);

        cluster.submitTopology("mondrian-trees", conf, topology.build());
        testSpout.feed(ImmutableList.of(new Values(new XData(-2.5,-2.0), 0)));

    }

    public class FilterTest implements Filter {
        @Override
        public void prepare(Map conf, TridentOperationContext context) {

        }

        @Override
        public boolean isKeep(TridentTuple tuple) {
            List<Double> score = (List<Double>) tuple.get(0);
            assertThat(score.toString()).isEqualTo("[0.0, 2.0]");
            return true;
        }

        @Override
        public void cleanup() {

        }
    }
}
