package qed.ai;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.Testing;
import backtype.storm.testing.TrackedTopology;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import com.google.common.collect.ImmutableList;
import org.powermock.modules.testng.PowerMockTestCase;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import qed.ai.domain.MondrianTree;
import qed.ai.domain.XData;
import qed.ai.functions.DataAggregator;
import qed.ai.spout.BatchDataSpout;
import storm.trident.TridentTopology;
import storm.trident.operation.Filter;
import storm.trident.operation.TridentOperationContext;
import storm.trident.testing.FeederBatchSpout;
import storm.trident.tuple.TridentTuple;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.fest.assertions.api.Assertions.assertThat;

/**
 * Tests for DataAggregator
 */
public class DataAggregatorIntegrationTest extends TopologyTestCase {

    @Test()
    public void testAggregateDataPointsAndBuildTrees() throws Exception {

        TridentTopology topology = new TridentTopology();
        // This time we use a "FeederBatchSpout", a spout designed for testing.
        FeederBatchSpout testSpout = new FeederBatchSpout(ImmutableList.of("number","label"));
        topology.newStream("spout1", testSpout).parallelismHint(1).
                partitionAggregate(new Fields("number","label"), new DataAggregator(), new Fields("tree")).each(new Fields("tree"), new FilterTest());
        LocalCluster cluster = new LocalCluster();

        Config conf = new Config();
        conf.setDebug(true);

        cluster.submitTopology("mondrian-trees", conf, topology.build());
        testSpout.feed(ImmutableList.of(new Values(new XData(1.0,2.0), 0),new Values(new XData(-1.0,-2.0), 1)));

    }

    public class FilterTest implements Filter {
        @Override
        public void prepare(Map conf, TridentOperationContext context) {

        }

        @Override
        public boolean isKeep(TridentTuple tuple) {
            MondrianTree tree = (MondrianTree) tuple.get(0);
            assertThat(tree.toString()).isEqualTo("rootNode: [(d:0 l:-0.66)]");
            assertThat(tree.getRoot().getData().toString()).isEqualTo((Arrays.asList(new XData(1.0,2.0),new XData(-1.0,-2.0))).toString());
            return true;
        }

        @Override
        public void cleanup() {

        }
    }
}
