package qed.ai;

import com.esotericsoftware.kryo.Kryo;
import org.apache.log4j.Logger;
import qed.ai.domain.MondrianBlock;
import qed.ai.domain.MondrianTree;
import qed.ai.util.RedisConnector;
import redis.clients.jedis.Jedis;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;

/**
 * Created by roberto on 2/2/16.
 */
public class MockRedisData extends RedisConnector {

    private static MockRedisData INSTANCE = new MockRedisData();
    private static final Logger LOG = Logger.getLogger(MockRedisData.class);

    private MockRedisData() {
        super(6381);
    }

    public static void flushRedis() {
        Jedis jedis = null;
        try {
            jedis = INSTANCE.getJedis();
            jedis.flushDB();

        } finally {
            INSTANCE.returnJedis(jedis);
        }
    }

    public static void insertTree(MondrianTree tree) {
        Jedis jedis = null;
        try {
            jedis = INSTANCE.getJedis();
            jedis.set("0:" + tree.getId(), tree.serialize(new Kryo()));

        } finally {
            INSTANCE.returnJedis(jedis);
        }
    }

}
