package qed.ai;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import org.apache.storm.shade.org.apache.commons.codec.binary.Base64;
import org.powermock.modules.testng.PowerMockTestCase;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import qed.ai.domain.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.fest.assertions.api.Assertions.assertThat;


/**
 * Tests for MondrianTree.
 */
public class MondrianTreeTest extends PowerMockTestCase {

    private List<XData> dataAll = new ArrayList<>(
            Arrays.asList(new XData(-0.5, -1.0), new XData(-2.0, -2.0), new XData(+1.0, +0.5),
                    new XData(-3.0, -1.5), new XData(-1.0, +1.0), new XData(-1.5, +1.5))
    );
    private List<Integer> labelAll = new ArrayList<>(
            Arrays.asList(0,0,1,1,2,2)
    );

    private List<XData> data1 = new ArrayList<>(
            Arrays.asList(new XData(-0.5, -1.0))
    );
    private List<Integer> label1 = new ArrayList<>(
            Arrays.asList(0)
    );


    private List<XData> data2 = new ArrayList<>(
            Arrays.asList(new XData(-0.5, -1.0), new XData(-2.0, -2.0))
    );
    private List<Integer> label2 = new ArrayList<>(
            Arrays.asList(0,0)
    );


    private List<XData> data3 = new ArrayList<>(
            Arrays.asList(new XData(-0.5, -1.0), new XData(-2.0, -2.0), new XData(+1.0, +0.5))
    );
    private List<Integer> label3 = new ArrayList<>(
            Arrays.asList(0,0,1)
    );

    private List<XData> data4 = new ArrayList<>(
            Arrays.asList(new XData(-0.5, -1.0), new XData(-2.0, -2.0), new XData(+1.0, +0.5),
                    new XData(-3.0, -1.5))
    );
    private final List<Integer> label4 = new ArrayList<>(
            Arrays.asList(0,0,1,1)
    );

    private List<XData> data5 = new ArrayList<>(
            Arrays.asList(new XData(-0.5, -1.0), new XData(-2.0, -2.0), new XData(+1.0, +0.5),
                    new XData(-3.0, -1.5), new XData(-1.0, +1.0))
    );
    private List<Integer> label5 = new ArrayList<>(
            Arrays.asList(0,0,1,1,2)
    );

    @BeforeSuite
    public void before() {
        // provide a fixed seed to the random stuff
        System.setProperty("env", "test");
    }

    @DataProvider
    public Object[][] trees() {
        return new Object[][]{
                { dataAll, new XData(-3.0,-2.0), new XData(1.0,1.5), 7.5, labelAll },
                { data1, new XData(-0.5, -1.0), new XData(-0.5, -1.0), 0, label1 },
                { data2, new XData(-2.0,-2.0), new XData(-0.5, -1.0), 2.5, label2 },
                { data3, new XData(-2.0,-2.0), new XData(1.0,0.5), 5.5, label3 },
                { data4, new XData(-3.0,-2.0), new XData(1.0,0.5), 6.5, label4 },
                { data5, new XData(-3.0,-2.0), new XData(1.0,1.0), 7.0, label5 }
        };
    }

    @DataProvider
    public Object[][] treesExtended() {
        return new Object[][]{
                { dataAll,new XData(0.5,1.0), labelAll, 1 },
                { data1,  new XData(-0.5,-1.0), label1, 0 },
                { data2,  new XData(-1.0,-1.0), label2, 0 },
                { data3,  new XData(-1.0,1.0), label3, 2 },
                { data4,  new XData(2.0,0.5), label4, 1 },
                { data5,  new XData(3.0,1.5), label5, 1 }
        };
    }

    @Test()
    public void testSerializeAndDeserialize() throws Exception {
        Kryo kryo = new Kryo();

        MondrianTree tree = new MondrianTree(Double.POSITIVE_INFINITY, dataAll, labelAll, MondrianTree.DEFAULT_DISCOUNT_PARAM, 3);
        //Serialize object using kryo
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        Output output = new Output(outStream);
        kryo.writeObject(output, tree);
        output.flush();
        String binaryData = Base64.encodeBase64String(outStream.toByteArray());
        output.close();

        InputStream stream = new ByteArrayInputStream(Base64.decodeBase64(binaryData));
        Input input = new Input(stream);
        MondrianTree treeDeserialized = kryo.readObject(input, MondrianTree.class);
        input.close();

        assertThat(tree.getId()).isEqualTo(treeDeserialized.getId());
        assertThat(tree.getRoot().toString()).isEqualTo(treeDeserialized.getRoot().toString());

    }

    @Test(dataProvider = "trees")
    public void testGrowTree(List<XData> data, XData lowerBounds, XData upperBounds, double linearDimension, List<Integer> label) throws Exception {

        MondrianTree tree = new MondrianTree(Double.POSITIVE_INFINITY, data, label, MondrianTree.DEFAULT_DISCOUNT_PARAM, 3);
        assertThat(tree.getRoot().getParent()).isEqualTo(null);

        MondrianBlock root = tree.getRoot();
        assertThat(root.getLowerBounds()).isEqualsToByComparingFields(lowerBounds);
        assertThat(root.getUpperBounds()).isEqualsToByComparingFields(upperBounds);
        assertThat(root.getLinearDimension()).isEqualTo(linearDimension);
        validateBlock(root);

    }

    @Test(dataProvider = "treesExtended")
    public void testUpdateTree(List<XData> data, XData newValue, List<Integer> label, int newLabel) throws Exception {

        MondrianTree tree = new MondrianTree(Double.POSITIVE_INFINITY, data, label, MondrianTree.DEFAULT_DISCOUNT_PARAM, 3);
        tree.extend(newValue, newLabel);

        List<XData> expectedData = new ArrayList<>();
        expectedData.addAll(data);
        expectedData.add(newValue);

        MondrianBlock root = tree.getRoot();

        assertThat(root.getData()).isEqualTo(expectedData);
        validateBlock(root);

    }

    @Test
    public void testGrowAndPredict() throws Exception {
        MondrianTree tree = new MondrianTree(Double.POSITIVE_INFINITY, dataAll, labelAll, MondrianTree.DEFAULT_DISCOUNT_PARAM, 3);
        assertThat(tree.getRoot().getClassFreq()).isEqualTo(Arrays.asList(1,2,1));
        assertThat(tree.getRoot().getTab()).isEqualTo(Arrays.asList(1,1,1));
        assertThat(tree.getRoot().getClassProb()).isEqualTo(Arrays.asList(0.24999999999999997, 0.49999999999999994, 0.24999999999999997));

        assertThat(tree.getRoot().getLeft().getClassFreq()).isEqualTo(Arrays.asList(0,1,0));
        assertThat(tree.getRoot().getLeft().getTab()).isEqualTo(Arrays.asList(0,1,0));
        assertThat(tree.getRoot().getLeft().getClassProb()).isEqualTo(Arrays.asList(0.0, 1.0, 0.0));

        assertThat(tree.getRoot().getRight().getClassFreq()).isEqualTo(Arrays.asList(2, 1, 2));
        assertThat(tree.getRoot().getRight().getTab()).isEqualTo(Arrays.asList(1,1,1));
        assertThat(tree.getRoot().getRight().getClassProb()).isEqualTo(Arrays.asList(0.3626295168053981, 0.27474096638920364, 0.3626295168053981));

        assertThat(tree.predict(new XData(-3.5,-2.5))).isEqualTo(Arrays.asList(0.04010173670905967, 0.9197965265818806, 0.04010173670905967));
        assertThat(tree.predict(new XData(-0.5,0.5))).isEqualTo(Arrays.asList(0.9244808827940719, 0.05111462495100831, 0.024404492254919717));
        assertThat(tree.predict(new XData(-1.25,1.25))).isEqualTo(Arrays.asList(0.024689558264533264, 0.020504389197095428, 0.9548060525383713));

    }

    @Test
    public void testGrowExtendAndPredict() throws Exception {
        MondrianTree tree = new MondrianTree(Double.POSITIVE_INFINITY, data4, label4, MondrianTree.DEFAULT_DISCOUNT_PARAM, 3);

        tree.extend( new XData(-1.0, +1.0), 2);
        tree.extend(new XData(-1.5, +1.5), 2);

        assertThat(tree.predict(new XData(-3.5,-2.5))).isEqualTo(Arrays.asList(0.04190462191324615, 0.9287488209796092, 0.0293465571071446));
        assertThat(tree.predict(new XData(-0.5,0.5))).isEqualTo(Arrays.asList(0.9363668983727202, 0.026452561871980966, 0.037180539755298773));
        assertThat(tree.predict(new XData(-1.25,1.25))).isEqualTo(Arrays.asList(0.05804848685606488, 0.032539658631463304, 0.9094118545124718));


    }


    private void validateBlock(MondrianBlock block) {

        MondrianBlock.Split splitRoot =block.getSplit();
        MondrianBlock left = block.getLeft();
        MondrianBlock right = block.getRight();
        if(left != null) {
            for (XData xData : left.getData()) {
                assertThat(xData.getDimensionByIndex(splitRoot.getSplitDimension())).isLessThanOrEqualTo(splitRoot.getSplitLocation());
            }
            validateBlock(block.getLeft());
        }

        if(right != null) {
            for (XData xData : right.getData()) {
                assertThat(xData.getDimensionByIndex(splitRoot.getSplitDimension())).isGreaterThan(splitRoot.getSplitLocation());
            }
            validateBlock(block.getRight());
        }

    }

}
