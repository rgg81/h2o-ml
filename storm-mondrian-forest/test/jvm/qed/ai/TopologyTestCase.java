package qed.ai;

import org.powermock.modules.testng.PowerMockTestCase;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import qed.ai.domain.XData;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by roberto on 2/13/16.
 */
public class TopologyTestCase extends PowerMockTestCase implements Serializable {

    protected final List<XData> dataAll = new ArrayList<>(
            Arrays.asList(new XData(-0.5, -1.0), new XData(-2.0, -2.0), new XData(+1.0, +0.5),
                    new XData(-3.0, -1.5), new XData(-1.0, +1.0), new XData(-1.5, +1.5))
    );
    protected final List<Integer> labelAll = new ArrayList<>(
            Arrays.asList(0,0,1,1,2,2)
    );
    @BeforeSuite
    public void before() {
        // provide a fixed seed to the random stuff
        System.setProperty("env", "test");
        System.setProperty("type.data", "test");
        StringBuilder out = new StringBuilder();
        String s;
        Process p;
        try {
            p = Runtime.getRuntime().exec("redis-cli -p 6381 ping");
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(p.getErrorStream()));
            while ((s = br.readLine()) != null){
                out.append(s);
            }
            System.out.println(out.toString());
            p.waitFor();
            p.destroy();
            if(out.toString().contains("Could not connect to Redis ")) {
                // start redis in port 6381 for integration test
                System.out.println("not started. Starting...");
                Process processStartRedis = Runtime.getRuntime().exec("redis-server --port 6381");
                // redis test server needs some time to start
                Thread.sleep(3000l);
                System.out.println("ending");
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @AfterSuite
    public void after() throws Exception {
        System.out.println("shutdown test redis server port 6381");
        Runtime.getRuntime().exec("redis-cli -p 6381 shutdown");

    }


}
