package qed.ai;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import com.google.common.collect.ImmutableList;
import org.apache.storm.redis.common.config.JedisPoolConfig;
import org.mockito.Mockito;
import org.testng.annotations.Test;
import qed.ai.domain.MondrianTree;
import qed.ai.domain.TrainedData;
import qed.ai.domain.XData;
import qed.ai.functions.ExtendAllTrees;
import qed.ai.functions.TreesDB;
import storm.trident.TridentState;
import storm.trident.TridentTopology;
import storm.trident.operation.Filter;
import storm.trident.operation.TridentOperationContext;
import storm.trident.state.StateSpec;
import storm.trident.testing.FeederBatchSpout;
import storm.trident.tuple.TridentTuple;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * Created by roberto on 2/13/16.
 */
public class ExtendAllTreesIntegrationTest extends TopologyTestCase {


    @Test
    public void testExtendTrees() throws Exception {

        // clean database
        MockRedisData.flushRedis();
        MondrianTree tree = new MondrianTree(Double.MAX_VALUE,dataAll,labelAll,MondrianTree.DEFAULT_DISCOUNT_PARAM, 3);
        // insert tree
        MockRedisData.insertTree(tree);

        TridentTopology topology = new TridentTopology();
        // This time we use a "FeederBatchSpout", a spout designed for testing.
        FeederBatchSpout testSpout = new FeederBatchSpout(ImmutableList.of("number","label"));

        JedisPoolConfig poolConfig = new JedisPoolConfig.Builder()
                .setHost("localhost").setPort(6381)
                .build();
        TreesDB.Factory factory = new TreesDB.Factory(poolConfig,1000l);
        StateSpec spec = new StateSpec(factory);
        spec.requiredNumPartitions = 1;
        TridentState staticState = topology.newStaticState(spec);

        topology.newStream("spout2", testSpout).parallelismHint(1)
                .stateQuery(staticState, new Fields("number","label"), new ExtendAllTrees(), new Fields("extendedTrees", "originalTrees"))
                .parallelismHint(1)
                .each(new Fields("extendedTrees"), new FilterTest());

        LocalCluster cluster = new LocalCluster();

        Config conf = new Config();
        conf.setDebug(true);

        cluster.submitTopology("mondrian-trees", conf, topology.build());
        testSpout.feed(ImmutableList.of(new Values(new XData(-2.5,-2.0), 0)));

    }

    public class FilterTest implements Filter {
        @Override
        public void prepare(Map conf, TridentOperationContext context) {

        }

        @Override
        public boolean isKeep(TridentTuple tuple) {
            List<MondrianTree> extended = (List<MondrianTree>) tuple.get(0);
            assertThat(extended.size()).isEqualTo(1);
            MondrianTree tree = extended.get(0);
            assertThat(tree.toString()).isEqualTo("rootNode: [(d:0 l:-2.31)]");
            assertThat(tree.getRoot().getData().toString()).isEqualTo(
                    (Arrays.asList(new XData(-0.5, -1.0), new XData(-2.0, -2.0), new XData(+1.0, +0.5),
                            new XData(-3.0, -1.5), new XData(-1.0, +1.0), new XData(-1.5, +1.5), new XData(-2.5,-2.0))).toString()
            );
            return true;
        }

        @Override
        public void cleanup() {

        }
    }

}
