#!/usr/bin/env python

from config import *
from io import StringIO, BytesIO

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import numpy as np
import os
from sklearn.externals import joblib
import time
import tornado.ioloop
import tornado.web

def plot_predictions(X, xx, yy, rf):
    fig = plt.figure()
    ax = fig.add_subplot(111)

    plot_colors = "ryb"
    cmap = plt.cm.RdYlBu
    estimator_alpha = 1.0 / rf.n_estimators
    for tree in rf.estimators_:
        Z = tree.predict(X)
        Z = Z.reshape(xx.shape)
        ax.contourf(xx, yy, Z, alpha=estimator_alpha, cmap=cmap)

    return fig

def generate_test_data():
    """
        Generate test data. See:
        http://scikit-learn.org/stable/auto_examples/ensemble/plot_forest_iris.html
    """
    plot_step = 0.02
    x_min, x_max = -3.0, 3.0
    y_min, y_max = -3.0, 3.0
    xx, yy = np.meshgrid(np.arange(x_min, x_max, plot_step),
                         np.arange(y_min, y_max, plot_step))
    X = np.c_[xx.ravel(), yy.ravel()]

    return X, xx, yy

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("""Navigate to:<br/>
                      <ul>
                        <li><a href="/current/">/current/</a> to view predictions from the most recent model.</li>
                        <li>/&lt;timestamp&gt;/ to view predictions from an earlier model</li>
                      </ul>
                    """)

class ImgHandler(tornado.web.RequestHandler):
    def get(self, arg='current'):
        rf = joblib.load(MODEL_DIR + arg + '.p')
        X, xx, yy = generate_test_data()
        figure = plot_predictions(X, xx, yy, rf)

        io = StringIO()
        plt.savefig(io, format='svg')
        plt.close()
        self.set_header("Content-Type", "image/svg+xml")
        self.write(io.getvalue())

if __name__ == "__main__":
    application = tornado.web.Application([
      (r"/", MainHandler),
      (r"/(.*)/", ImgHandler)
      ])
    application.listen(8888)

    print "Server running at: http://127.0.0.1:8888/"
    print "Press ^C to exit"
    tornado.ioloop.IOLoop.instance().start()
