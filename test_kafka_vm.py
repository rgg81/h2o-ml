#!/usr/bin/env python

from kafka import KafkaClient, SimpleProducer, SimpleConsumer
import logging.config

logging.config.fileConfig('conf/logging.conf')

kafka = KafkaClient("localhost:9092")
kafka.ensure_topic_exists('turkey')
producer = SimpleProducer(kafka)
for i in range(3):
    print 'Pushed: gobble gobble ' + str(i)
    producer.send_messages("turkey", "gobble gobble " + str(i))

consumer = SimpleConsumer(kafka, "my-group", "turkey")
for message in consumer:
    print 'Success, consumed:', message.message.value
