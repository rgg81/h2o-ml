#!/usr/bin/env python

from config import *
import errno
import matplotlib.pyplot as plt
import numpy as np
import os
from sklearn.ensemble import RandomForestClassifier
from sklearn.externals import joblib
import time

def plot_predictions(X, rf):
    """
        http://scikit-learn.org/stable/auto_examples/ensemble/plot_forest_iris.html
    """
    plot_step = 0.02
    plot_colors = "ryb"
    cmap = plt.cm.RdYlBu

    x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, plot_step),
                         np.arange(y_min, y_max, plot_step))

    estimator_alpha = 1.0 / rf.n_estimators
    for tree in rf.estimators_:
        Z = tree.predict(np.c_[xx.ravel(), yy.ravel()])
        Z = Z.reshape(xx.shape)
        plt.contourf(xx, yy, Z, alpha=estimator_alpha, cmap=cmap)

    for i, c in zip(xrange(rf.n_classes_), plot_colors):
        idx = np.where(Y == i)
        plt.scatter(X[idx, 0], X[idx, 1], c=c, cmap=cmap)

    plt.show()

def load_training_data():
    data = []
    for data_file in os.listdir(DATA_DIR):
        with open(DATA_DIR + data_file, 'r') as f:
            data.extend(f.read().splitlines())

    data = set(data) # deduplicate training data
    data = [d.split('\t') for d in data]

    # each row of data is of the form (timestamp, x1, x2, label)
    X = np.array([[float(x) for x in d[1:-1]] for d in data])
    Y = np.array([int(d[-1]) for d in data])

    # standardize
    mean = X.mean(axis=0)
    std = X.std(axis=0)
    X = (X - mean) / std

    return X, Y

def persist_model(rf):
    timestamp = str(int(time.time()))
    model_file = timestamp + '.p'
    model_path = MODEL_DIR + model_file
    joblib.dump(rf, model_path)

    # symlink current to this model: MODEL_DIR/current.p
    # always points to the most recent batch-trained model
    link_file = MODEL_DIR + 'current.p'
    try:
        os.unlink(link_file)
    except OSError, e:
        if e.errno == errno.ENOENT:
            pass
    os.symlink(model_file, link_file)

if __name__ == "__main__":
    X, Y = load_training_data()

    n_estimators = 10
    max_depth = 9
    max_features = None # use both dimensions
    rf = RandomForestClassifier(n_estimators=n_estimators,
                                max_depth=max_depth,
                                max_features=max_features)
    rf = rf.fit(X, Y)
    persist_model(rf)

    # DEBUG: plot predictions
    plot_predictions(X, rf)
