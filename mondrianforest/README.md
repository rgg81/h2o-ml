# Mondrian Forests

## Quickstart

```
pip install -r requirements.txt
python mondrianforest.py
```

## Plots

The script constructs a Mondrian forest from a toy dataset having three
classes and plots it. Tree splits are denoted by dashed lines and node boundaries
by solid lines. The label on each split is the `tau` for the node corresponding
to that split.

There are 4 plots generated in the same directory:

   1. `mt1.pdf`: The Mondrian tree after adding 2 data points in batch.
   2. `mt2.pdf`: The Mondrian tree after adding 1 data point incrementally.
   3. `mt3.pdf`: The Mondrian tree after adding 3 data points incrementally.
   4. `mtp.pdf`: Soft class predictions of the Mondrian forest (R, G, B).

## Notes

> Lakshminarayanan, Balaji, Daniel M. Roy, and Yee Whye Teh.
> "Mondrian forests: Efficient online random forests."
> Advances in Neural Information Processing Systems. 2014.

This is an implementation of the algorithms from the aforementioned paper (NIPS14).
It differs from the [author's implementation][1] but shares its underlying essence. 

Some notable differences are:

   * We use the "lifetime", "tau" and "E" parameters as in the paper, while the
     author uses the "budget" and "split cost" parameters. They are related as:

      budget = lifetime - parent_tau
      split cost = E

Important notes:

   * If the `discount_parameter` (gamma in the paper) is too high (>10),
     it will cause the posterior distribution at the leaves to be weighted
     entirely towards the counts at that leaf (discount = 0) leading to a
     non-smooth posterior distribution. This can be seen in the author's
     implementation too (where gamma = 10).

     To keep the posterior distributions at the leaves smooth, the default
     discount parameter can be reduced.

     However, smoothness is introduced while the test sample moves to the
     leaf from the root, from every node whose bounding box does not completely
     contain the test sample.

[1]: https://github.com/balajiln/mondrianforest/
