#!/usr/bin/env python

"""
    Implementation of Mondrian Forests.
    See also:
        - Paper: http://papers.nips.cc/paper/5234-mondrian-forests-efficient-online-random-forests.pdf
        - Paper code: https://github.com/balajiln/mondrianforest
"""

import numpy as np
import math
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.colors as mcolors
import sys

# for floating point comparisions
EPSILON = 10 ** -10

# for drawing bounds/splits
LINE_WEIGHT = '0.0'

# colors for the toy data classes
COLORS = ['r', 'g', 'b']

np.random.seed(10)

class MondrianBlock:
    """
        A single partition of the input space
        corresponding to a node in the Mondrian tree.
    """

    def __init__(self, data, labels, nclass, parent, lifetime, fig,
                 tau=None, split=None, recurse=True): # used by _extend()
        # instance variables (data-based)
        self._lower_bounds = []      # bounds depending on this node's data
        self._upper_bounds = []      # bounds depending on this node's data
        self._linear_dimension = 0.0 # sum of bound ranges
        self._tau = 0                # creation "time" (in the Mondrian context)
        self._data = []              # a slice of the original data, for this node
        self._labels = []            # labels for the data in this node
        self._nclass = -1            # number of classes (the same for all nodes)
        self._split = (0,0)          # (split dimension, split location)
        self._lifetime = lifetime    # lifetime of the tree

        # instance variables (debug plotting)
        self._fig = fig              # figure to plot block on
        self._patch = None           # a rectangular patch on the figure
        self._line = None            # split line on the figure
        self._label = None           # label on the split line

        # instance variables (tree-based)
        self._parent = None
        self._left = None
        self._right = None

        # initialise prediction variables
        self._labels = labels
        self._nclass = nclass
        self._class_freq = np.zeros(nclass) # frequency of each class at this node
        self._tab = np.zeros(nclass)        # number of "tables" for each class (IKN)
        self._class_prob = np.zeros(nclass) # predictive posterior distribution

        # (NIPS14, Algorithm 5), if j' is a leaf
        self._class_freq = np.bincount(self._labels[:,0],
                                       minlength=self._nclass)
        self._tab = np.minimum(self._class_freq, 1)

        # initialise instance recursively (NIPS14, Algorithm 1, 2)
        self._data = data
        self._parent = parent
        self._lower_bounds,\
        self._upper_bounds,\
        self._linear_dimension = self._compute_data_bounds(data)

        self._plot_bounds()

        if tau is not None and split is not None:
            # called by extend() which fixes the tau and split
            self._tau = tau
            self._split = split
        else:
            if self._linear_dimension < EPSILON: # single data point in this block
                E = np.inf
            else:
                # np.random.exponential(beta) uses beta = 1/lambda
                E = np.random.exponential(1.0/self._linear_dimension)

            parent_tau = self.get_parent_tau()
            if E + parent_tau >= self._lifetime: # this is a leaf
                self._tau = self._lifetime
                return

            self._tau = E + parent_tau
            self._split = self._sample_split()

        self._plot_split()

        if not recurse: # used by extend to create a new parent node w/ 2 children
            return

        data_l, labels_l, data_r, labels_r = self._split_data()
        self._left = MondrianBlock(data=data_l, labels=labels_l, nclass=self._nclass,
                                   parent=self, lifetime=self._lifetime, fig=fig)
        self._right = MondrianBlock(data=data_r, labels=labels_r, nclass=self._nclass,
                                    parent=self, lifetime=self._lifetime, fig=fig)

        # (NIPS14, Algorithm 5), if j' is not a leaf
        # (in the paper, c_j' = tab_left + tab_right, does not include self._tab)
        # (we deviate because otherwise, higher class freqs may end up having lower
        #  class probs at internal nodes)
        self._class_freq = self._left._tab + self._right._tab
        self._tab = np.minimum(self._class_freq, 1)

    def _sample_split(self):
        # sample a dimension w/ prob. proportional to the range of each dimension 
        dimension_probabilities = (self._upper_bounds - self._lower_bounds) * 1.0 / \
                                   self._linear_dimension
        dimension = np.random.choice(len(self._upper_bounds), # no. of dimensions
                                     1, # pick one dimension
                                     p=dimension_probabilities)[0]

        # sample locations uniformly from lower_bounds - upper_bounds
        location = np.random.uniform(self._lower_bounds[dimension],
                                     self._upper_bounds[dimension])

        return (dimension, location)
    
    def _plot_bounds(self):
        if self._fig is None:
            return

        ax = self._fig.gca()
        bottom_left = self._lower_bounds
        width, height = self._upper_bounds - self._lower_bounds
        self._patch = patches.Rectangle(bottom_left, width, height,
                                        ec=LINE_WEIGHT, fill=False,
                                        zorder=10)
        ax.add_patch(self._patch)

    def _clear_bounds(self):
        if self._fig is None or self._patch is None:
            return
        self._patch.remove()
    
    def _plot_split(self):
        if self._fig is None:
            return
        
        ax = self._fig.gca()
        dimension, location = self.get_split()
        if dimension == 0: # vertical line
            line_start, line_stop = np.array([[location, location],
                                              [self._lower_bounds[1],
                                               self._upper_bounds[1]]])
        elif dimension == 1: # horizontal line
            line_start, line_stop = np.array([[self._lower_bounds[0],
                                               self._upper_bounds[0]],
                                               [location, location]])
        self._line, = ax.plot(line_start, line_stop, '--', color=LINE_WEIGHT,
                              zorder=10)

        text_location = [sum(line_start)/2. + (1 - dimension) * 0.05,
                         sum(line_stop)/2. + dimension * 0.05]
        text_string = "{:2.3f}".format(self._tau)
        if dimension == 1:
            text_rotation = 'horizontal'
        else:
            text_rotation = 'vertical'
        self._label = ax.text(text_location[0], text_location[1], text_string,
                              size=6, rotation=text_rotation, zorder=20)

    def _clear_split(self):
        if self._fig is None or self._line is None:
            return
        self._line.remove()
        self._label.remove()

    def _split_data(self):
        dimension, location = self.get_split()
        idx_l = self._data[:,dimension] <= location
        data_l = self._data[idx_l,:]
        labels_l = self._labels[idx_l,:]
        data_r = self._data[~idx_l,:]
        labels_r = self._labels[~idx_l,:]
        return data_l, labels_l, data_r, labels_r

    def _compute_data_bounds(self, data):
        lower_bounds = np.min(data, 0)
        upper_bounds = np.max(data, 0)
        linear_dimension = np.sum(upper_bounds - lower_bounds)
        return lower_bounds, upper_bounds, linear_dimension

    def _extend(self, row, label):
        # (NIPS14 Algorithm 4)
        extent_lower = np.maximum(0, self._lower_bounds - row)
        extent_upper = np.maximum(0, row - self._upper_bounds)
        extent_linear_dimension = sum(extent_lower + extent_upper)
        if extent_linear_dimension < EPSILON: # new row is within this node
            E = np.inf
        else:
            E = np.random.exponential(1.0/extent_linear_dimension)

        parent_tau = self.get_parent_tau()
        if E + parent_tau < self._tau:
            # create a new parent for the current node, containing the
            # data of the current node + the new row.
            dimension_probabilities = (extent_lower + extent_upper) * 1.0 /\
                                       extent_linear_dimension
            dimension = np.random.choice(len(extent_lower), # no. of dimensions
                                         1, # pick one dimension
                                         p=dimension_probabilities)[0]

            if row[dimension] > self._upper_bounds[dimension]:
                location_range = (self._upper_bounds[dimension], row[dimension])
            else:
                location_range = (row[dimension], self._lower_bounds[dimension])

            location = np.random.uniform(*location_range)

            new_parent_node = MondrianBlock(data=np.vstack([self._data, row]),
                                            labels=np.vstack([self._labels, label]),
                                            nclass=self._nclass,
                                            parent=self._parent,
                                            lifetime=self._lifetime,
                                            fig=self._fig,
                                            tau=E+parent_tau,
                                            split=(dimension, location),
                                            recurse=False)
            if self._parent is not None:
                if self._parent.get_left() == self:
                    self._parent.set_left(new_parent_node)
                else:
                    self._parent.set_right(new_parent_node)
            self._parent = new_parent_node

            # create a new sibling for the current node
            new_sibling = MondrianBlock(data=np.array([row]),
                                        labels=np.array([label]),
                                        nclass=self._nclass,
                                        parent=new_parent_node,
                                        lifetime=self._lifetime, fig=self._fig)

            # set the left and right children of the new parent
            if row[dimension] <= location:
                new_parent_node.set_left(new_sibling)
                new_parent_node.set_right(self)
            else:
                new_parent_node.set_left(self)
                new_parent_node.set_right(new_sibling)

            # (NIPS14, Algorithm 6)
            new_parent_node._class_freq = new_parent_node._left._tab + \
                                          new_parent_node._right._tab
            new_parent_node._tab = np.minimum(new_parent_node._class_freq, 1)
            new_parent_node._update_posterior_counts(label)

            return new_parent_node

        else:
            # update data and bounds of this Mondrian block
            self._data = np.vstack([self._data, row])
            self._labels = np.vstack([self._labels, label])
            self._lower_bounds,\
            self._upper_bounds,\
            self._linear_dimension = self._compute_data_bounds(self._data)

            self._clear_bounds()
            self._plot_bounds()

            self._clear_split() # old split, needs to be extended
            self._plot_split() # uses the new bounds

            if self._left is not None and self._right is not None: # not a leaf
                # recurse
                dimension, location = self.get_split()
                if row[dimension] <= location:
                    child = self._left
                else:
                    child = self._right
                child._extend(row, label)
            else: # leaf
                self._class_freq[label] += 1
                self._update_posterior_counts(label)

            return self

    def extend(self, X, y):
        # (NIPS14 Algorithm 3)
        node = self
        for i in range(X.shape[0]):
            node = node._extend(X[i], y[i])
        return node

    def _update_posterior_counts(self, label):
        if self._tab[label] == 1:
            return
        if (self._left is not None or self._right is not None): # not a leaf
            self._class_freq = self._left._tab + self._right._tab
        self._tab = np.minimum(self._class_freq, 1)

        if self._parent is None:
            return

        return self._parent._update_posterior_counts(label)

    def _compute_predictive_posterior_distribution(self, discount_param):
        # (NIPS14, Algorithm 7)
        if self._parent is None: # root
            # uniform prior at the root
            parent_class_prob = np.ones(self._nclass) / self._nclass
        else:
            parent_class_prob = self._parent._class_prob

        d = math.exp(-discount_param * (self.get_tau() - self.get_parent_tau()))
        self._class_prob = (self._class_freq - d * self._tab + \
                            d * np.sum(self._tab) * parent_class_prob) /\
                           np.sum(self._class_freq)

        if self._left is not None:
            self._left._compute_predictive_posterior_distribution(discount_param)

        if self._right is not None:
            self._right._compute_predictive_posterior_distribution(discount_param)

    def _predict(self, row, class_prob, p_notseparatedyet, discount_param):
        # (NIPS14 Algorithm 8)
        delta = self._tau - self.get_parent_tau()
        extent_lower = np.maximum(0, row - self._upper_bounds)
        extent_upper = np.maximum(0, self._lower_bounds - row)
        eta = np.sum(extent_lower + extent_upper)
        p_split = 1 - math.exp(-delta * eta)

        if p_split > 0: # x lies outside the bounding box, create a new parent
            # Derivation of d (not in the paper):
            #
            # d is the expected value of exp(-discount_param * x), where x is
            # drawn from exponential(eta) truncated to (0, delta]
            #
            # The exponential(a) truncated to (0,b] has the PDF:
            #
            #   f(x) = a * exp(-ax) / (1 - exp(-ab))
            #
            # d = integral of exp(-discount_param * x) from 0 to delta has been
            # hand-verified to work out to the the value below:
            #
            #   d =         eta             1 - exp(-delta(eta + discount_param))
            #       -------------------- X --------------------------------------
            #       eta + discount_param          1 - exp(-delta * eta)
            #
            d = (eta / (eta + discount_param)) * \
                       (-np.expm1(-delta * (eta + discount_param))) / \
                       (-np.expm1(-delta * eta))

            nuparent_class_freq = np.minimum(self._class_freq, 1)
            nuparent_tab = np.minimum(self._class_freq, 1)

            # uniform prior at the root
            if self._parent is None:
                parent_class_prob = np.ones(self._nclass) / self._nclass
            else:
                parent_class_prob = self._parent._class_prob

            nuparent_class_prob = (nuparent_class_freq - d * nuparent_tab + \
                                   d * np.sum(nuparent_tab) * parent_class_prob) / \
                                  np.sum(nuparent_class_freq)

            class_prob = class_prob + \
                          p_notseparatedyet * p_split * nuparent_class_prob

        if self._left is None and self._right is None: # leaf
            class_prob = class_prob + \
                          p_notseparatedyet * (1 - p_split) * self._class_prob
            return class_prob
        else:
            p_notseparatedyet = p_notseparatedyet * (1 - p_split)

            dimension, location = self.get_split()
            if row[dimension] <= location:
                return self._left._predict(row, class_prob, p_notseparatedyet,
                                           discount_param)
            else:
                return self._right._predict(row, class_prob, p_notseparatedyet,
                                            discount_param)

    def get_tau(self):
        return self._tau

    def get_parent_tau(self):
        if self._parent is None:
            return 0 # root
        return self._parent.get_tau()

    def set_left(self, left):
        self._left = left

    def get_left(self):
        return self._left

    def set_right(self, right):
        self._right = right

    def get_right(self):
        return self._right

    def get_data(self):
        return self._data

    def get_split(self):
        return self._split

class MondrianTree:
    """
        A Mondrian tree consisting of nodes (Mondrian blocks)
    """

    def __init__(self, nclass, lifetime=np.inf, fig=None, discount_param=10):
        # instance variables
        self._root = None            # root node, a Mondrian block
        self._lifetime = lifetime    # "lifetime" hyperparameter
        self._fig = fig              # figure to plot on
        self._nclass = nclass        # number of classes
        self._discount_param = discount_param
        assert not math.isinf(lifetime)

    def fit(self, X, y):
        if self._fig is not None:
            # can plot only in 2D
            assert X.shape[1] == 2

        assert X.shape[0] == y.shape[0]

        if self._root is None:
            # first batch of data, initiate the tree
            self._root = MondrianBlock(data=X, labels=y, nclass=self._nclass,
                                       parent=None, lifetime=self._lifetime,
                                       fig=self._fig)
        else:
            self._root = self._root.extend(X, y)

        self._root._compute_predictive_posterior_distribution(self._discount_param)

    def predict(self, X):
        # (NIPS14, Algorithm 8)
        assert self._root is not None

        p_notseparatedyet = 1.0
        probs = [0] * self._nclass
        preds = np.zeros((X.shape[0], self._nclass))
        for i, x in enumerate(X):
            preds[i,:] = self._root._predict(x, probs, p_notseparatedyet,
                                             self._discount_param)

        return preds

    def get_fig(self):
        return self._fig

    def set_fig(self, fig):
        self._fig = fig

    def print_tree(self):
        self._print_tree(self._root)

    def _print_tree(self, node, tabs=''):
        if node is None:
            return

        print tabs + 'DATA:'
        print tabs + str(node.get_data()).replace('\n', '\n' + tabs)
        print tabs + 'LABELS:'
        print tabs + str(node._labels.reshape((-1,)))
        print tabs + 'CLASSFREQ/TAB'
        print tabs + str(node._class_freq) + str(node._tab)
        print tabs + 'CLASSPROB', ', '.join(["{:3.3f}".format(p)
                                             for p in node._class_prob])

        if node._left is None and node._right is None:
            print tabs + 'LEAF'
        else:
            dimension, location = node.get_split()
            print tabs + 'SPLIT (dim, loc, tau):', dimension,
            print "{:3.3f}".format(location),
            print "{:3.3f}".format(node.get_tau())

            print tabs + 'LEFT:'
            self._print_tree(node.get_left(), tabs + '\t')

            print tabs + 'RIGHT:'
            self._print_tree(node.get_right(), tabs + '\t')

# from balajiln/mondrian-forest
def load_toy_mf_data():
    # data points
    X = np.array([[-0.5, -1.0],
                  [-2.0, -2.0],
                  [+1.0, +0.5],
                  [-3.0, -1.5],
                  [-1.0, +1.0],
                  [-1.5, +1.5]])

    # labels
    y = np.array([[0],
                  [0],
                  [1],
                  [1],
                  [2],
                  [2]])
    return X, y

def plot_data(X, y, fig=None):
    if fig is None:
        fig = plt.figure()
    ax = fig.gca()
    for x, y_i in zip(X,y):
        ax.scatter(x[0], x[1], c=COLORS[y_i], zorder=10)
    return fig

def plot_prediction_mesh(X, y, mt, fig):
    top_left = np.min(X, 0)
    bottom_right = np.max(X, 0)
    inc = 0.05

    xx, yy = np.meshgrid(np.arange(top_left[0] - .5,
                                   bottom_right[0] + .5 + inc, inc),
                         np.arange(top_left[1] - .5,
                                   bottom_right[1] + .5 + inc, inc))

    sz = xx.shape[0] * xx.shape[1]
    xy = np.hstack((np.ravel(xx).reshape((sz,1)), np.ravel(yy).reshape((sz,1))))
    z = mt.predict(xy)
    z = z.reshape((xx.shape[0], xx.shape[1], 3))

    cmap = mcolors.ListedColormap(COLORS)
    ax = fig.gca()
    ax.imshow(z, extent=(top_left[0] - 0.5, bottom_right[0] + 0.5,
                         top_left[1] - 0.5, bottom_right[1] + 0.5),
              alpha=0.4, origin='lower')

    return fig

if __name__ == "__main__":
    X, y = load_toy_mf_data()
    fig = plot_data(X[:2,:], y[:2,:])
    mt = MondrianTree(nclass=3, lifetime=sys.maxint, fig=fig)
    mt.fit(X[:2,:], y[:2,:])
    mt.get_fig().savefig('mt1.pdf', type='pdf')
    mt.print_tree()

    fig = plot_data(X[2:3,:], y[2:3,:], fig=fig)
    mt.set_fig(fig)
    mt.fit(X[2:3,:], y[2:3,:])
    mt.get_fig().savefig('mt2.pdf', type='pdf')
    mt.print_tree()

    fig = plot_data(X[3:6,:], y[3:6,:], fig=fig)
    mt.set_fig(fig)
    mt.fit(X[3:6,:],y[3:6,:])
    mt.get_fig().savefig('mt3.pdf', type='pdf')
    mt.print_tree()

    fig = plot_prediction_mesh(X, y, mt, fig)
    fig.savefig('mtp.pdf', type='pdf')
