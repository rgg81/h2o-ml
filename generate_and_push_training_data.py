#!/usr/bin/env python

from config import *
from kafka import KafkaClient, SimpleProducer
import logging
import logging.config
import matplotlib.pyplot as plt
import numpy as np
import sys
import time

logging.config.fileConfig(LOG_FILE)
log = logging.getLogger(sys.argv[0])
np.random.seed(13)

def generate_data(npoints_total):
    """
        Generate spiral data
        Source: https://github.com/karpathy/Random-Forest-Matlab/blob/master/demos/forestdemo.m
    """
    t = np.linspace(0.5, 2*np.pi, npoints_total)
    x1 = t * np.cos(t)
    y1 = t * np.sin(t)
    x2 = t * np.cos(t+2)
    y2 = t * np.sin(t+2)
    x3 = t * np.cos(t+4)
    y3 = t * np.sin(t+4)

    X = np.matrix([np.concatenate((x1,x2,x3)),
                   np.concatenate((y1,y2,y3)),
                   np.concatenate((np.zeros(npoints_total),
                                   np.ones(npoints_total),
                                   np.ones(npoints_total)*2))]).T

    # DEBUG
    # plt.scatter(X[:,0], X[:,1], 20, np.array(X[:,2]))
    # plt.show()

    return X

if __name__ == "__main__":
    time_to_run = int(sys.argv[1]) # total running time in seconds
    npoints_per_batch = int(sys.argv[2]) # training data points pushed per batch

    nbatches = time_to_run / BATCH_DURATION
    npoints_total = npoints_per_batch * nbatches / 3

    X = generate_data(npoints_total)
    np.random.shuffle(X)

    kafka = KafkaClient(KAFKA_URL)
    kafka.ensure_topic_exists(KAFKA_TOPIC)
    producer = SimpleProducer(kafka)

    for batch_number in xrange(nbatches):
        start_row = npoints_per_batch * batch_number
        end_row = npoints_per_batch * (batch_number+1)
        X_batch = X[start_row:end_row,:]

        log.info('Pushing batch: ' + str(batch_number) +
                 ' npoints: ' + str(npoints_per_batch) +
                 ' of: ' + str(npoints_total*3))

        timestamp = str(int(time.time()))
        for x in X_batch:
            message = '\t'.join([timestamp, str(x[0,0]), str(x[0,1]), str(int(x[0,2]))])
            producer.send_messages(KAFKA_TOPIC, message)

        time.sleep(BATCH_DURATION)
