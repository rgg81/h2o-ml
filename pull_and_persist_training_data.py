#!/usr/bin/env python

from config import *
from kafka import KafkaConsumer
from kafka.common import ConsumerTimeout
import logging
import logging.config
import sys
import time

logging.config.fileConfig(LOG_FILE)
log = logging.getLogger(sys.argv[0])

if __name__ == "__main__":

    # In case of an "offset out of range" error from kafka,
    # this consumer begins reading the queue from the smallest
    # offset (which is the oldest message). This may happen if the stored
    # offsets in Kafka/Zookeeper are somehow removed.
    #
    # Hence, messages may be duplicated, but never lost. It is up to the
    # training script to deduplicate messages before training.
    consumer = KafkaConsumer(KAFKA_TOPIC, group_id=KAFKA_GROUP,
                             bootstrap_servers=[KAFKA_URL],
                             auto_offset_reset='smallest',
                             consumer_timeout_ms=AUTOCOMMIT_INTERVAL_MS)

    data = []
    try:
        for message in consumer:
            data.append(message.value)
            consumer.task_done(message)
    except ConsumerTimeout:
        log.info('Read ' + str(len(data)) + ' messages.')

    timestamp = str(int(time.time()))

    if len(data) > 0:
        with open(DATA_DIR + str(timestamp) + '.txt', 'w') as f:
            for d in data:
                f.write(d + '\n')
        consumer.commit() # commit offsets only after data has been persisted
        log.info('Persisted ' + str(len(data)) + ' new data points.')
